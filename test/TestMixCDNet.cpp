#include "TestMixCDNet.h"

/**************
Data Tests
***************/
bool TestMixCDNet::temporal_data_test(string infile_path, string infile_seq_path, int max_depth_prior,
                                      int clt_threshold_prior, int max_cond_depth, int max_depth,
                                      int clt_threshold, int maxiter, vector<double> answers,
                                      unsigned int seed, double precision, bool debug) {
    // Set seed
    setSeed(seed);
    // Create training data
    Dataset data(infile_path);
    vector<int> seqs = readSeqFromFile(infile_seq_path, ',');
    // Create prior distribution as CNet
    vector<Domain*> doms(data.size()*2);
    vector<Variable*> vars(data.size()*2);
    int V = data.size();
    for(int v=0; v<V; v++) {
        doms[v] = data[v].toDomain(v, "d"+to_string(v));
        doms[v+V] = data[v].toDomain(v+V, "d"+to_string(v+V));
        vars[v] = new Variable(*doms[v], v, "v"+to_string(v));
        vars[v+V] = new Variable(*doms[v+V], v+V, "v"+to_string(v+V));
    }
    // Create prior distribution as CNet
    vector<Variable*> prior_vars(vars.begin()+V, vars.begin()+(2*V));
    CNet* prior_dist = new CNet(data, prior_vars, max_depth_prior, clt_threshold_prior);
    // Create transition distribution as MixCNet
    vector<Variable*> transition_vars(vars.begin(), vars.begin()+V);
    Dataset* transition_data = createTransitionData(data, seqs);
    vector<int> x_indices(data.size());
    iota(x_indices.begin(), x_indices.end(), data.size());
    MixCDNet* transition_dist = new MixCDNet1(*transition_data, vars, x_indices, max_cond_depth,
                                              max_depth, clt_threshold, maxiter);
    // Use multiply and marginalize to compute message
    vector < pair<int,int> > msg_evids;
    CNet* msg = transition_dist->multiplyAndMarginalize(*prior_dist, msg_evids);
    // Check if answers match
    vector<Variable*> non_prior_vars(vars.begin(), vars.begin()+V);
    int size = var_vector_size(non_prior_vars);
    if (answers.size() != size)
        return false;
    set_all_to_zero(non_prior_vars);
    for(int i=0; i<size; i++) {
        if (!are_equal(msg->quickProbEvid(debug),answers[i],precision))
            return false;
        increment_vars(non_prior_vars);
    }
    // Garbage collection
    deleteVec(doms);
    deleteVec(vars);
    delete msg;
    delete prior_dist;
    delete transition_dist;
    // Return true
    return true;
}

bool TestMixCDNet::temporal_data_test1(double precision, bool debug) {
    // Set parameters
    string infile_path = "../data/temporal/temporal2.train";
    string infile_seq_path = "../data/temporal/temporal2.train.seq";
    int seed = 1991;
    int max_depth_prior = 0;
    int clt_threshold_prior = 1;
    int max_cond_depth = 2;
    int max_depth = 0;
    int clt_threshold = 1;
    int maxiter = 10;
    vector<double> answers({0.2160, 0.1742, 0.1882, 0.4216});
    // Create training data
    return temporal_data_test(infile_path, infile_seq_path, max_depth_prior, clt_threshold_prior,
                                max_cond_depth, max_depth, clt_threshold, maxiter, answers, seed, precision, debug);
}

/**************
Run All Tests
***************/
void TestMixCDNet::run_all_tests(bool debug, double precision) {
    cout << "=================================" << endl;
    cout << "Testing MixCDNet.h " << endl;
    cout << "=================================" << endl;
    cout << "Passed test1: " << temporal_data_test1(precision, debug) << endl;
}