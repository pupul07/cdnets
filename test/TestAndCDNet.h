#ifndef TESTANDCDNET_H
#define TESTANDCDNET_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../src/Dataset.h"
#include "../src/DCN.h"
#include "../src/AndCDNet.h"

using namespace std;

// Permissible error bound for computing probabilities
const double PREC_ANDCDNET = 0.01;

class TestAndCDNet {
public:
    // Data Test
    static bool disc_data_test(string infile_path, vector<int> x_indices, int max_cond_depth, int max_depth,
                                   int clt_threshold, vector<vector<pair<int,int> > > all_evids, vector<double> answers,
                                   int max_part_size=1, double precision=PREC_ANDCDNET, bool debug=false);
    static bool disc_data_test1(double precision=PREC_ANDCDNET, bool debug=false);
    // Run all Tests
    static void run_all_tests(bool=false, double=PREC_ANDCDNET);
};

#endif
