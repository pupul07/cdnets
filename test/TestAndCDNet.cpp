#include "TestAndCDNet.h"

#include <utility>

/**************
Data Tests
***************/
bool TestAndCDNet::disc_data_test(string infile_path, vector<int> x_indices, int max_cond_depth, int max_depth,
                                      int clt_threshold, vector<vector<pair<int,int> > > all_evids, vector<double> answers,
                                      int max_part_size, double precision, bool debug) {
    // Create training data
    Dataset data(std::move(infile_path));
    // Create transition distribution as MixCNet
    AndCDNet* andcdnet = new AndCDNet(data, x_indices, max_cond_depth,
                                      max_depth, clt_threshold, max_part_size);
    // Check if answers match
    for(int e=0; e<all_evids.size(); e++) {
        if (!are_equal(andcdnet->probEvid(all_evids[e]),answers[e],precision))
            return false;
    }
    // Garbage collection
    delete andcdnet;
    // Return true
    return true;
}

bool TestAndCDNet::disc_data_test1(double precision, bool debug) {
    // Set model parameters
    string infile_path = "../data/temporal/temporal3.csv";
    vector<int> x_indices({2,3});
    int max_cond_depth = 2;
    int max_depth = 1;
    int clt_threshold = 1;
    int max_part_size = 1;
    // Build Evidence
    vector<vector<pair<int,int> > > all_evids = vector<vector<pair<int,int> > >({zip(vector<int>({0,1,2,3}), vector<int>({0,0,0,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,1,0,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,0,0,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,1,0,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,0,0,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,1,0,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,0,0,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,1,0,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,0,1,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,1,1,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,0,1,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,1,1,0})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,0,1,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({0,1,1,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,0,1,1})),
                                                                                 zip(vector<int>({0,1,2,3}), vector<int>({1,1,1,1}))});
    vector<double> answers({0.27, 0.03, 0.63, 0.07,
                            0.40, 0.40, 0.10, 0.10,
                            0.30, 0.20, 0.30, 0.20,
                            0.04, 0.36, 0.06, 0.54});
    // Run test
    return disc_data_test(infile_path, x_indices, max_cond_depth, max_depth, clt_threshold,
                            all_evids, answers, max_part_size, precision, debug);
}

/**************
Run All Tests
***************/
void TestAndCDNet::run_all_tests(bool debug, double precision) {
    cout << "=================================" << endl;
    cout << "Testing AndCDNet.h " << endl;
    cout << "=================================" << endl;
    cout << "Passed test1: " << disc_data_test1(precision, debug) << endl;
}