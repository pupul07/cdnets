#include "TestCDNet.h"
#include "TestDCN.h"

/**************
Data-based Tests
***************/
bool TestDCN::data_test(string data_dir, string data_name, int max_depth_prior, int clt_threshold_prior,
               int max_cond_depth, int max_depth, int clt_threshold, vector<int> evid_indices,
               double avg_ll, double precision, bool debug) {
    // Initialize paths
    string train_path = data_dir + "/" + data_name + ".train";
    string train_seqs_path = data_dir + "/" + data_name + ".train.seq";
    string test_path = data_dir + "/" + data_name + ".test";
    string test_seqs_path = data_dir + "/" + data_name + ".test.seq";
    // Learn Model
    Dataset train_data(train_path);
    vector<int> train_seqs = readSeqFromFile(train_seqs_path, ',');
    DCN dcn(train_data, train_seqs, max_depth_prior, clt_threshold_prior,
            max_cond_depth, max_depth, clt_threshold, false);
    // Perform inference
    Dataset test_data(test_path);
    vector<int> test_seqs = readSeqFromFile(test_seqs_path, ',');
    double forward_ll = dcn.forwardLL(test_data, test_seqs, false);
    return are_equal(forward_ll, avg_ll, precision);
}

bool TestDCN::data_gen_test1(double precision, bool debug) {
    // Initialize parameters
    string data_dir = "/Users/chiradeep/dev/cdnets/data/temporal";
    string data_name = "temporal2";
    int max_depth_prior = 0;
    int clt_threshold_prior = 1;
    int max_cond_depth = 2;
    int max_depth = 0;
    int clt_threshold = 1;
    vector<int> evid_indices;
    double avg_ll = -1.04;
    // Call method
    return data_test(data_dir, data_name, max_depth_prior, clt_threshold_prior, max_cond_depth,
                max_depth, clt_threshold, evid_indices, avg_ll, precision, debug);
}

bool TestDCN::data_disc_test1(double precision, bool debug) {
    // Initialize parameters
    string data_dir = "/Users/chiradeep/dev/cdnets/data/temporal";
    string data_name = "temporal2";
    int max_depth_prior = 0;
    int clt_threshold_prior = 1;
    int max_cond_depth = 2;
    int max_depth = 0;
    int clt_threshold = 1;
    vector<int> evid_indices({1});
    double avg_ll = -0.41;
    // Call method
    return data_test(data_dir, data_name, max_depth_prior, clt_threshold_prior, max_cond_depth,
              max_depth, clt_threshold, evid_indices, avg_ll, precision, debug);
}

/**************
Run All Tests
***************/
void TestDCN::run_all_tests(bool debug, double precision) {
    cout << "=================================" << endl;
    cout << "Testing DCN.h " << endl;
    cout << "=================================" << endl;
    cout << "Passed data1_gen_test: " << data_gen_test1(precision, debug) << endl;
    // cout << "Passed data1_disc_test: " << data_disc_test1(precision, debug) << endl;
}