#ifndef TESTFUNCTION_H_
#define TESTFUNCTION_H_

#include <iostream>
#include <vector>
#include "../src/Variable.h"
#include "../src/Function.h"
#include "../src/Utilities.h"

using namespace std;

struct TestFunction {
  static bool var_vector_increment_test(bool=false);
  static void run_all_tests(bool=false);
};

struct TestCPT {
  static bool normalize1(bool=false);
  static bool multiply_and_marginalize1(bool=false);
  static bool multiply_and_marginalize2(bool=false);
  static bool multiply_and_marginalize3(bool=false);
  static bool multiply_and_marginalize4(bool=false);
  static bool remove_evidence1(bool=false);
  static bool remove_evidence2(bool=false);
  static bool remove_evidence3(bool=false);
  static bool remove_evidence4(bool=false);
  static void run_all_tests(bool=false);
};

vector<Variable> assign_variables(vector<Domain*>, vector<string> = vector<string>(),
                                    vector<int> = vector<int>());
CPT create_cpt(vector<Variable>&, vector<int>, vector<int>);

#endif
