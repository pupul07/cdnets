#ifndef TESTDCN_H
#define TESTDCN_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../src/DCN.h"
#include "../src/Utilities.h"
#include "TestCNet.h"

using namespace std;

// Permissible error bound for computing probabilities
const double PREC_DCN = 0.01;

class TestDCN {
    public:
        // Data tests
        static bool data_test(string data_dir, string data_name, int max_depth_prior, int clt_threshold_prior,
                               int max_cond_depth, int max_depth, int clt_threshold, vector<int> evid_indices,
                               double avg_ll, double precision=PREC_DCN, bool debug=false);
        static bool data_gen_test1(double precision=PREC_DCN, bool debug=false);
        static bool data_disc_test1(double precision=PREC_DCN, bool debug=false);
        // Run all Tests
        static void run_all_tests(bool=false, double=PREC_DCN);
};

#endif
