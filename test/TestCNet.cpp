#include "TestCNet.h"

using namespace std;

/**************
Pr(evid) tests
***************/
bool TestCNet::prob_evid_test(CNet& cnet, vector<pair<int,int> >& evidence,
                             double prob, double precision, bool debug) {
    return are_equal(cnet.probEvid(evidence), prob, precision);
}

bool TestCNet::prob_evid_test_batch(CNet& cnet, vector<vector<pair<int,int> > >& evids,
                                   vector<double>& probs, double precision, bool debug) {
    if(evids.size() != probs.size())
        return false;
    // Test for each evidence combination in list
    bool all_passed = true;
    for(int e=0; e<evids.size(); e++) {
        bool passed = prob_evid_test(cnet, evids[e], probs[e], precision, debug);
        if(!passed && !debug)
            return false;
        if(!passed && debug)
            cout << "[WARNING] Failed test case " << e+1 << endl;
        all_passed = all_passed && passed;
    }
    return all_passed;
}

/******************
Pr(marg|evid) tests
*******************/
bool TestCNet::prob_marg_evid_test(CNet & cnet, vector<pair<int, int> >& marg_evid,
                                  vector<pair<int, int> >& evid, double prob,
                                  double precision, bool debug) {
    return are_equal(cnet.margEvid(marg_evid, evid, debug),prob,precision);
}

bool TestCNet::prob_marg_evid_test_batch(CNet& cnet, vector<vector<pair<int,int> > >& margs,
                                         vector<vector<pair<int,int> > >& evids, vector<double>& probs,
                                         double precision, bool debug) {
    if(margs.size() != evids.size() || margs.size() != probs.size())
        return false;
    // Test for each evidence combination in list
    bool all_passed = true;
    for(int e=0; e<evids.size(); e++) {
        bool passed = prob_marg_evid_test(cnet, margs[e],evids[e], probs[e], precision, debug);
        if(!passed && !debug)
            return false;
        if(!passed && debug)
            cout << "[WARNING] Failed test case " << e+1 << endl;
        all_passed = all_passed && passed;
    }
    return all_passed;
}

/******************
MPE tests
*******************/
bool TestCNet::mpe_test(CNet & cnet, vector<pair<int, int> >& evidence,
                       vector<int>& tuple, double prob, double precision,
                       bool debug) {
    pair<double,vector<int> > mpe = cnet.MPE(evidence);
    return are_equal_vec(mpe.second, tuple, 0) &&
           are_equal(mpe.first, prob, precision);
}

bool TestCNet::mpe_test_batch(CNet & cnet, vector<vector<pair<int, int> > >& evids,
                             vector<vector<int> >& tuples, vector<double>& probs,
                             double precision, bool debug) {
    if(evids.size() != probs.size())
        return false;
    // Test for each evidence combination in list
    bool all_passed = true;
    for(int e=0; e<evids.size(); e++) {
        bool passed = mpe_test(cnet, evids[e], tuples[e], probs[e], precision, debug);
        if(!passed && !debug)
            return false;
        if(!passed && debug)
            cout << "[WARNING] Failed test case " << e+1 << endl;
        all_passed = all_passed && passed;
    }
    return all_passed;
}

/**************
Data-based Tests
***************/
void TestCNet::data_test(string infile_path, int max_depth, int clt_threshold,
                         vector<vector<pair<int,int> > >& margs, vector<vector<pair<int,int> > >& evids,
                         vector<vector<pair<int,int> > >& mpe_evids, vector<vector<int> >& mpe_tuples,
                         vector<double>& probs, vector<double>& mpe_probs, double precision, bool debug) {
    // Create Cutset Network
    CNet* cnet = create_cnet(infile_path, max_depth, clt_threshold);
    // Test Marginal Probability of Evidence
    cout << "Passed P(m|e) tests: " << prob_marg_evid_test_batch(*cnet, margs, evids, probs, precision, debug) << endl;
    // Test MPE
    cout << "Passed MPE tests: " << mpe_test_batch(*cnet, mpe_evids, mpe_tuples, mpe_probs, precision, debug)
         << endl;
}

void TestCNet::data1_test(double precision, bool debug) {
    // Set Cutset Network parameters
    string infile_path = "/Users/chiradeep/dev/cdnets/data/misc/data1.csv";
    int max_depth=2;
    int clt_threshold=1;
    // Prepare testing data for P(m|e)
    vector<vector<pair<int,int> > > margs({zip(vector<int>({1}),vector<int>({0})),
                                           zip(vector<int>({1}),vector<int>({1})),
                                           zip(vector<int>({2}),vector<int>({0})),
                                           zip(vector<int>({2}),vector<int>({1}))});
    vector<vector<pair<int,int> > > evids({zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({}))});
    vector<double> probs({0.74,0.26,0.38,0.62});
    // Prepare testing data for MPE
    vector<vector<pair<int,int> > > mpe_evids({zip(vector<int>({0}),vector<int>({0})),
                                                zip(vector<int>({0}),vector<int>({1})),
                                                zip(vector<int>({1}),vector<int>({0})),
                                                zip(vector<int>({1}),vector<int>({1})),
                                                zip(vector<int>({2}),vector<int>({0})),
                                                zip(vector<int>({2}),vector<int>({1}))});
    vector<vector<int> > mpe_tuples({vector<int>({0,0,1}),
                                     vector<int>({1,0,0}),
                                     vector<int>({1,0,1}),
                                     vector<int>({0,1,1}),
                                     vector<int>({1,0,0}),
                                     vector<int>({1,0,1})});
    vector<double> mpe_probs({0.162, 0.280, 0.280, 0.108, 0.280, 0.280});
    // Print test results
    cout << "*******************" << endl;
    cout << infile_path << endl;
    cout << "*******************" << endl;
    for(int depth=0; depth<=max_depth; depth++) {
        cout << "** Depth: " << depth << endl;
        data_test(infile_path, depth, clt_threshold, margs, evids, mpe_evids,
                  mpe_tuples, probs, mpe_probs, precision, debug);
    }
}

void TestCNet::data2_test(double precision, bool debug) {
    // Set Cutset Network parameters
    string infile_path = "/Users/chiradeep/dev/cdnets/data/misc/data2.csv";
    int max_depth=3;
    int clt_threshold=1;
    // Prepare testing data for P(m|e)
    vector<vector<pair<int,int> > > margs({zip(vector<int>({1}),vector<int>({0})),
                                           zip(vector<int>({1}),vector<int>({1})),
                                           zip(vector<int>({2}),vector<int>({0})),
                                           zip(vector<int>({2}),vector<int>({1})),
                                           zip(vector<int>({3}),vector<int>({0})),
                                           zip(vector<int>({3}),vector<int>({1})),
                                           zip(vector<int>({1,3}),vector<int>({0,0})),
                                           zip(vector<int>({0,2}),vector<int>({1,0}))});
    vector<vector<pair<int,int> > > evids({zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({})),
                                           zip(vector<int>({}),vector<int>({}))});
    vector<double> probs({0.6,0.4,0.82,0.18,0.136,0.864,0.072,0.656});
    // Prepare testing data for MPE
    vector<vector<pair<int,int> > > mpe_evids({zip(vector<int>({}),vector<int>({})),
                                               zip(vector<int>({0}),vector<int>({0})),
                                               zip(vector<int>({0,3}),vector<int>({1,1})),
                                               zip(vector<int>({1,2}),vector<int>({0,1})),
                                               zip(vector<int>({1,2}),vector<int>({1,0})),
                                               zip(vector<int>({0,1,2}),vector<int>({1,0,1})),
                                               zip(vector<int>({0,1,2,3}),vector<int>({1,0,0,0}))});
    vector<vector<int> > mpe_tuples({vector<int>({1,0,0,1}),
                                     vector<int>({0,0,0,1}),
                                     vector<int>({1,0,0,1}),
                                     vector<int>({0,0,1,1}),
                                     vector<int>({1,1,0,1}),
                                     vector<int>({1,0,1,1}),
                                     vector<int>({1,0,0,0})});
    vector<double> mpe_probs({0.389, 0.097, 0.389, 0.025, 0.202, 0.017, 0.043});
    // Print test results
    cout << "*******************" << endl;
    cout << infile_path << endl;
    cout << "*******************" << endl;
    for(int depth=0; depth<=max_depth; depth++) {
        cout << "** Depth: " << depth << endl;
        data_test(infile_path, depth, clt_threshold, margs, evids, mpe_evids,
                  mpe_tuples, probs, mpe_probs, precision, debug);
    }
}

/**************
Run All Tests
***************/
void TestCNet::run_all_tests(bool debug, double precision) {
    cout << "=================================" << endl;
    cout << "Testing CNet.h " << endl;
    cout << "=================================" << endl;
    data1_test();
    data2_test();
}

/**************
Misc Functions
***************/
CNet* create_cnet(string infile_path, int max_depth, int clt_threshold, bool smooth) {
    CNet* cnet = nullptr;
    Dataset data(infile_path);
    cnet = new CNet(data, max_depth, clt_threshold, smooth);
    return cnet;
}

CNet* create_cnet(Dataset& data, vector<Variable*>& vars, int max_depth, int clt_threshold, bool smooth) {
    return new CNet(data, vars, max_depth, clt_threshold, smooth);
}