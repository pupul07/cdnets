#ifndef TESTUTILITIES_H_
#define TESTUTILITIES_H_

#include <iostream>
#include <vector>
#include "../src/Utilities.h"

using namespace std;

struct TestUtilities {
  static bool vector_union_test1(bool=false);
  static bool vector_intersection_test1(bool=false);
  static bool vector_difference_test1(bool=false);
  static bool vector_difference_test2(bool=false);
  static void run_all_tests(bool=false);
};

#endif
