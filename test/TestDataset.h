#ifndef TESTFUNCTION_H_
#define TESTFUNCTION_H_

#include <iostream>
#include <vector>
#include "../src/Dataset.h"
#include "../src/Utilities.h"

using namespace std;

struct TestDataset {
  static void subset_test1(bool=false)
  static void run_all_tests(bool=false);
};

#endif
