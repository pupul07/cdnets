#include "TestCLT.h"

using namespace std;

/**************
Creation Tests
***************/
bool TestCLT::create_test(Dataset &data, bool debug) {
    CLT clt(data);
    // Compare number of CPTs
    if (clt.cpts().size() != 3)
        return false;
    // Compare parameters of each CPT
    return are_equal_vec(clt.cpts()[0]->table(), vector<double>({0.3,0.7}), 0.01) &&
            are_equal_vec(clt.cpts()[1]->table(), vector<double>({0.6,0.8,0.4,0.2}), 0.01) &&
            are_equal_vec(clt.cpts()[2]->table(), vector<double>({0.1,0.5,0.9,0.5}), 0.01);
}

bool TestCLT::create_test1(bool debug) {
    string infile_path = "../data/data1.csv";
    Dataset data(infile_path);
    return create_test(data, debug);
}

/**************
Pr(evid) tests
***************/
bool TestCLT::prob_evid_test(CLT& clt, vector<pair<int,int> >& evidence,
                    double prob, double precision, bool debug) {
    return abs(clt.probEvid(evidence)-prob) <= precision;
}

bool TestCLT::prob_evid_test_batch(CLT& clt, vector<vector<pair<int,int> > >& evids,
                             vector<double>& probs, double precision, bool debug) {
    if(evids.size() != probs.size())
        return false;
    // Test for each evidence combination in list
    bool all_passed = true;
    for(int e=0; e<evids.size(); e++) {
        bool passed = prob_evid_test(clt, evids[e], probs[e], precision, debug);
        if(!passed && !debug)
            return false;
        if(!passed && debug)
            cout << "[WARNING] Failed test case " << e+1 << endl;
        all_passed = all_passed && passed;
    }
    return all_passed;
}

bool TestCLT::prob_evid_test1(double precision, bool debug) {
    CLT clt = create_clt("../data/data1.csv");
    vector<pair<int,int> > evidence;
    return prob_evid_test(clt, evidence, 1.0, precision, debug);
}

bool TestCLT::prob_evid_test_batch1(double precision, bool debug) {
    CLT clt = create_clt("../data/data1.csv");
    vector<vector<pair<int,int> > > evids({zip(vector<int>({1}),vector<int>({0})),
                                           zip(vector<int>({1}),vector<int>({1})),
                                           zip(vector<int>({2}),vector<int>({0})),
                                           zip(vector<int>({2}),vector<int>({1}))});
    vector<double> probs({0.74,0.26,0.38,0.62});
    return prob_evid_test_batch(clt, evids, probs, precision, debug);
}

/******************
Pr(marg|evid) tests
*******************/
bool TestCLT::prob_marg_evid_test(CLT & clt, vector<pair<int, int> >& marg_evid,
                                    vector<pair<int, int> >& evid, double prob,
                                    double precision, bool debug) {
    return abs(clt.margEvid(marg_evid, evid, debug)-prob) <= precision;
}

bool TestCLT::prob_marg_evid_test1(double precision, bool debug) {
    CLT clt = create_clt("../data/data1.csv");
    vector<pair<int,int> > marg_evid;
    vector<pair<int,int> > evid;
    return prob_marg_evid_test(clt, marg_evid, evid, 1.0, precision, debug);
}

/******************
MPE tests
*******************/
bool TestCLT::mpe_test(CLT & clt, vector<pair<int, int> >& evidence,
                       vector<int>& tuple, double prob, double precision,
                       bool debug) {
    pair<double,vector<int> > mpe = clt.MPE(evidence);
    return are_equal_vec(mpe.second, tuple, 0) &&
            abs(mpe.first-prob) <= precision;
}

bool TestCLT::mpe_test_batch(CLT & clt, vector<vector<pair<int, int> > >& evids,
                            vector<vector<int> >& tuples, vector<double>& probs,
                            double precision, bool debug) {
    if(evids.size() != probs.size())
        return false;
    // Test for each evidence combination in list
    bool all_passed = true;
    for(int e=0; e<evids.size(); e++) {
        bool passed = mpe_test(clt, evids[e], tuples[e], probs[e], precision, debug);
        if(!passed && !debug)
            return false;
        if(!passed && debug)
            cout << "[WARNING] Failed test case " << e+1 << endl;
        all_passed = all_passed && passed;
    }
    return all_passed;
}

bool TestCLT::mpe_test1(double precision, bool debug) {
    CLT clt = create_clt("../data/data1.csv");
    vector<pair<int,int> > evidence;
    double mpe_prob = 0.280;
    vector<int> mpe_tuple({1,0,0});
    return mpe_test(clt, evidence, mpe_tuple, mpe_prob, precision, debug);
}

bool TestCLT::mpe_test_batch1(double precision, bool debug) {
    CLT clt = create_clt("../data/data1.csv");
    vector<vector<pair<int,int> > > evids({zip(vector<int>({0}),vector<int>({0})),
                                           zip(vector<int>({0}),vector<int>({1})),
                                           zip(vector<int>({1}),vector<int>({0})),
                                           zip(vector<int>({1}),vector<int>({1})),
                                           zip(vector<int>({2}),vector<int>({0})),
                                           zip(vector<int>({2}),vector<int>({1}))});
    vector<vector<int> > mpe_tuples({vector<int>({0,0,1}),
                                     vector<int>({1,0,0}),
                                     vector<int>({1,0,0}),
                                     vector<int>({0,1,1}),
                                     vector<int>({1,0,0}),
                                     vector<int>({1,0,1})});
    vector<double> mpe_probs({0.162, 0.280, 0.280, 0.108, 0.280, 0.280});
    return mpe_test_batch(clt, evids, mpe_tuples, mpe_probs, precision, debug);
}

/**************
Run All Tests
***************/
void TestCLT::run_all_tests(bool debug, double precision) {
    cout << "=================================" << endl;
    cout << "Testing CLT.h " << endl;
    cout << "=================================" << endl;
    cout << "Passed create_test1(): " << create_test1(debug) << endl;
    cout << "Passed prob_evid_test1(): " << prob_evid_test1(precision, debug) << endl;
    cout << "Passed prob_evid_test_batch1(): " << prob_evid_test_batch1(precision, debug) << endl;
    cout << "Passed prob_marg_evid_test1(): " << prob_marg_evid_test1(precision, debug) << endl;
    cout << "Passed mpe_test1(): " << mpe_test1(precision, debug) << endl;
    cout << "Passed mpe_test_batch1(): " << mpe_test_batch1(precision, debug) << endl;
}

/**************
Misc Functions
***************/
CLT& create_clt(string infile_path) {
    CLT* clt = nullptr;
    Dataset data(infile_path);
    clt = new CLT(data);
    return *clt;
}