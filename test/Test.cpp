#include <iostream>
#include <string>
#include "TestCLT.h"
#include "TestCNet.h"
#include "TestCDNet.h"
#include "TestMixCDNet.h"
#include "TestAndCDNet.h"
#include "TestDCN.h"
#include "../src/Utilities.h"

int main(int argc, char* argv[]) {
  // TestCLT::run_all_tests();
  // TestCNet::run_all_tests();
  // TestCDNet::run_all_tests();
  // TestDCN::run_all_tests();
  // TestMixCDNet::run_all_tests();
  TestAndCDNet::run_all_tests();
  return 0;
}
