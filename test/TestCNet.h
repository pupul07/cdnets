#ifndef TESTCNET_H
#define TESTCNET_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../src/Variable.h"
#include "../src/Function.h"
#include "../src/Dataset.h"
#include "../src/CLT.h"
#include "../src/CNet.h"
#include "../src/Utilities.h"

using namespace std;

// Permissible error bound for computing probabilities
const double PREC_CNET = 0.01;

struct TestCNet {
    // Pr(evid)
    static bool prob_evid_test(CNet&, vector<pair<int,int> >&, double,
                               double=PREC_CNET, bool=false);
    static bool prob_evid_test_batch(CNet&, vector<vector<pair<int,int> > >&,
                                        vector<double>&, double=PREC_CNET, bool=false);
    // Pr(marg|evid)
    static bool prob_marg_evid_test(CNet&, vector<pair<int,int> >&,
                                    vector<pair<int,int> >&, double,
                                    double=PREC_CNET, bool=false);
    static bool prob_marg_evid_test_batch(CNet&, vector<vector<pair<int,int> > >&,
                                             vector<vector<pair<int,int> > >&, vector<double>&,
                                             double=PREC_CNET, bool=false);
    // MPE [ argmax Pr(x|e) ]
    static bool mpe_test(CNet&, vector<pair<int, int> >&,
                         vector<int>&, double, double=PREC_CNET, bool=false);
    static bool mpe_test_batch(CNet&, vector<vector<pair<int, int> > >&,
                                vector<vector<int> >&, vector<double>&,
                                double=PREC_CNET, bool=false);
    // Data-based tests
    static void data_test(string infile_path, int max_depth, int clt_threshold,
                          vector<vector<pair<int,int> > >& margs, vector<vector<pair<int,int> > >& evids,
                          vector<vector<pair<int,int> > >& mpe_evids, vector<vector<int> >& mpe_tuples,
                          vector<double>& probs, vector<double>& mpe_probs,
                          double precision=PREC_CNET, bool debug=false);
    static void data1_test(double=PREC_CNET, bool=false);
    static void data2_test(double=PREC_CNET, bool=false);
    // Run All Tests
    static void run_all_tests(bool=false, double=PREC_CNET);
};

// Miscellaneous Functions
CNet* create_cnet(string, int=5, int=5, bool=true);
CNet* create_cnet(Dataset&, vector<Variable*>&, int=5, int=5, bool=true);

#endif
