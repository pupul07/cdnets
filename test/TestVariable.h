#ifndef TESTVARIABLE_H_
#define TESTVARIABLE_H_

#include <iostream>
#include "../src/Variable.h"

using namespace std;

struct TestDomain {
  static bool create_test(vector<string>&);
  static bool create_test1();
  static void run_all_tests();
};

#endif
