#ifndef TESTCLT_H_
#define TESTCLT_H_

#include <iostream>
#include <vector>
#include <cmath>
#include "../src/Variable.h"
#include "../src/Function.h"
#include "../src/Dataset.h"
#include "../src/CLT.h"
#include "../src/Utilities.h"

using namespace std;

// Permissible error bound for computing probabilities
const double PREC_CLT = 0.01;

struct TestCLT {
  // Creation
  static bool create_test(Dataset&, bool=false);
  static bool create_test1(bool=false);
  // Pr(evid)
  static bool prob_evid_test(CLT&, vector<pair<int,int> >&, double,
                                double=PREC_CLT, bool=false);
  static bool prob_evid_test_batch(CLT&, vector<vector<pair<int,int> > >&,
                                   vector<double>&, double=PREC_CLT, bool=false);
  static bool prob_evid_test1(double=PREC_CLT, bool=false);
  static bool prob_evid_test_batch1(double=PREC_CLT, bool=false);
  // Pr(marg|evid)
  static bool prob_marg_evid_test(CLT&, vector<pair<int,int> >&,
                                  vector<pair<int,int> >&, double,
                                  double=PREC_CLT, bool=false);
  static bool prob_marg_evid_test1(double=PREC_CLT, bool=false);
  // MPE [ argmax Pr(x|e) ]
  static bool mpe_test(CLT&, vector<pair<int, int> >&,
                        vector<int>&, double, double=PREC_CLT, bool=false);
  static bool mpe_test_batch(CLT&, vector<vector<pair<int, int> > >&,
                             vector<vector<int> >&, vector<double>&,
                             double=PREC_CLT, bool=false);
  static bool mpe_test1(double=PREC_CLT, bool=false);
  static bool mpe_test_batch1(double=PREC_CLT, bool=false);
  // Run All Tests
  static void run_all_tests(bool=false, double=PREC_CLT);
};

// Miscellaneous Functions
CLT& create_clt(string infile);

#endif
