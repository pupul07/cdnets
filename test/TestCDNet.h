#ifndef TESTCDNET_H
#define TESTCDNET_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../src/Variable.h"
#include "../src/Function.h"
#include "../src/Dataset.h"
#include "../src/CLT.h"
#include "../src/CNet.h"
#include "../src/CDNet.h"
#include "../src/Utilities.h"
#include "TestCNet.h"

using namespace std;

// Permissible error bound for computing probabilities
const double PREC_CDNET = 0.01;

class TestCDNet {
    public:
        // Data tests
        static bool data_test(string infile_path, vector<int>& x_indices, vector<pair<int,int> >& evids,
                                int max_cond_depth, int max_depth, int clt_threshold,
                                vector<double> probs, double precision=PREC_CDNET, bool debug=false);
        static bool data1_gen_test(double=PREC_CDNET, bool=false);
        static bool data1_disc_test(double=PREC_CDNET, bool=false);
        // Run all Tests
        static void run_all_tests(bool=false, double=PREC_CDNET);
};

// Miscellaneous Functions
CDNet& create_cdnet(string infile, vector<int> x_indices, int max_cond_depth=2,
                    int max_depth=5, int clt_threshold=5, bool smooth=true);
CDNet& create_cdnet(Dataset& data, vector<Variable*>& vars, vector<int> x_indices,
                    int max_cond_depth=2, int max_depth=5, int clt_threshold=5, bool smooth=true);

#endif
