#include "TestFunction.h"

using namespace std;

/***********************
Function
***********************/
bool TestFunction::var_vector_increment_test(bool debug) {
  // Initialize variables
  Domain sex(vector<string>({"male", "female"}), 0, "sex");
  Domain emotions(vector<string>({"joy", "sadness", "disgust", "fear", "anger"}), 1, "emotions");
  Variable gender(sex, 0, "gender");
  Variable mood(emotions, 1, "mood");
  // Create vector
  vector<Variable*> scope;
  scope.push_back(&gender);
  scope.push_back(&mood);
  // Check
  vector<int> gender_indexes({0, 0, 0, 0, 0, 1, 1, 1, 1, 1});
  vector<int> mood_indexes({0, 1, 2, 3, 4, 0, 1, 2, 3, 4});
  for(int i=0; i<gender_indexes.size(); i++) {
    if (debug) cout << scope[0]->val() << " " << scope[1]->val() << endl;
    if (scope[0]->val() != gender_indexes[i] || scope[1]->val() != mood_indexes[i])
      return false;
    increment_vars(scope);
  }
  return true;
}

void TestFunction::run_all_tests(bool debug) {
  cout << "=================================" << endl;
  cout << "Testing Function.h" << endl;
  cout << "=================================" << endl;
  cout << "Passed var_vector_increment_test(): " << var_vector_increment_test(debug) << endl;
}

/***********************
CPT
***********************/
bool TestCPT::normalize1(bool debug) {
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin}));
  CPT cpt = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  cpt.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  vector<double> answer({0.2, 0.7, 0.8, 0.3});
  if (debug) cout << "CPT" << cpt.str() << endl;
  return compare_vectors<double>(cpt.table(), answer, 0.001);
}

bool TestCPT::multiply_and_marginalize1(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin, &d_bin, &d_bin}));
  CPT cpt1 = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  CPT cpt2 = create_cpt(vars, vector<int>({2}), vector<int>({3}));
  cpt1.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  cpt2.table() = vector<double>({0.4, 0.9, 0.6, 0.1});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT 1" << cpt1.str() << endl;
  if (debug) cout << "CPT 2" << cpt2.str() << endl;
  // Multiply and marginalize CPT 3
  CPT* cpt3 = CPT::multiplyAndMarginalize(cpt1, cpt2);
  // Print table if debug option is on
  if (debug) cout << "CPT 3" << cpt3->str() << endl;
  // Store and compare answers
  vector<double> table(cpt3->table());
  vector<double> answer({0.08, 0.18, 0.28, 0.63, 0.12, 0.02, 0.42, 0.07,
                          0.32, 0.72, 0.12, 0.27, 0.48, 0.08, 0.18, 0.03});
  delete cpt3;
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::multiply_and_marginalize2(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin, &d_bin}));
  CPT cpt1 = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  CPT cpt2 = create_cpt(vars, vector<int>({1}), vector<int>({2}));
  cpt1.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  cpt2.table() = vector<double>({0.4, 0.9, 0.6, 0.1});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT 1" << cpt1.str() << endl;
  if (debug) cout << "CPT 2" << cpt2.str() << endl;
  // Multiply and marginalize CPT 3
  CPT* cpt3 = CPT::multiplyAndMarginalize(cpt1, cpt2);
  // Print table if debug option is on
  if (debug) cout << "CPT 3" << cpt3->str() << endl;
  // Store and compare answers
  vector<double> table(cpt3->table());
  vector<double> answer({0.08, 0.18, 0.42, 0.07, 0.32, 0.72, 0.18, 0.03});
  delete cpt3;
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::multiply_and_marginalize3(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin, &d_bin}));
  CPT cpt1 = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  CPT cpt2 = create_cpt(vars, vector<int>({1}), vector<int>({2}));
  cpt1.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  cpt2.table() = vector<double>({0.4, 0.9, 0.6, 0.1});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT 1" << cpt1.str() << endl;
  if (debug) cout << "CPT 2" << cpt2.str() << endl;
  // Multiply and marginalize CPT 3
  CPT* cpt3 = CPT::multiplyAndMarginalize(cpt1, cpt2, vector<Variable*>({&vars[0]}));
  // Print table if debug option is on
  if (debug) cout << "CPT 3" << cpt3->str() << endl;
  // Store and compare answers
  vector<double> table(cpt3->table());
  vector<double> answer({0.5, 0.25, 0.5, 0.75});
  delete cpt3;
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::multiply_and_marginalize4(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin, &d_bin}));
  CPT cpt1 = create_cpt(vars, vector<int>({0}), vector<int>({2}));
  CPT cpt2 = create_cpt(vars, vector<int>({1}), vector<int>({2}));
  cpt1.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  cpt2.table() = vector<double>({0.4, 0.9, 0.6, 0.1});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT 1" << cpt1.str() << endl;
  if (debug) cout << "CPT 2" << cpt2.str() << endl;
  // Multiply and marginalize CPT 3
  CPT* cpt3 = CPT::multiplyAndMarginalize(cpt1, cpt2, vector<Variable*>({&vars[0]}));
  // Print table if debug option is on
  if (debug) cout << "CPT 3" << cpt3->str() << endl;
  // Store and compare answers
  vector<double> table(cpt3->table());
  vector<double> answer({0.08, 0.63, 0.12, 0.07, 0.32, 0.27, 0.48, 0.03});
  delete cpt3;
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::remove_evidence1(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin}));
  CPT cpt = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  cpt.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT" << cpt.str() << endl;
  // Reduce cpt
  CPT* cpt_new = cpt.removeEvidence(vector<Variable*>());
  // Print table if debug option is on
  if (debug) cout << "CPT_new" << cpt_new->str() << endl;
  // Store and compare answers
  vector<double> table(cpt_new->table());
  vector<double> answer({0.2, 0.7, 0.8, 0.3});
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::remove_evidence2(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  Domain d_evid(vector<string>({"e"}), 1, "evidence");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin}));
  vector<Variable> evid_vars = assign_variables(vector<Domain*>({&d_evid}));
  CPT cpt = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  cpt.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT" << cpt.str() << endl;
  // Reduce cpt
  vars[1].val()=0;
  CPT* cpt_new = cpt.removeEvidence(vector<Variable*>{&evid_vars[0]});
  // Print table if debug option is on
  if (debug) cout << "CPT_new" << cpt_new->str() << endl;
  // Store and compare answers
  vector<double> table(cpt_new->table());
  vector<double> answer({0.2, 0.8});
  delete cpt_new;
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::remove_evidence3(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  Domain d_evid(vector<string>({"e"}), 1, "evidence");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin}));
  vector<Variable> evid_vars = assign_variables(vector<Domain*>({&d_evid}));
  CPT cpt = create_cpt(vars, vector<int>({0}), vector<int>({1}));
  cpt.table() = vector<double>({0.2, 0.7, 0.8, 0.3});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT" << cpt.str() << endl;
  // Reduce cpt
  vars[0].val()=1;
  CPT* cpt_new = cpt.removeEvidence(vector<Variable*>{&evid_vars[0]});
  // Print table if debug option is on
  if (debug) cout << "CPT_new" << cpt_new->str() << endl;
  // Store and compare answers
  vector<double> table(cpt_new->table());
  vector<double> answer({0.8, 0.3});
  delete cpt_new;
  return compare_vectors<double>(table, answer, 0.001);
}

bool TestCPT::remove_evidence4(bool debug) {
  // Initialize CPTs
  Domain d_bin(vector<string>({"0", "1"}), 0, "binary");
  Domain d_evid(vector<string>({"e"}), 1, "evidence");
  vector<Variable> vars = assign_variables(vector<Domain*>({&d_bin, &d_bin, &d_bin, &d_bin}));
  vector<Variable> evid_vars = assign_variables(vector<Domain*>({&d_evid, &d_evid}));
  CPT cpt = create_cpt(vars, vector<int>({0, 1}), vector<int>({2, 3}));
  cpt.table() = vector<double>({0.3, 0.25, 0.5, 0.3, 0.15, 0.35, 0.1, 0.3,
                                0.4, 0.3, 0.2, 0.15, 0.15, 0.1, 0.1, 0.45});
  // Print CPTs if debug option is on
  if (debug) cout << "CPT" << cpt.str() << endl;
  // Reduce cpt
  vars[0].val()=1;
  vars[3].val()=1;
  CPT* cpt_new = cpt.removeEvidence(vector<Variable*>{&evid_vars[0], &evid_vars[1]});
  // Print table if debug option is on
  if (debug) cout << "CPT_new" << cpt_new->str() << endl;
  // Store and compare answers
  vector<double> table(cpt_new->table());
  vector<double> answer({0.3, 0.15, 0.10, 0.45});
  delete cpt_new;
  return compare_vectors<double>(table, answer, 0.001);
}

void TestCPT::run_all_tests(bool debug) {
  cout << "=================================" << endl;
  cout << "Testing Function.h (CPT)" << endl;
  cout << "=================================" << endl;
  cout << "Passed normalize1(): " << normalize1(debug) << endl;
  cout << "Passed multiply_and_marginalize1(): " << multiply_and_marginalize1(debug) << endl;
  cout << "Passed multiply_and_marginalize2(): " << multiply_and_marginalize2(debug) << endl;
  cout << "Passed multiply_and_marginalize3(): " << multiply_and_marginalize3(debug) << endl;
  cout << "Passed multiply_and_marginalize4(): " << multiply_and_marginalize3(debug) << endl;
  cout << "Passed remove_evidence1(): " << remove_evidence1(debug) << endl;
  cout << "Passed remove_evidence2(): " << remove_evidence2(debug) << endl;
  cout << "Passed remove_evidence3(): " << remove_evidence3(debug) << endl;
  cout << "Passed remove_evidence4(): " << remove_evidence4(debug) << endl;
}

/***********************
Miscellaneous Functions
***********************/
vector<Variable> assign_variables(vector<Domain*> domains, vector<string> names, vector<int> ids) {
  vector<Variable> result;
  for(int d=0; d<domains.size(); d++) {
    int id = (ids.size()%domains.size())==0 ? (d+1) : ids[d%domains.size()] ;
    string name = (names.size()%domains.size()==0) ? "v"+to_string(id) : names[d%domains.size()];
    Variable var(*domains[d], id, name);
    result.push_back(var);
  }
  return result;
}

CPT create_cpt(vector<Variable>& vars, vector<int> marg_indexes, vector<int> cond_indexes) {
  vector<Variable*> marg_scope, cond_scope;
  set<int> used_indexes;
  for(int m=0; m<marg_indexes.size(); m++) {
    int marg_index = marg_indexes[m];
    if (marg_index>=vars.size() || used_indexes.find(marg_index)!=used_indexes.end())
      continue;
    marg_scope.push_back(&vars[marg_index]);
    used_indexes.insert(marg_index);
  }
  for(int c=0; c<cond_indexes.size(); c++) {
    int cond_index = cond_indexes[c];
    if (cond_index>=vars.size() || used_indexes.find(cond_index)!=used_indexes.end())
      continue;
    cond_scope.push_back(&vars[cond_index]);
    used_indexes.insert(cond_index);
  }
  return CPT(marg_scope, cond_scope);
}
