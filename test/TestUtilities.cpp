#include "TestUtilities.h"

using namespace std;

bool TestUtilities::vector_union_test1(bool debug) {
  vector<int> a({1,2,3,4,5}), b({2,4,6,7,8}), c({1,2,3,4,5,6,7,8}), d;
  d = vector_union<int>(a, b);
  if (debug) cout << endl << "[ Expected: " << to_string_vec<int>(c) <<
                      ", Actual: " << to_string_vec<int>(d) << " ]" << endl;
  return c==d;
}

bool TestUtilities::vector_intersection_test1(bool debug) {
  vector<int> a({1,2,3,4,5}), b({2,4,6,7,8}), c({2,4}), d;
  d = vector_intersection<int>(a, b);
  if (debug) cout << endl << "[ Expected: " << to_string_vec<int>(c) <<
                      ", Actual: " << to_string_vec<int>(d) << " ]" << endl;
  return c==d;
}

bool TestUtilities::vector_difference_test1(bool debug) {
  vector<int> a({1,2,3,4,5}), b({2,4,6,7,8}), c({1,3,5}), d;
  d = vector_difference<int>(a, b);
  if (debug) cout << endl << "[ Expected: " << to_string_vec<int>(c) <<
                      ", Actual: " << to_string_vec<int>(d) << " ]" << endl;
  return c==d;
}

bool TestUtilities::vector_difference_test2(bool debug) {
  vector<int> a({1,2,3,4,5}), b({2,4,6,7,8}), c({6,7,8}), d;
  d = vector_difference<int>(b, a);
  if (debug) cout << endl << "[ Expected: " << to_string_vec<int>(c) <<
                      ", Actual: " << to_string_vec<int>(d) << " ]" << endl;
  return c==d;
}

void TestUtilities::run_all_tests(bool debug) {
  cout << "=================================" << endl;
  cout << "Testing Utilities.h" << endl;
  cout << "=================================" << endl;
  cout << "Passed vector_union_test1(): " << vector_union_test1(debug) << endl;
  cout << "Passed vector_intersection_test1(): " << vector_intersection_test1(debug) << endl;
  cout << "Passed vector_difference_test1(): " << vector_difference_test1(debug) << endl;
  cout << "Passed vector_difference_test2(): " << vector_difference_test2(debug) << endl;
}
