#include "TestCDNet.h"

#include <utility>

/**************
Data-based Tests
***************/
bool TestCDNet::data_test(string infile_path, vector<int>& x_indices, vector<pair<int,int> >& evids, int max_cond_depth,
                          int max_depth, int clt_threshold, vector<double> probs, double precision, bool debug) {
    // Import dataset and create variables
    Dataset data(infile_path);
    vector<Domain*> doms(data.size());
    vector<Variable*> vars(data.size());
    for(int v=0; v<vars.size(); v++) {
        doms[v] = data[v].toDomain(v, "d"+to_string(v));
        vars[v] = new Variable(*doms[v], v, "v"+to_string(v));
    }
    // Create prior model
    if (debug) cout << "[DEBUG] Training prior ..." << endl;
    CNet* prior = create_cnet(data, vars, max_depth, clt_threshold);
    // Create transition model
    if (debug) cout << "[DEBUG] Training transition ..." << endl;
    CDNet transition = create_cdnet(data, vars, x_indices, max_cond_depth, max_depth, clt_threshold);
    // Multiply and marginalize into new model
    if (debug) cout << "[DEBUG] Multiplying and marginalizing ..." << endl;
    CNet& msg = *(transition.multiplyAndMarginalize(*prior, evids));
    // Check marginal probability of evidence in new model
    vector<Variable*> msg_vars(msg.vars().begin(), msg.vars().end()-1);
    set_all_to_zero(msg_vars);
    bool match = true;
    for(int i=0; i<var_vector_size(msg_vars); i++) {
        match = match && are_equal(msg.quickProbEvid(), probs[i], precision);
        increment_vars(msg_vars);
    }
    return match;
}

bool TestCDNet::data1_gen_test(double precision, bool debug) {
    // Set model parameters
    string infile_path = "../data/temporal1.csv";
    int max_cond_depth=2;
    int max_depth=1;
    int clt_threshold=1;
    // Set indices, evidences and answers
    vector<int> x_indices({2,3});
    vector<pair<int,int > > evids;
    vector<double> answers({0.22, 0.16, 0.20, 0.42});
    // Call data_test
    return data_test(infile_path, x_indices, evids, max_cond_depth, max_depth,
                     clt_threshold, answers, precision, debug);
}

bool TestCDNet::data1_disc_test(double precision, bool debug) {
    // Set model parameters
    string infile_path = "../data/temporal1.csv";
    int max_cond_depth=2;
    int max_depth=1;
    int clt_threshold=1;
    // Set indices, evidences and answers
    vector<int> x_indices({2,3});
    vector<pair<int,int > > evids1 = zip(vector<int>({1}), vector<int>({0}));
    vector<pair<int,int > > evids2 = zip(vector<int>({1}), vector<int>({1}));
    vector<double> answers1({0.185, 0.195, 0.240, 0.380});
    vector<double> answers2({0.25, 0.12, 0.17, 0.46});
    // Call data_test
    bool test1 = data_test(infile_path, x_indices, evids1, max_cond_depth, max_depth,
                     clt_threshold, answers1, precision, debug);
    bool test2 = data_test(infile_path, x_indices, evids2, max_cond_depth, max_depth,
                           clt_threshold, answers2, precision, debug);
    return test1 && test2;
}

/**************
Run All Tests
***************/
void TestCDNet::run_all_tests(bool debug, double precision) {
    cout << "=================================" << endl;
    cout << "Testing CDNet.h " << endl;
    cout << "=================================" << endl;
    cout << "Passed data1_gen_test: " << data1_gen_test(precision, debug) << endl;
    cout << "Passed data1_disc_test: " << data1_disc_test(precision, debug) << endl;
}

/**************
Misc Functions
***************/
CDNet& create_cdnet(string infile_path, vector<int> x_indices, int max_cond_depth,
                    int max_depth, int clt_threshold, bool smooth) {
    CDNet* cdnet = nullptr;
    Dataset data(move(infile_path));
    cdnet = new CDNet(data, move(x_indices), max_cond_depth, max_depth, clt_threshold, smooth);
    return *cdnet;
}

CDNet& create_cdnet(Dataset& data, vector<Variable*>& vars, vector<int> x_indices,
                    int max_cond_depth, int max_depth, int clt_threshold, bool smooth) {
    return *(new CDNet(data, vars, move(x_indices), max_cond_depth, max_depth, clt_threshold, smooth));
}