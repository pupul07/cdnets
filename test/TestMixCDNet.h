#ifndef TESTMIXCDNET_H
#define TESTMIXCDNET_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../src/Dataset.h"
#include "../src/DCN.h"
#include "TestCNet.h"

using namespace std;

// Permissible error bound for computing probabilities
const double PREC_MIXCDNET = 0.01;

class TestMixCDNet {
public:
    // Data Test
    static bool temporal_data_test(string infile_path, string infile_seq_path, int max_depth_prior,
                                    int clt_threshold_prior, int max_cond_depth, int max_depth,
                                    int clt_threshold, int maxiter, vector<double> answers,
                                    unsigned int seed=1991,double precision=PREC_MIXCDNET, bool debug=false);
    static bool temporal_data_test1(double precision=PREC_MIXCDNET, bool debug=false);
    // Run all Tests
    static void run_all_tests(bool=false, double=PREC_MIXCDNET);
};

#endif
