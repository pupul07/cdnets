#include "TestVariable.h"

using namespace std;

/**************
 Domain
**************/
bool TestDomain::create_test(vector<string>& vec) {
  Domain dom(vec);
  if (vec.size() != dom.size())
    return false;
  for(int i=0; i<vec.size(); i++) {
    if(vec[i] != dom[i])
      return false;
  }
  return true;
}

bool TestDomain::create_test1() {
  vector<string> vec({"0","1","2","3","4","5","6","7"});
  return create_test(vec);
}

void TestDomain::run_all_tests() {
  cout << "Passed create_test1(): " << create_test1() << endl;
}
