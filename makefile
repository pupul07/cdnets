GPP=g++
BIN=./bin
LIB=./lib
SRC=./src
TEST=./test
OPTS=-std=c++11 -fsanitize=address
#OPTS=-std=c++11 -ggdb3

all: Main

# Executables
Main: DCN.o
	${GPP} ${OPTS} ${BIN}/Utilities.o ${BIN}/Variable.o ${BIN}/Function.o \
		${BIN}/Dataset.o ${BIN}/CLT.o ${BIN}/CNet.o ${BIN}/CDNet.o ${BIN}/DCN.o \
		${BIN}/MixCDNet.o ${BIN}/AndCDNet.o ${BIN}/MixAndCDNet.o ${SRC}/Main.cpp -o ${BIN}/Main

MainMix: DCN.o
	${GPP} ${OPTS} ${BIN}/Utilities.o ${BIN}/Variable.o ${BIN}/Function.o \
		${BIN}/Dataset.o ${BIN}/CLT.o ${BIN}/CNet.o ${BIN}/CDNet.o ${BIN}/DCN.o \
		${BIN}/MixCDNet.o ${BIN}/AndCDNet.o ${BIN}/MixAndCDNet.o ${SRC}/MainMix.cpp -o ${BIN}/MainMix

Test: TestVariable.o TestFunction.o TestUtilities.o TestDataset.o TestCLT.o TestCNet.o TestCDNet.o TestDCN.o
	${GPP} ${OPTS} ${BIN}/Variable.o ${BIN}/Function.o ${BIN}/Utilities.o \
	 			${BIN}/TestVariable.o ${BIN}/TestFunction.o ${BIN}/TestUtilities.o ${BIN}/TestDataset.o \
				${BIN}/TestCLT.o ${BIN}/TestCNet.o ${BIN}/TestCDNet.o ${BIN}/TestDCN.o \
				${BIN}/TestMixCDNet.o ${BIN}/TestAndCDNet.o ${TEST}/Test.cpp -o ${BIN}/Test

Scratch: DCN.o
	${GPP} ${OPTS} ${BIN}/Utilities.o ${BIN}/Variable.o ${BIN}/Function.o \
		${BIN}/Dataset.o ${BIN}/CLT.o ${BIN}/CNet.o ${BIN}/CDNet.o ${BIN}/DCN.o \
		${BIN}/MixCDNet.o ${BIN}/AndCDNet.o ${SRC}/Scratch.cpp -o ${BIN}/Scratch

# Dependencies
DCN.o: MixCDNet.o MixAndCDNet.o
	${GPP} ${OPTS} -c ${SRC}/DCN.cpp -o ${BIN}/DCN.o

MixAndCDNet.o: AndCDNet.o
	${GPP} ${OPTS} -c ${SRC}/MixAndCDNet.cpp -o ${BIN}/MixAndCDNet.o

AndCDNet.o: CDNet.o
	${GPP} ${OPTS} -c ${SRC}/AndCDNet.cpp -o ${BIN}/AndCDNet.o

MixCDNet.o: CDNet.o
	${GPP} ${OPTS} -c ${SRC}/MixCDNet.cpp -o ${BIN}/MixCDNet.o

CDNet.o: CNet.o
	${GPP} ${OPTS} -c ${SRC}/CDNet.cpp -o ${BIN}/CDNet.o

CNet.o: CLT.o
	${GPP} ${OPTS} -c ${SRC}/CNet.cpp -o ${BIN}/CNet.o

CLT.o: Function.o Dataset.o
	${GPP} ${OPTS} -c ${SRC}/CLT.cpp -o ${BIN}/CLT.o

Function.o: Variable.o
	${GPP} ${OPTS} -c ${SRC}/Function.cpp -o ${BIN}/Function.o

Dataset.o: Utilities.o Variable.o Function.o
	${GPP} ${OPTS} -c ${SRC}/Dataset.cpp -o ${BIN}/Dataset.o

Variable.o: Utilities.o
	${GPP} ${OPTS} -c ${SRC}/Variable.cpp -o ${BIN}/Variable.o

Utilities.o:
	${GPP} ${OPTS} -c ${SRC}/Utilities.cpp -o ${BIN}/Utilities.o

# Testing
TestVariable.o: Variable.o
	${GPP} ${OPTS} -c ${TEST}/TestVariable.cpp -o ${BIN}/TestVariable.o

TestFunction.o: Function.o
	${GPP} ${OPTS} -c ${TEST}/TestFunction.cpp -o ${BIN}/TestFunction.o

TestUtilities.o: Utilities.o
	${GPP} ${OPTS} -c ${TEST}/TestUtilities.cpp -o ${BIN}/TestUtilities.o

TestCLT.o: Utilities.o Function.o CLT.o
	${GPP} ${OPTS} -c ${TEST}/TestCLT.cpp -o ${BIN}/TestCLT.o

TestCNet.o: CNet.o
	${GPP} ${OPTS} -c ${TEST}/TestCNet.cpp -o ${BIN}/TestCNet.o

TestCDNet.o: CDNet.o
	${GPP} ${OPTS} -c ${TEST}/TestCDNet.cpp -o ${BIN}/TestCDNet.o

TestMixCDNet.o: MixCDNet.o
	${GPP} ${OPTS} -c ${TEST}/TestMixCDNet.cpp -o ${BIN}/TestMixCDNet.o

TestAndCDNet.o: AndCDNet.o
	${GPP} ${OPTS} -c ${TEST}/TestAndCDNet.cpp -o ${BIN}/TestAndCDNet.o

TestDCN.o: DCN.o
	${GPP} ${OPTS} -c ${TEST}/TestDCN.cpp -o ${BIN}/TestDCN.o

TestDataset.o: Dataset.o
	${GPP} ${OPTS} -c ${TEST}/TestDataset.cpp -o ${BIN}/TestDataset.o

# Other Targets
clean:
	rm -rf ${BIN}/*.o
