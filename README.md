# README #

Code to train and perform inference on Cutset Networks written using C++11. Support for conditional and dynamic models will be added soon.

### Build Instructions ###

1. Navigate to project directory: `cd path/to/cdnets`
2. Build the project using the following command: `make Main`
3. Run `bin/Main data/data1.csv` from the command line

### Usage Examples ###
1. **Learning a Cutset Network from data**
The example below creates a cutset network from a dataset and prints it.
*Example:*
`string infile = "path/to/data.csv";`
`Dataset data(infile);`
`CNet cnet(data);`
`std::cout << cnet.str() << endl;`

2. **Average Log-Likelihood**
The example below calculates the average log-likelihood of a cutset network w.r.t a dataset.
*Example:*
`string infile = "path/to/data.csv";`
`Dataset data(infile);`
`double ll = cnet.avgLL(data);`

3. **P(evid):**
The example below calculates the probability P(v1=0).
*Example:*
`vector<int,int> evid = zip(vector<int>({1}),vector<int>({0}));`
`double prob = cnet.probEvid(evid);`

4. **P(marg|evid):**
The example below calculates the marginal probability P(v2=1|v1=0).
*Example:*
`vector<int,int> marg = zip(vector<int>({2}),vector<int>({1}));`
`vector<int,int> evid = zip(vector<int>({1}),vector<int>({0}));`
`double prob = cnet.probEvid(marg,evid);` 

5. **MPE:**
The example below calculates the Most Probable Explanation given evidence v1=0.
`vector<int,int> evid = zip(vector<int>({1}),vector<int>({0}));`
`pair<double,vector<int> > mpe = cnet.MPE(evid);` 

6. **Sampling with Likelihood-Weighting:**
The example below calculates 10 samples from the posterior distribution P(v0,v2|v1=0) using likelihood-weighting.
*Example:*
`vector<int,int> evid = zip(vector<int>({1}),vector<int>({0}));`
`vector<pair<double,vector<int> > > samples = cnet.samples(evid, 10);` 
