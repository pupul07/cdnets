#ifndef CDNET_H
#define CDNET_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include "Dataset.h"
#include "CNet.h"
#include "Utilities.h"

using namespace std;

struct DecisionNode {
public:
    Variable* x_var;
    vector<DecisionNode*> children;
    CNet* data;
    // Methods
    // Constructors
    DecisionNode(Variable* x_var, vector<DecisionNode*> children, CNet* data);
    DecisionNode(DecisionNode& d);
    // Validation
    bool isValid();
    bool isCNet();
    // Inference
    double quickProbEvid(bool debug=false);
    pair<double,vector<int> > quickMPE(bool debug=false);
    void quickSample(bool debug=false);
    // Learning
    void updateParams(Dataset& data, vector<Variable*>& existing_vars, unordered_map<Variable*,int>& idx_map,
                      vector<int>& idx_to_remove, vector<pair<int,string> >& curr_filters,
                      int max_depth, int clt_threshold);
    // Helper Methods
    void multiplyAndMarginalize(CNet& new_cnet, CNet& x_dist, double& Z);
    // String representation
    string str(int level, int spacing, string val, bool verbose);
    // Destructor
    ~DecisionNode();
};

class CDNet {
    protected:
        bool _import_vars;
        vector<Domain*> _doms;
        vector<Variable*> _vars;
        vector<Variable*> _y_vars;
        vector<Variable*> _x_vars;
        DecisionNode* _root{};
        int _nleaves;
        // Helper methods
        static DecisionNode* learnCDNetHelper(Dataset& data, vector<int> x_indices, vector<Variable*> curr_vars,
                                              vector<Variable*>& y_vars, int curr_depth, int max_depth,
                                              int max_cnet_depth, int clt_threshold, int& nleaves, bool smooth=true);
    public:
        // Constructors
        CDNet(Dataset& data, vector<int> x_indices, int max_cond_depth=5,
                int max_depth=5, int clt_threshold=5, bool smooth=true);
        CDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int> x_indices,
                int max_cond_depth=5, int max_depth=5, int clt_threshold=5, bool smooth=true);
        CDNet(vector<Domain*>& doms, vector<Variable*>& vars, vector<Variable*>& y_vars,
                vector<Variable*>& x_vars, DecisionNode* root, int nleaves);
        // Access
        DecisionNode* root();
        vector<Domain*>& doms();
        vector<Variable*>& vars();
        vector<Variable*>& y_vars();
        vector<Variable*>& x_vars();
        int nleaves();
        // Learning
        void learnCDNet(Dataset& data, vector<int> x_indices, int max_cond_depth,
                        int max_depth=5, int clt_threshold=5, bool smooth=true);
        void learnCDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int> x_indices, int max_cond_depth,
                    int max_depth=5, int clt_threshold=5, bool smooth=true);
        void updateParams(Dataset& data, vector<Variable*>& existing_vars, vector<int> x_indices,
                            int max_depth, int clt_threshold);
        // Inference
        double LL(vector<string>, vector<int> = vector<int>({}), bool=false);
        double avgLL(Dataset&, bool=false);
        double probEvid(vector<pair<int, int> > evids, bool debug=false);
        double margEvid(vector<pair<int, int> > margs, vector<pair<int, int> > evids, bool debug=false);
        pair<double, vector<int> > MPE(vector<pair<int, int> > evids, bool debug=false);
        // Sampling
        pair <double, vector<int> > sample(vector<pair<int, int> > evids, bool debug=false);
        vector <pair <double, vector<int> > > samples(vector<pair<int, int> > evids, int m=100, bool debug=false);
        // Marginalize
        CNet* quickMultiplyAndMarginalize(CNet& x_dist);
        CNet* multiplyAndMarginalize(CNet& x_dist, vector<pair<int,int> > evids);
        // String
        string str(bool verbose=false, int spacing=3);
        // Destructor
        ~CDNet();
};

#endif
