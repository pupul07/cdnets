#ifndef MIXANDCDNET_H
#define MIXANDCDNET_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include "Dataset.h"
#include "AndCDNet.h"

using namespace std;

class MixAndCDNet {
protected:
    bool _import_vars;
    vector<double> _probs;
    vector<AndCDNet*> _andcdnets;
    vector<Domain*> _doms;
    vector<Variable*> _vars;
    vector<Variable*> _y_vars;
    vector<Variable*> _x_vars;
    int _nleaves;
public:
    // Constructors
    MixAndCDNet(Dataset& data, vector<int>& x_indices, int max_cond_depth, int max_depth,
                int clt_threshold, int maxiter, int num_comps);
    MixAndCDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices, int max_cond_depth,
                int max_depth, int clt_threshold, int maxiter, int num_comps);
    // Access
    bool& import_vars();
    vector<double>& probs();
    vector<AndCDNet*>& andcdnets();
    vector<Domain*>& doms();
    vector<Variable*>& vars();
    vector<Variable*>& y_vars();
    vector<Variable*>& x_vars();
    int nleaves();
    // Inference
    double avgLL(Dataset& data);
    double probEvid(vector<pair<int, int> >& evids);
    // Learning
    void learn(Dataset& data, vector<int>& x_indices, int max_cond_depth, int max_depth,
                int clt_threshold, int maxiter, int num_comps);
    void learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices, int max_cond_depth,
                int max_depth, int clt_threshold, int maxiter, int num_comps);
    // String Representation
    string str(bool verbose=false, int spacing=3);
    ~MixAndCDNet();
};

#endif
