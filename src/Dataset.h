#ifndef DATASET_H_
#define DATASET_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <unordered_set>
#include <math.h>
#include "Variable.h"
#include "Function.h"
#include "Utilities.h"

using namespace std;

template <typename T>
class DiscreteColumn {
  protected:
    string _name;
    vector <T> _vals;
    map < T, set<int> > val_counts;
  public:
    explicit DiscreteColumn(string name="untitled");
    explicit DiscreteColumn(vector<T> data, string name="untitled");
    DiscreteColumn<T> filterByIndex(vector<int> indices);
    DiscreteColumn<T> filterByValue(vector<T> values);
    DiscreteColumn<T>* removeByIndex(vector<int> indices, bool to_sort=false);
    void append(vector<T> data);
    void append(DiscreteColumn<T> col);
    string name();
    vector<T>& vals();
    int size();
    Domain* toDomain(int=0,string="d0");
    T &operator [](int i);
    set<int> indexesOf(T val);
    vector<T> uniqueVals();
    int count(T key);
    void insert(T val, int i);
    void push(T val);
    void erase(int i);
    T pop();
    const string str(bool verbose=false);
};

class Dataset {
  protected:
    vector < DiscreteColumn<string>* > _cols;
    vector<double> _weights;
  public:
    Dataset(string infile_path, string delim=",", bool skip_header=false, bool debug=false);
    explicit Dataset(vector<DiscreteColumn<string>*> cols);
    Dataset(Dataset&);
    int size();
    int nrows();
    vector<double>& weights();
    vector < DiscreteColumn<string>* >& cols();
    DiscreteColumn<string> &operator [](int i);
    vector<string> row(int i);
    Dataset* filterByValues(vector<pair<int,string> > value_pairs);
    vector<int> filterByValuesOnly(vector<pair<int,string> > value_pairs);
    Dataset* dropCols(set<int>);
    vector< vector<double> > MIMatrix(bool self_zero=true);
    string subset(int start=0, int length=-1);
    const string str();
    string head();
    string tail();
    ~Dataset();
};

template <typename T>
double MI(DiscreteColumn<T>&, DiscreteColumn<T>&, vector<double>&);
Function* discreteColsToFunction(vector<Variable*>&, Dataset&, vector<int>, bool=true);
CPT* discreteColsToCPT(vector<Variable*>&, Dataset&, vector<int>, vector<int>, bool=true);
set < pair<int,int> > maxSpanTree(vector < vector<double> >&);
vector<int> extract_var_val_indices(vector<string>&, vector<Variable*>&);
int reorder_by_evidence(Dataset& data, vector<int>& evid_indices, bool to_sort=false);

/***************************
  Functions
****************************/
template <typename T>
double MI(DiscreteColumn<T>& x, DiscreteColumn<T>& y, vector<double>& weights) {
  /* double Z_x = x.size();
  double Z_y = y.size();
  double Z_xy = (Z_x<Z_y) ? Z_x : Z_y; */
  double Z_x = accumulate(weights.begin(),weights.end(),0.0);
  double Z_y = Z_x;
  double Z_xy = Z_x;
  vector<T> x_vals = x.uniqueVals();
  vector<T> y_vals = y.uniqueVals();
  double mi = 0.0;
  for(int i=0; i<x_vals.size(); i++) {
    for(int j=0; j<y_vals.size(); j++) {
      // int xy_counts = (x.indexesOf(x_vals[i]).intersection(y.indexesOf(y_vals[j]))).size();
      set<int> x_rows = x.indexesOf(x_vals[i]);
      set<int> y_rows = y.indexesOf(y_vals[j]);
      set<int> xy_rows = quick_intersection<int>(x_rows, y_rows);
      vector<int> x_rows_vec(x_rows.begin(), x_rows.end());
      vector<int> y_rows_vec(y_rows.begin(), y_rows.end());
      vector<int> xy_rows_vec(xy_rows.begin(), xy_rows.end());
      double weight_xy = sumWeights(weights, xy_rows_vec);
      double weight_x = sumWeights(weights, x_rows_vec);
      double weight_y = sumWeights(weights, y_rows_vec);
      double p_xy = (Z_xy==0.0) ? 0.0 : weight_xy / Z_xy;
      double p_x = (Z_x==0.0) ? 0.0 : weight_x / Z_x;
      double p_y = (Z_y==0.0) ? 0.0 : weight_y / Z_y;
      /* double p_xy = (quick_intersection<int>(x_rows, y_rows).size()/Z_xy);
      double p_x = x.count(x_vals[i])/Z_x;
      double p_y = y.count(y_vals[j])/Z_y; */
      /* cout << "p_" << x_vals[i] << y_vals[j] << ": " << p_xy << endl;
      cout << "p_" << x_vals[i] << ": " << p_x << endl;
      cout << "p_" << y_vals[j] << ": " << p_y << endl; */
      mi += ((p_xy)==0.0) ? 0.0 : p_xy * log(p_xy/(p_x * p_y));
      // cout << "mi: " << mi << endl;
    }
  }
  return mi;
}

vector< vector<int> > partition_vars(Dataset& data, int max_size=1);
vector<Domain*> readDomsFromFile(string infile_path, string delim=",", bool trans_flag=false, bool debug=false);

/***************************
  DiscreteColumn
****************************/
template <typename T>
DiscreteColumn<T>::DiscreteColumn(string name) {
  _name = name;
}

template <typename T>
DiscreteColumn<T>::DiscreteColumn(vector<T> data, string name) {
  _name = name;
  append(data);
}

template <typename T>
DiscreteColumn<T> DiscreteColumn<T>::filterByIndex(vector<int> indices) {
  vector<T> data;
  for(int i=0; i<indices.size(); i++) {
    if (indices[i]<size())
      data.push_back(vals()[indices[i]]);
  }
  return DiscreteColumn<T>(data, name());
}

template <typename T>
DiscreteColumn<T> DiscreteColumn<T>::filterByValue(vector<T> values) {
  set<int> indices;
  for(int v=0; v<values.size(); v++) {
    set<int> val_indices = indexesOf(values[v]);
    if (indices.size()==0)
      indices = val_indices;
    else
      indices = quick_union(indices, val_indices);
  }
  return filterByIndex(vector<int>(indices.begin(), indices.end()));
}

template <typename T>
DiscreteColumn<T>* DiscreteColumn<T>::removeByIndex(vector<int> indices, bool to_sort) {
    vector<T> data = vals();
    removeIndices(data, indices, to_sort);
    return new DiscreteColumn<T>(data, name());
}

template <typename T>
void DiscreteColumn<T>::append(vector<T> data) {
  int org_size = vals().size();
  vals().insert(vals().end(), data.begin(), data.end());
  for(int i=org_size; i<vals().size(); i++) {
    if ( val_counts.find(vals()[i]) == val_counts.end() )
      val_counts[vals()[i]] = set<int>();
    val_counts[vals()[i]].insert(i);
  }
}

template <typename T>
void DiscreteColumn<T>::append(DiscreteColumn<T> col) {
  append(col.vals());
}

template <typename T>
string DiscreteColumn<T>::name() {
  return _name;
}

template <typename T>
vector<T>& DiscreteColumn<T>::vals() {
    return _vals;
}

template <typename T>
int DiscreteColumn<T>::size() {
  return vals().size();
}

template <typename T>
Domain* DiscreteColumn<T>::toDomain(int id, string name) {
  vector<string> keys;
  for(auto const& pair: val_counts) {
    keys.push_back(to_string(pair.first));
  }
  return new Domain(keys, id, name);
}

template <typename T>
T &DiscreteColumn<T>::operator [](int i) {
  return vals()[i];
}

template <typename T>
set<int> DiscreteColumn<T>::indexesOf(T val) {
  if ( val_counts.find(val) == val_counts.end() )
    return set<int>();
  return val_counts[val];
}

template <typename T>
vector<T> DiscreteColumn<T>::uniqueVals() {
  vector<T> unique_vals;
  for(auto const& pair: val_counts) {
    unique_vals.push_back(pair.first);
  }
  return unique_vals;
}

template <typename T>
int DiscreteColumn<T>::count(T key) {
  if ( val_counts.find(key) == val_counts.end() )
    return 0;
  return val_counts[key].size();
}

template <typename T>
void DiscreteColumn<T>::insert(T val, int i) {
  // Insertion
  if (i>0 && i<=vals().size()-1)
    vals().insert(vals().begin()+i,val);
  else
    vals().push_back(val);
  // Bookkeeping
  if ( val_counts.find(val) == val_counts.end() )
    val_counts[val] = set<int>();
  val_counts[val].insert(i);
}

template <typename T>
void DiscreteColumn<T>::push(T val) {
  insert(val, vals().size());
}

template <typename T>
void DiscreteColumn<T>::erase(int i) {
  if (i>=vals().size())
    return;
  // Bookkeeping
  val_counts[vals()[i]].erase(i);
  if (val_counts[vals()[i]].size() == 0)
    val_counts.erase(vals()[i]);
  // Deletion
  vals().erase(vals().begin()+i);
}

template <typename T>
T DiscreteColumn<T>::pop() {
  if (vals().size()==0)
    throw "SizeError: DiscreteColumn instance is empty!";
  T last = vals()[vals().size()-1];
  erase(vals().size()-1);
  return last;
}

template <typename T>
const string DiscreteColumn<T>::str(bool verbose) {
  string docstr = "<DiscreteColumn '" + _name + "'>: " + to_string(vals().size());
  if (verbose)
    return docstr + " [ " + to_string_map_size< T,set<int> >(val_counts) + " ] { " + to_string_vec<T>(vals()) + " }";
  return docstr;
}

#endif
