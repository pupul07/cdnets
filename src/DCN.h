#ifndef DCN_H
#define DCN_H

#include <iostream>
#include <algorithm>
#include <vector>
#include "Dataset.h"
#include "CNet.h"
#include "CDNet.h"
#include "MixCDNet.h"
#include "MixAndCDNet.h"
#include "Utilities.h"

using namespace std;

template <typename Model>
class DCN {
    protected:
        CNet* _prior_dist;
        Model* _transition_dist;
        vector<Domain*> _doms;
        vector<Variable*> _vars;
        vector<Variable*> _prior_vars;
        vector<Variable*> _transition_vars;
    public:
        // Constructors
        DCN(Dataset& data, vector<int>& seqs, int max_depth_prior, int clt_threshold_prior,
                vector<int>& model_params, vector<Domain*> existing_doms=vector<Domain*>({}), bool debug=true);
        // Access
        CNet* prior_dist();
        Model* transition_dist();
        vector<Domain*>& doms();
        vector<Variable*>& vars();
        vector<Variable*>& prior_vars();
        vector<Variable*>& transition_vars();
        // Inference
        /* double forwardLL(Dataset& data, vector<int> test_seqs=vector<int>(),
                        vector<int> evid_indices=vector<int>(), bool debug=true); */
        double forwardLL(Dataset& data, vector<int> test_seqs=vector<int>(), bool debug=true);
        // Destructor
        ~DCN();
};

Dataset* createTransitionData(Dataset& data, vector<int> seq);
template <typename Model>
Model* getTransitionDist(Dataset& data, vector<Variable*>& vars,
                            vector<int>& x_indices, vector<int>& model_params);

/*************************
DYNAMIC CUTSET NETWORK
**************************/
/*************************
DCN: Constructors
**************************/
template <typename Model>
DCN<Model>::DCN(Dataset& data, vector<int>& seqs, int max_depth_prior, int clt_threshold_prior,
                vector<int>& model_params, vector<Domain*> existing_doms, bool debug) {
    if (debug) cout << "[DEBUG] *** Learning DCN" << endl;
    // Create variables
    doms() = vector<Domain*>(data.size()*2);
    vars() = vector<Variable*>(data.size()*2);
    int V = data.size();
    for(int v=0; v<V; v++) {
        doms()[v] = (existing_doms.size()!=doms().size()) ? data[v].toDomain(v, "d" + to_string(v)) : existing_doms[v];
        doms()[v + V] = (existing_doms.size()!=doms().size()) ? data[v].toDomain(v + V, "d" + to_string(v + V)) : existing_doms[v+V];
        vars()[v] = new Variable(*doms()[v], v, "v"+to_string(v));
        vars()[v+V] = new Variable(*doms()[v+V], v+V, "v"+to_string(v+V));
    }
    // Learn prior distribution
    if (debug) cout << "[DEBUG] * Learning prior" << endl;
    prior_vars() = vector<Variable*>(vars().begin()+V, vars().begin()+(2*V));
    _prior_dist = new CNet(data, prior_vars(), max_depth_prior, clt_threshold_prior);
    // Create transition dataset
    transition_vars() = vector<Variable*>(vars().begin(), vars().begin()+V);
    Dataset* transition_data = createTransitionData(data, seqs);
    // Learn transition distribution
    if (debug) cout << "[DEBUG] * Learning transition" << endl;
    vector<int> x_indices(data.size());
    iota(x_indices.begin(), x_indices.end(), data.size());
    // Choose model to use for distribution
    _transition_dist = getTransitionDist<Model>(*transition_data, vars(), x_indices, model_params);
    delete transition_data;
}

template <typename Model>
DCN<Model>::~DCN() {
    deleteVec(vars());
    deleteVec(doms());
    delete _prior_dist;
    delete _transition_dist;
}

/*************************
DCN: Access
**************************/
template <typename Model>
CNet* DCN<Model>::prior_dist() {
    return _prior_dist;
}

template <typename Model>
Model* DCN<Model>::transition_dist() {
    return _transition_dist;
}

template <typename Model>
vector<Domain*>& DCN<Model>::doms() {
    return _doms;
}

template <typename Model>
vector<Variable*>& DCN<Model>::vars() {
    return _vars;
}

template <typename Model>
vector<Variable*>& DCN<Model>::prior_vars() {
    return _prior_vars;
}

template <typename Model>
vector<Variable*>& DCN<Model>::transition_vars() {
    return _transition_vars;
}

/*************************
DCN: Inference
**************************/
/* double DCN::forwardLL(Dataset& data, vector<int> test_seqs, vector<int> evid_indices, bool debug) {
    if (debug) cout << "[DEBUG] *** Forward Algorithm with LL scores" << endl;
    // Do nothing if dataset is empty
    if (!data.nrows())
        return 1.0;
    // If test sequences is empty, then just set it to number of rows in dataset
    test_seqs = (test_seqs.size()) ? test_seqs : vector<int>({data.nrows()});
    // Initialize variables
    double ll, total_ll = 0.0;
    int r = 0;
    // Start processing each sequence in order
    for (int s=0; s<test_seqs.size(); s++) {
        if (debug) cout << "Processing sequence " << s+1 << " (Size: " << test_seqs[s] << ")" << endl;
        // Calculate LL for point 0
        ll = prior_dist()->LL(data.row(r+0), evid_indices);
        total_ll += ll;
        if (debug) cout << "t=0: " << ll << endl;
        // Create transition map for substitution
        unordered_map<Variable *, Variable *> transition_prior_map = zipMap(transition_vars(), prior_vars());
        // Calculate LL for all other points
        CNet* prev_msg = new CNet(*prior_dist());
        // Extract evidence variables from transition distribution
        vector<Variable *> evid_vars = slice(transition_dist()->x_vars(), evid_indices);
        for (int i = 1; i < test_seqs[s]; i++) {
            // Set evidence
            vector<string> prev_evids = slice(data.row(r + i - 1), evid_indices);
            vector<pair<int, int> > evids = zip(evid_indices,
                                                extract_var_val_indices(prev_evids, evid_vars));
            // Generate message
            CNet* msg = transition_dist()->multiplyAndMarginalize(*prev_msg, evids);
            // Deallocate prev_msg
            delete prev_msg;
            // Calculate log likelihood
            ll = msg->LL(data.row(r+i), evid_indices, true);
            total_ll += ll;
            if (debug) cout << "t=" << i << ": " << ll << endl;
            // Substitute message variables for next time slice
            msg->substituteVars(transition_prior_map);
            // Assign msg to prev_msg
            prev_msg = msg;
        }
        // Deallocate prev_msg
        delete prev_msg;
        // Update number of rows
        r += test_seqs[s];
    }
    return total_ll / data.nrows();
} */

template <typename Model>
double DCN<Model>::forwardLL(Dataset& data, vector<int> test_seqs, bool debug) {
    if (debug) cout << "[DEBUG] *** Forward Algorithm with LL scores" << endl;
    // Do nothing if dataset is empty
    if (!data.nrows())
        return 1.0;
    // If test sequences is empty, then just set it to number of rows in dataset
    test_seqs = (test_seqs.size()) ? test_seqs : vector<int>({data.nrows()});
    // Initialize variables
    vector<int> all_indices(2*data.size());
    iota(all_indices.begin(), all_indices.end(), 0);
    double ll, total_ll = 0.0;
    int r = 0;
    // Start processing each sequence in order
    for (int s=0; s<test_seqs.size(); s++) {
        if (debug) cout << "Processing sequence " << s+1 << " (Size: " << test_seqs[s] << ")" << endl;
        // Calculate LL for point 0
        ll = prior_dist()->LL(data.row(r+0));
        total_ll += prior_dist()->LL(data.row(r+0));
        if (debug) cout << "t=0: " << ll << endl;
        // Calculate LL for all other points
        for (int i = 1; i < test_seqs[s]; i++) {
            // First, extract evidence
            vector<string> super_row(data.row(r+i));
            vector<string> prev_row(data.row(r+i-1));
            super_row.insert(super_row.end(), prev_row.begin(), prev_row.end());
            vector<int> super_row_vals = extract_var_val_indices(super_row, vars());
            vector< pair<int,int> > assign_evid = zip(all_indices, super_row_vals);
            // Compute LL for current point and add to total
            ll = log(transition_dist()->probEvid(assign_evid));
            if (debug) cout << "t=" << i << ": " << ll << endl;
            total_ll += ll;
        }
        // Update number of rows
        r += test_seqs[s];
    }
    return total_ll / data.nrows();
}

/*************************
Miscellaneous Functions
**************************/
template <> inline MixAndCDNet* getTransitionDist<MixAndCDNet>(Dataset& data, vector<Variable*>& vars,
                                                        vector<int>& x_indices, vector<int>& model_params) {
    if(model_params.size()!=5)
        return nullptr;
    return new MixAndCDNet(data, vars, x_indices, model_params[0],
            model_params[1], model_params[2], model_params[3],
            model_params[4]);
}

template <> inline AndCDNet* getTransitionDist<AndCDNet>(Dataset& data, vector<Variable*>& vars,
                                                               vector<int>& x_indices, vector<int>& model_params) {
    if(model_params.size()!=4)
        return nullptr;
    return new AndCDNet(data, vars, x_indices, model_params[0],
                           model_params[1], model_params[2], model_params[3]);
}

#endif
