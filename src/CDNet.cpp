#include "CDNet.h"

using namespace std;

/*************************
DECISION NODE
**************************/
/*************************
DecisionNode: Constructors
**************************/
DecisionNode::DecisionNode(Variable* x_var, vector<DecisionNode*> children, CNet* data) {
    this->x_var = x_var;
    this->children = children;
    this->data = data;
}

DecisionNode::DecisionNode(DecisionNode &d) {
    // First, copy everything
    x_var = d.x_var;
    children = d.children;
    data = d.data;
    // Call copy constructor of CNet if necessary
    if(d.isCNet()) {
        data = new CNet(*d.data);
        return;
    }
    // Otherwise, copy all children
    for(int c=0; c<d.children.size(); c++) {
        children[c] = new DecisionNode(*d.children[c]);
    }
}

DecisionNode::~DecisionNode() {
    if(isCNet())
        delete data;
    else {
        for(auto const& child : children) {
            delete child;
        }
    }
}

/*************************
DecisionNode: Access
**************************/
bool DecisionNode::isValid() {
    return !((!x_var && !isCNet()) || (x_var && isCNet()));
}

bool DecisionNode::isCNet() {
    return data != nullptr;
}

/*************************
DecisionNode: Inference
**************************/
double DecisionNode::quickProbEvid(bool debug) {
    if(!isValid())
        return -1.0;
    if(isCNet())
       return data->quickProbEvid(debug);
    return children[x_var->val()]->quickProbEvid(debug);
}

pair<double,vector<int> > DecisionNode::quickMPE(bool debug) {
    vector<int> empty_tuple;
    if(!isValid())
        return pair<double,vector<int> >({-1.0, empty_tuple});
    if(isCNet())
        return data->quickMPE(debug);
    return children[x_var->val()]->quickMPE(debug);
}

/*************************
DecisionNode: Learning
**************************/
void DecisionNode::updateParams(Dataset& dset, vector<Variable*>& existing_vars, unordered_map<Variable*,int>& idx_map,
                                vector<int>& idx_to_remove, vector<pair<int,string> >& curr_filters,
                                int max_depth, int clt_threshold) {
    if (!isValid())
        return;
    if (isCNet()) {
        // Delete old CNet
        delete data;
        // Create idx sets
        vector<int> all_idx(existing_vars.size());
        iota(all_idx.begin(), all_idx.end(), 0);
        set<int> all_idx_set(all_idx.begin(), all_idx.end());
        set<int> idx_to_remove_set(idx_to_remove.begin(), idx_to_remove.end());
        set<int> new_var_idx_set = quick_difference(all_idx_set, idx_to_remove_set);
        // Create new dataset by filtering x values and dropping unneeded columns
        Dataset *filtered_dset = dset.filterByValues(curr_filters);
        Dataset* new_dset = filtered_dset->dropCols(idx_to_remove_set);
        // Select y variables from existing_vars
        vector<Variable*> new_vars(new_var_idx_set.size());
        int p=0;
        for(auto const & v: new_var_idx_set) {
            new_vars[p++] = existing_vars[v];
        }
        // Create CNet
        data = new CNet(*new_dset, new_vars, max_depth, clt_threshold);
        // Garbage collection
        delete filtered_dset;
        delete new_dset;
        return;
    }
    if (idx_map.find(x_var) == idx_map.end())
        return;
    idx_to_remove.push_back(idx_map[x_var]);
    for(int c=0; c<children.size(); c++) {
        curr_filters.push_back(pair<int,string>({idx_map[x_var],x_var->domain()[c]}));
        children[c]->updateParams(dset, existing_vars, idx_map, idx_to_remove,
                            curr_filters, max_depth, clt_threshold);
        curr_filters.pop_back();
    }
    idx_to_remove.pop_back();
}

/*************************
DecisionNode: Sampling
**************************/
void DecisionNode::quickSample(bool debug) {
    if(!isValid())
        return;
    if(isCNet())
        data->quickSample(debug);
    else
        children[x_var->val()]->quickSample(debug);
}

/*************************
DecisionNode: Helpers
**************************/
void DecisionNode::multiplyAndMarginalize(CNet& new_cnet, CNet& x_dist, double& Z) {
    if (!isValid())
        return;
    if (isCNet()) {
        double prob = x_dist.quickProbEvid();
        new_cnet.root()->probs.push_back(prob);
        new_cnet.root()->children.push_back((new CNet(*data))->root());
        Z += prob;
        return;
    }
    if (x_var->isSet()) {
        children[x_var->val()]->multiplyAndMarginalize(new_cnet, x_dist, Z);
        return;
    }
    for(int c=0; c<children.size(); c++) {
        x_var->val() = c;
        children[c]->multiplyAndMarginalize(new_cnet, x_dist, Z);
        x_var->val() = -1;
    }
}

/***********************************
DecisionNode: String Representation
************************************/
string DecisionNode::str(int level, int spacing, string val, bool verbose) {
    // Calculate spaces
    string spaces = duplicate(" ", level*spacing);
    // Check if node is a CNet
    if (isCNet() && data->root())
        return data->root()->str(level+1, spacing, "", val, verbose);
    // Add node information
    string s;
    s += spaces + "[" + val + "] " + x_var->name();
    // Add children information
    for(int c=0; c<children.size(); c++) {
        s += "\n" + children[c]->str(level+1, spacing, to_string(x_var->domain()[c]), verbose);
    }
    return s;
}

/*************************
CUTSET DECISION NETWORK
**************************/
/*************************
CDNet: Constructors
**************************/
CDNet::CDNet(Dataset& data, vector<int> x_indices, int max_cond_depth,
             int max_depth, int clt_threshold, bool smooth) {
    _import_vars = false;
    _root = nullptr;
    _nleaves = 0;
    learnCDNet(data, x_indices, max_cond_depth, max_depth, clt_threshold, smooth);
}

CDNet::CDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int> x_indices,
             int max_cond_depth, int max_depth, int clt_threshold, bool smooth) {
    _import_vars = true;
    _root = nullptr;
    _nleaves = 0;
    learnCDNet(data, existing_vars, x_indices, max_cond_depth, max_depth, clt_threshold, smooth);
}

CDNet::CDNet(vector<Domain*>& doms, vector<Variable*>& vars, vector<Variable*>& y_vars,
             vector<Variable*>& x_vars, DecisionNode* root, int nleaves) {
    _import_vars = true;
    _doms = vector<Domain*>(doms);
    _vars = vector<Variable*>(vars);
    _y_vars = vector<Variable*>(y_vars);
    _x_vars = vector<Variable*>(x_vars);
    _root = root;
    _nleaves = nleaves;
}

CDNet::~CDNet() {
    if(!_import_vars) {
        deleteVec(doms());
        deleteVec(vars());
    }
    if (root())
        delete _root;
}
/*************************
CDNet: Access
**************************/
DecisionNode* CDNet::root() {
    return _root;
}

vector<Domain*>& CDNet::doms() {
    return _doms;
}

vector<Variable*>& CDNet::vars() {
    return _vars;
}

vector<Variable*>& CDNet::y_vars() {
    return _y_vars;
}

vector<Variable*>& CDNet::x_vars() {
    return _x_vars;
}

int CDNet::nleaves() {
    return _nleaves;
}

/*************************
CDNet: Learning
**************************/
void CDNet::learnCDNet(Dataset& data, vector<int> x_indices, int max_cond_depth,
                       int max_depth, int clt_threshold, bool smooth) {
    // Create new domains and variables from data
    _import_vars = false;
    doms() = vector<Domain*>(data.size());
    vars() = vector<Variable*>(data.size());
    for(int c=0; c<data.size(); c++) {
        doms()[c] = data[c].toDomain(c, "d"+to_string(c));
        vars()[c] = new Variable(*doms()[c], c, "v"+to_string(c));
    }
    // Call learnCDNet() method with new set of variables
    learnCDNet(data, vars(), x_indices, max_cond_depth, max_depth, clt_threshold, smooth);
}

void CDNet::learnCDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int> x_indices, int max_cond_depth,
                int max_depth, int clt_threshold, bool smooth) {
    // Do nothing for empty dataset
    _import_vars = true;
    if (!data.size() || existing_vars.size() != data.size())
        return;
    // Create domains and variables
    doms() = vector<Domain*>(data.size());
    vars() = existing_vars;
    vector<int> x_indices_final;
    for(int c=0; c<data.size(); c++) {
        // Assign domain from existing_vars
        doms()[c] = &vars()[c]->domain();
        // Add to x_vars if evidence, otherwise add to y_vars
        if(find(x_indices.begin(), x_indices.end(), c) != x_indices.end()) {
            x_vars().push_back(vars()[c]);
            x_indices_final.push_back(c);
        }
        else
            y_vars().push_back(vars()[c]);
    }
    // Then, we need to learn the root using the helper
    int num_evids = x_vars().size();
    _nleaves = 0;
    _root = CDNet::learnCDNetHelper(data, x_indices_final, vars(), y_vars(),
                                    0, max_cond_depth, max_depth, clt_threshold, _nleaves, smooth);
}

DecisionNode* CDNet::learnCDNetHelper(Dataset& data, vector<int> x_indices, vector<Variable*> curr_vars,
                                      vector<Variable*>& y_vars, int curr_depth, int max_depth,
                                      int max_cnet_depth, int clt_threshold, int& nleaves, bool smooth) {
    // Termination condition for cutset networks
    if (curr_depth>=max_depth || x_indices.empty() || data.nrows() <= (y_vars.size()*3) ) {
        Dataset* new_data = data.dropCols(set<int>(x_indices.begin(), x_indices.end()));
        CNet* cnet = new CNet(*new_data, y_vars, max_cnet_depth, clt_threshold, smooth);
        delete new_data;
        nleaves++;
        return new DecisionNode(nullptr, vector<DecisionNode*>({}), cnet);
    }
    // Otherwise, find variable to split with the maximum MI
    int var_index = 0;
    double max_mi = 0.0;
    vector< vector<double> > mi_matrix = data.MIMatrix();
    for(auto & x: x_indices) {
        double curr_mi = 0.0;
        for(double var_mi : mi_matrix[x]) {
            curr_mi += var_mi;
        }
        if (curr_mi > max_mi) {
            max_mi = curr_mi;
            var_index = x;
        }
    }
    // Second, remove this variable from curr_vars
    Variable* var = curr_vars[var_index];
    curr_vars.erase(curr_vars.begin()+var_index);
    evidencePop1d(x_indices, var_index);
    // Third, Split on variable
    vector<DecisionNode*> children(var->domain().size());
    for(int c=0; c<children.size(); c++) {
        vector <pair<int,string> > filter = zip(vector<int>({var_index}), vector<string>({var->domain()[c]}));
        Dataset* filtered_data = data.filterByValues(filter);
        Dataset* new_data = filtered_data->dropCols(set<int>({var_index}));
        children[c] = learnCDNetHelper(*new_data, x_indices, curr_vars, y_vars,curr_depth+1,
                                        max_depth, max_cnet_depth, clt_threshold, nleaves, smooth);
        delete filtered_data;
        delete new_data;
    }
    // Finally, create new node and return
    return new DecisionNode(var, children, nullptr);
}

void CDNet::updateParams(Dataset& data, vector<Variable*>& existing_vars, vector<int> x_indices,
                            int max_depth, int clt_threshold) {
    if (data.size() != vars().size() || x_vars().size() != x_indices.size() || !root())
        return;
    // Construct mapping from *Variable to index
    vector<int> x_indices_final;
    for(int c=0; c<data.size(); c++) {
        if(find(x_indices.begin(), x_indices.end(), c) != x_indices.end())
            x_indices_final.push_back(c);
    }
    if (x_vars().size() != x_indices_final.size())
        return;
    unordered_map<Variable*,int> idx_map = zipMap(x_vars(), x_indices_final);
    vector<int> idx_to_remove(x_indices_final.begin(), x_indices_final.end());
    vector<pair<int,string> > curr_filters;
    // Update parameters for each leaf
    root()->updateParams(data, existing_vars,idx_map, idx_to_remove,
                        curr_filters, max_depth, clt_threshold);
}

/*************************
CDNet: Inference
**************************/
double CDNet::LL(vector<string> row, vector<int> evid_indices, bool debug) {
    if(root()==nullptr || row.size()!=vars().size())
        return 1.0;
    // Extract variable indices
    vector<int> vals = extract_var_val_indices(row, vars());
    // Set evidence
    for(int v=0; v<vals.size(); v++) {
        vars()[v]->val() = vals[v];
    }
    // Calculate LL
    double ll = log(root()->quickProbEvid(debug));
    unset_all(vars());
    // If evidence is given, calculate conditional LL
    if (!evid_indices.empty()) {
        for(auto const & e : evid_indices) {
            if(e>=0 && e<vals.size())
                vars()[e]->val() = vals[e];
        }
        ll -= log(root()->quickProbEvid(debug));
        unset_all(vars());
    }
    return ll;
}

double CDNet::avgLL(Dataset &dset, bool debug) {
    if(root()==nullptr || dset.size()==0 || dset.nrows()==0)
        return 1.0;
    double total_ll = 0.0;
    for(int i=0; i<dset.nrows(); i++) {
        vector<string> row = dset.row(i);
        double curr_ll = LL(row, vector<int>(), debug);
        if (curr_ll > 0)
            return 1.0;
        // Add to total LL score
        total_ll += curr_ll;
    }
    return total_ll / dset.nrows();
}

double CDNet::probEvid(vector<pair<int, int> > evids, bool debug) {
    // Return -1.0 if no root
    if (!root())
        return -1.0;
    // Set evidence
    unset_all(vars());
    set_evidence(vars(), move(evids));
    if (!is_unset_vars(x_vars()).empty())
        return -1.0;
    // Calculate probability of evidence
    return root()->quickProbEvid(debug);
}

double CDNet::margEvid(vector<pair<int, int> > margs, vector<pair<int, int> > evids, bool debug) {
    // Return -1.0 if no root
    if (!root())
        return -1.0;
    // Get all evidence
    vector<pair<int, int> > all_evids = vector_union(move(margs), evids);
    // Calculate Pr(m,e)
    unset_all(vars());
    set_evidence(vars(), all_evids);
    if (!is_unset_vars(x_vars()).empty())
        return -1.0;
    double total_prob = root()->quickProbEvid(debug);
    // Return if less than or equal to zero
    if (total_prob<=0.0)
        return total_prob;
    // Calculate Pr(e)
    unset_all(vars());
    set_evidence(vars(), evids);
    if (!is_unset_vars(x_vars()).empty())
        return -1.0;
    double prob = root()->quickProbEvid(debug);
    // Calculate probability of evidence
    return total_prob / prob;
}

pair<double, vector<int> > CDNet::MPE(vector<pair<int, int> > evids, bool debug) {
    vector<int> empty_tuple;
    // Return -1.0 if no root
    if (!root())
        return pair<double, vector<int> >({-1.0, empty_tuple});
    // Set evidence
    unset_all(vars());
    set_evidence(vars(), move(evids));
    if (!is_unset_vars(x_vars()).empty())
        return pair<double, vector<int> >({-1.0, empty_tuple});
    // Calculate probability of evidence
    double mpe_prob = (root()->quickMPE(debug)).first;
    vector<int> mpe_tuple(vars().size());
    for(int v=0; v<vars().size(); v++) {
        mpe_tuple[v] = vars()[v]->val();
    }
    return pair<double, vector<int> >({mpe_prob, mpe_tuple});
}

/*************************
CDNet: Sampling
**************************/
pair <double, vector<int> > CDNet::sample(vector<pair<int, int> > evids, bool debug) {
    vector<int> sample_vec(vars().size(), -1);
    // Return -1.0 if no root
    if (!root())
        return pair <double, vector<int> >({-1.0, sample_vec});
    // Set evidence
    unset_all(vars());
    set_evidence(vars(), move(evids));
    if (!is_unset_vars(x_vars()).empty())
        return pair <double, vector<int> >({-1.0, sample_vec});
    // Extract evidence variables that are not a part of X in P(Y|X)
    vector<Variable*> evid_min_x_vars = vector_difference(is_set_vars(vars()), x_vars());
    // Call quickSample from root
    root()->quickSample(debug);
    // Collate sample
    for(int v=0; v<vars().size(); v++) {
        sample_vec[v] = vars()[v]->val();
    }
    // Compute weight
    double joint_prob = root()->quickProbEvid(debug);
    unset_all(evid_min_x_vars);
    double non_evid_prob = root()->quickProbEvid(debug);
    double weight = (non_evid_prob==0.0) ? 0.0 : joint_prob / non_evid_prob;
    // Return weighted sample
    return pair <double, vector<int> >({weight, sample_vec});;
}

vector<pair<double,vector<int> > > CDNet::samples(vector<pair<int, int> > evids, int m, bool debug) {
    vector<pair<double,vector<int> > > samples(m);
    for(int i=0; i<m; i++) {
        samples[i] = sample(evids, debug);
    }
    return samples;
}

/*************************
CDNet: Marginalize
**************************/
/**
 * This function takes a message m(X) and multiplies
 * it with P(Y|X) to give a new distribution m(Y) in
 * the form of a mixture of cutset networks
 *
 * m(Y) will have all the y variables from P(Y|X)
 * plus an additional latent mixture variable
 * called 'vsum'
 *
 * Additionally, if some x variables are set as
 * evidence i.e. m(X,e), then the new distribution
 * will be m(Y|e)
 **/
CNet* CDNet::quickMultiplyAndMarginalize(CNet& x_dist) {
    // First, check if all x_dist variables are contained inside x_vars
    if (!vector_difference(x_vars(), x_dist.vars()).empty())
        return nullptr;
    // Create latent variable for mixture network
    vector<string> mixture_comps = domRange(0, nleaves()-1);
    Domain* latent_dom = new Domain(mixture_comps, nleaves(), "dsum");
    Variable* latent_var = new Variable(*latent_dom, nleaves(), "vsum");
    Node* new_root = new Node(latent_var);
    // Create new cutset network and set variables
    CNet* cnet = new CNet(new_root);
    cnet->vars() = vector<Variable*>(y_vars().size()+1);
    cnet->doms() = vector<Domain*>(y_vars().size()+1);
    for(int v=0; v<cnet->vars().size()-1; v++) {
        cnet->vars()[v] = y_vars()[v];
        cnet->doms()[v] = &cnet->vars()[v]->domain();
    }
    cnet->doms()[y_vars().size()] = latent_dom;
    cnet->vars()[y_vars().size()] = latent_var;
    // Recursively calculate mixture probabilities
    double Z = 0.0;
    root()->multiplyAndMarginalize(*cnet, x_dist, Z);
    // Normalize all branch probabilities (if needed)
    if(Z!=1.0) {
        for(double & prob : cnet->root()->probs) {
            prob /= Z;
        }
    }
    // Return CNet
    return cnet;
}

/**
 * Same as quickMultiplyAndMarginalize() with the
 * key difference being that evidence is
 * explicitly provided in the form of (key,value)
 * pairs
 **/
CNet* CDNet::multiplyAndMarginalize(CNet &x_dist, vector<pair<int, int> > evids) {
    // Set evidence
    set_evidence(x_vars(), evids);
    // Call quickMultiplyAndMarginalize()
    return quickMultiplyAndMarginalize(x_dist);
}

/***************************
CDNet: String Representation
****************************/
string CDNet::str(bool verbose, int spacing) {
    if (root()==nullptr)
        return "NULL";
    return root()->str(0, spacing, "", verbose);
}