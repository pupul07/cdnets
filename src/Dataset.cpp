#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include "Utilities.h"
#include "Dataset.h"

using namespace std;

/***************************
  DATASET
****************************/
/***************************
  Constructors
****************************/
Dataset::Dataset(string infile_path, string delim, bool skip_header, bool debug) {
  ifstream infile(infile_path);
  string line;
  getline(infile, line);
  vector<string> tokens = split(line, delim);
  // Create column vectors
  for(int i=0; i<tokens.size(); i++) {
    string name = (!skip_header) ? "c"+to_string(i) : tokens[i];
    DiscreteColumn<string>* col = new DiscreteColumn<string>(name);
    cols().push_back(col);
  }
  if (debug) cout << "[DEBUG] Created " << cols().size() << " columns" << endl;
  // Reset pointer if first line is data
  if (!skip_header)
    infile.seekg(0);
  // Start parsing each line
  int line_num = 1;
  while (getline(infile, line)) {
    tokens = split(line, delim);
    // Display warning message if token count mismatch
    if (tokens.size() != cols().size())
      cout << "[WARNING] Column number mismatch on line " << line_num << "(" << tokens.size() << ")" << endl;
    // Insert into columns
    for(int i=0; i<cols().size(); i++) {
      string token = (i<tokens.size()) ? trim(tokens[i]) : "";
      cols()[i]->push(token);
    }
    // Increment line number
    line_num++;
  }
  // Initialize weights to 1
  if (!cols().empty()) _weights = vector<double>(cols()[0]->size(), 1.0);
  if (debug) cout << "[DEBUG] Finished creating dataset!" << endl;
}

Dataset::Dataset(vector<DiscreteColumn<string>*> cols) {
    _cols = cols;
    _weights = vector<double>(_cols[0]->size(), 1.0);
}

// Copy Constructor
Dataset::Dataset(Dataset& data) {
  _cols = vector<DiscreteColumn<string>* >(data.cols().size());
  for(int c=0; c<_cols.size(); c++) {
    DiscreteColumn<string>* data_col = data.cols()[c];
    _cols[c] = new DiscreteColumn<string>(data_col->vals(), data_col->name());
  }
  _weights = data.weights();
}

// Destructor
Dataset::~Dataset() {
    for(auto const & col: cols()) {
        delete col;
    }
}

/***************************
  Access
****************************/
int Dataset::size() {
    return cols().size();
}

int Dataset::nrows() {
    if (cols().size()==0)
        return 0;
    return cols()[0]->size();
}

vector<double>& Dataset::weights() {
    return _weights;
}

vector < DiscreteColumn<string>* >& Dataset::cols() {
    return _cols;
}

DiscreteColumn<string>& Dataset::operator [](int i) {
    return *cols()[i];
}

vector<string> Dataset::row (int i) {
    if (cols().size()==0 || i<0 || i>=cols()[0]->size())
        return vector<string>();
    vector<string> vals(cols().size());
    for(int c=0; c<cols().size(); c++) {
        vals[c] = (*cols()[c])[i];
    }
    return vals;
}

string Dataset::head() {
    return subset(0, 10);
}

string Dataset::tail() {
    return subset(-10, 10);
}

/***************************
  Operations
****************************/
string Dataset::subset(int start, int length) {
  string subset_str = "";
  // Return empty string if everything is empty
  if (cols().size()==0 || cols()[0]->size()==0)
    return subset_str;
  // Print all column headings
  for(auto & col : cols()) {
    subset_str += col->name() + "\t";
  }
  subset_str += "\n";
  // Print all rows
  start = (start<0)? max(0, cols()[0]->size() + start) : start;
  int end = (length<0) ? cols()[0]->size() - start : min(start + length, cols()[0]->size());
  for(int i=start; i<end; i++) {
    for(int j=0; j<cols().size(); j++) {
      subset_str += (*cols()[j])[i] + "\t";
    }
    subset_str += "\n";
  }
  return subset_str;
}

vector< vector<double> > Dataset::MIMatrix(bool self_zero) {
  int num_cols = cols().size();
  vector< vector<double> > mi_mat(num_cols, vector<double>(num_cols, 0.0));
  for(int i=0; i<num_cols; i++) {
    for(int j=i; j<num_cols; j++) {
      if (self_zero && i==j)
        continue;
      double mi_ij = MI<string>(*cols()[i],*cols()[j],weights());
      mi_mat[i][j] = mi_ij;
      mi_mat[j][i] = mi_ij;
    }
  }
  return mi_mat;
}

// Returns pointer to new dataset after applying filters to columns
Dataset* Dataset::filterByValues(vector<pair<int,string> > value_pairs) {
  // First find the indices
  set<int> indices;
  for(int v=0; v<value_pairs.size(); v++) {
    int c = value_pairs[v].first;
    string value = value_pairs[v].second;
    if (c>=size())
      continue;
    set<int> col_indices = cols()[c]->indexesOf(value);
    indices = (indices.size()==0) ? col_indices : quick_intersection(indices, col_indices);
  }
  // Next create the new dataset
  Dataset* new_data = new Dataset(*this);
  vector<int> indices_vec(indices.begin(), indices.end());
  for(int c=0; c<cols().size(); c++) {
    (*new_data)[c] = (*new_data)[c].filterByIndex(indices_vec);
  }
  // Finally, calculate the weights
  new_data->weights() = selectWeights(weights(), indices_vec);
  // Return dataset
  return new_data;
}

// Returns only the indices
vector<int> Dataset::filterByValuesOnly(vector<pair<int,string> > value_pairs) {
    // First find the indices
    set<int> indices;
    for (int v = 0; v < value_pairs.size(); v++) {
        int c = value_pairs[v].first;
        string value = value_pairs[v].second;
        if (c >= size())
            continue;
        set<int> col_indices = cols()[c]->indexesOf(value);
        indices = (indices.size() == 0) ? col_indices : quick_intersection(indices, col_indices);
    }
    return vector<int>(indices.begin(), indices.end());
}

// Returns pointer to new dataset after dropping designated columns
Dataset* Dataset::dropCols(set<int> col_indices) {
  Dataset* new_data = new Dataset(*this);
  set<int>::reverse_iterator rit;
  for(rit=col_indices.rbegin(); rit!=col_indices.rend(); rit++) {
    DiscreteColumn<string>* col = new_data->cols()[*rit];
    new_data->cols().erase(new_data->cols().begin() + *rit);
    delete col;
  }
  return new_data;
}

/***************************
  String Representation
****************************/
const string Dataset::str() {
  return subset();
}

/***************************
  Functions
****************************/
Function* discreteColsToFunction(vector<Variable*>& all_vars, Dataset& data,
                                    vector<int> indices, bool smooth) {
  // Create variable of vectors for function
  vector<Variable*> col_vars(indices.size(), nullptr);
  for(int i=0; i<indices.size(); i++) {
    col_vars[i] = all_vars[indices[i]];
  }
  // Create empty function
  Function* func = new Function(col_vars);
  func->setAllToZero();
  // Return uniform distribution if dataset is empty
  if (!data.nrows()) {
      func->normalize();
      return func;
  }
  // Compute function table
  double smooth_factor = (data.nrows() * 0.001) / var_vector_size(col_vars);
  for(int i=0; i<var_vector_size(col_vars); i++) {
    // set<int> all_indices(data[col_vars[0]->id()].indexesOf(col_vars[0]->domainVal()));
    set<int> all_indices(data[indices[0]].indexesOf(all_vars[indices[0]]->domainVal()));
    for(int v=1; v<col_vars.size(); v++) {
      string curr_val = col_vars[v]->domainVal();
      // set<int> curr_indices = data[col_vars[v]->id()].indexesOf(curr_val);
      set<int> curr_indices = data[indices[v]].indexesOf(curr_val);
      all_indices = quick_intersection(all_indices, curr_indices);
    }
    // func->table()[i] = smooth ? all_indices.size() + smooth_factor : all_indices.size();
    vector<int> all_indices_vec(all_indices.begin(), all_indices.end());
    func->table()[i] = sumWeights(data.weights(), all_indices_vec);
    if (smooth) func->table()[i] += smooth_factor;
    increment_vars(col_vars);
  }
  // Normalize function
  func->normalize();
  func->unsetAll();
  // Return pointer to function
  return func;
}

CPT* discreteColsToCPT(vector<Variable*>& all_vars, Dataset& data,
                          vector<int> indices, vector<int> marg_indices,
                          bool smooth) {
  // cout << "[DEBUG] Inside discreteColsToCPT()" << endl;
  Function* func = discreteColsToFunction(all_vars, data, indices, smooth);
  // cout << "[DEBUG] Generated function" << endl;
  vector<Variable*> marg_vars;
  for(int m=0; m<marg_indices.size(); m++) {
    marg_vars.push_back(all_vars[marg_indices[m]]);
  }
  CPT* cpt = new CPT(*func, marg_vars);
  // cout << "[DEBUG] Generated CPT" << endl;
  delete func;
  // cout << "[DEBUG] Exiting discreteColsToCPT()" << endl;
  return cpt;
}

set < pair<int,int> > maxSpanTree(vector < vector<double> >& cost_mat) {
  set < pair<int,int> > tree;
  set<int> in_tree({0});
  // Create set containing all nodes in graph and find min val
  set<int> all_nodes;
  double min_val = -9999999.0;
  for(int i=0; i<cost_mat.size(); i++) {
    all_nodes.insert(i);
    for (int j=0; j<cost_mat.size(); j++) {
      min_val = min(min_val, cost_mat[i][j]);
    }
  }
  // Use Prim's algorithm to find maximum spanning tree
  while( in_tree.size() < cost_mat.size()) {
    double max_val = min_val;
    int i = 0, j = 0;
    for(auto u: in_tree) {
      for(auto v: quick_difference(all_nodes, in_tree)) {
        if (cost_mat[u][v] > max_val) {
          i = u;
          j = v;
          max_val = cost_mat[u][v];
        }
      }
    }
    in_tree.insert(j);
    tree.insert(make_pair(i, j));
  }
  return tree;
}

vector<int> extract_var_val_indices(vector<string>& vals, vector<Variable*>& vars) {
    if(vals.size() > vars.size())
        return vector<int>();
    vector<int> indices(vals.size());
    for(int v=0; v<vals.size(); v++) {
        int index = vars[v]->indexOf(vals[v]);
        if (index<0)
            return vector<int>();
        indices[v] = index;
    }
    return indices;
}

/*********
 * This function takes in a Dataset and reorders
 * the columns by pushing the evidences in front.
 * It also returns the number of columns that have
 * been shifted (ignores invalid columns).
 *
 * Ex:
 * vector<int> evid_indices({3, 10});
 * int s = reorder_by_evidence(data, evid_indices);
 *
 * This will rearrange the dataset as follows:
 * data(3,10,1,...,n)
 * Also, s=2 since 2 columns were successfully
 * re-arranged.
 *
 * NOTE: evid_indices MUST be sorted! If not,
 * then set sorting flag to true
*********/
int reorder_by_evidence(Dataset& data, vector<int>& evid_indices, bool to_sort) {
    // Sort if unsorted
    if (to_sort)
        sort(evid_indices.begin(), evid_indices.end());
    // Push evid_indices to beginning
    int evid_pos = 0;
    for(auto & evid_index: evid_indices) {
        if (evid_index < data.size()) {
            DiscreteColumn<string>* temp = &data[evid_index];
            data.cols().erase(data.cols().begin() + evid_index);
            data.cols().insert(data.cols().begin() + (evid_pos++), temp);
        }
    }
    // Return number of columns successfully shifted
    return evid_pos;
}

vector< vector<int> > partition_vars(Dataset& data, int max_size) {
    // Check validity
    if(max_size<1)
        return vector< vector<int> >();
    // If max_size == 1
    if (max_size==1) {
        vector< vector<int> > partitions(data.size());
        for(int v=0; v<data.size(); v++) {
            partitions[v] = vector<int>({v});
        }
        return partitions;
    }
    // Else, partition using MI scores
    vector < vector<double> > mi_matrix = data.MIMatrix();
    vector<double> mi_totals = sum_2d_vector_rows(mi_matrix);
    vector<int> vars_desc = sort_indices_desc(mi_totals);
    // Find MI Matrix sorted in descending order of marginal MI scores by index
    vector < vector<int> > mi_index_matrix(mi_matrix.size(), vector<int>(mi_matrix.size()));
    for(int i=0; i<mi_index_matrix.size(); i++) {
        mi_index_matrix[i] = sort_indices_desc(mi_matrix[i]);
    }
    // Finally, start grouping variables
    unordered_set<int> remaining_vars = init_unordered_set_range<int>(data.size());
    vector< vector<int> > partitions;
    for(auto const & v: vars_desc) {
        // Continue if element has already been processed
        if (remaining_vars.find(v) == remaining_vars.end())
            continue;
        // First, remove element from remaining_vars
        remaining_vars.erase(v);
        // Next, add this to the current partition
        vector<int> partition(min<int>(max_size, remaining_vars.size()));
        partition[0] = v;
        // Find some more elements to add to partition
        int p = 1;
        for(auto const & w: mi_index_matrix[v]) {
            if (v==w)
                continue;
            if (p==partition.size())
                break;
            if (remaining_vars.find(w) != remaining_vars.end()) {
                partition[p++] = w;
                remaining_vars.erase(w);
            }
        }
        // Add partition to partitions
        partitions.push_back(partition);
    }
    return partitions;
}

/*********
 * This function reads in domains from a file of the following form:
 *
 * File:
 * =====
 * n (no. of domains)
 * d01, d02, d03, ..., d0n
 * ...
 * dm1, dm2, dm3, ..., dmp
 *
 * This will create m+1 domains from d0,..,dm in a vector called doms
*********/
vector<Domain*> readDomsFromFile(string infile_path, string delim, bool trans_flag, bool debug) {
    // First, read in number of domains needed
    ifstream infile(infile_path);
    string line;
    getline(infile, line);
    int n = stoi(line);
    int size = (!trans_flag) ? n : 2*n;
    // Next read in each domain
    vector<Domain*> doms(size, nullptr);
    for(int i=0; i<n; i++) {
        if(!getline(infile, line)) {
            deleteVec(doms);
            return vector<Domain*>({});
        }
        vector<string> vals(split(line, delim));
        sort(vals.begin(), vals.end());
        doms[i] = new Domain(vals, i, "d"+to_string(i));
        if (trans_flag)
            doms[n+i] = new Domain(vals, i, "d"+to_string(i));
    }
    // Return all doms
    return doms;
}