#include "Variable.h"

using namespace std;

/***************************
  Domain
****************************/
Domain::Domain(vector<string> vals, int id, string name) {
  set(vals, id, name);
}

void Domain::set(vector<string> vals, int id, string name) {
  _id = id;
  _name = name;
  _vals = vals;
}

int& Domain::id() {
    return _id;
}

string& Domain::name() {
    return _name;
}

vector<string>& Domain::vals() {
    return _vals;
}

int Domain::size() {
  return _vals.size();
}

// Only works if domain values are sorted
int Domain::indexOf(string val) {
  int l=0, u=size()-1, m;
  while(l<=u) {
    m = (l+u)/2;
    if(_vals[m]==val)
      return m;
    else if(_vals[m]<val)
      l=m+1;
    else
      u=m-1;
  }
  return -1;
}

const string &Domain::operator [](int i) {
  if (i<0 || i>=_vals.size())
    return null;
  return _vals[i];
}

string Domain::str() {
  return "<Domain '" + _name + "'>(" + to_string(_id) + "): { " + to_string_vec<string>(_vals) + " }";
}

/***************************
  Variable
****************************/
Variable::Variable(Domain& domain, int id, string name): _domain(&domain), _id(id), _name(name), _val(-1) {}

void Variable::set(Domain& domain, int id, string name) {
  _id = id;
  _name = name;
  _domain = &domain;
  _val = -1;
}

int Variable::id() {
  return _id;
}

string Variable::name() {
  return _name;
}

int& Variable::val() {
  return _val;
}

string Variable::domainVal() {
  return domain()[val()];
}

Domain& Variable::domain() {
  return *_domain;
}

// Only works if domain values are sorted
int Variable::indexOf(string val) {
  return domain().indexOf(val);
}

bool Variable::isSet() {
  return _val>=0;
}

void Variable::unset() {
  _val = -1;
}

void Variable::setToZero() {
  _val = 0;
}

const string Variable::str() {
  return "<Variable '" + _name + "'>(" + to_string(_id) + ") = " + (*_domain)[_val] + " [ " + _domain->str() + " ]";
}

const string Variable::toJSON(bool no_key) {
    string json_str = "{ \"Id\": " + to_string(id()) + ", " +
                        "\"Name\": \"" + name() + "\", " +
                        "\"Values\": [ " + to_string_vec(domain().vals()) + " ] }";
    if(no_key)
        return json_str;
    return "{ \"Variable\": " + json_str + " }";
}

/***************************
  Functions
****************************/
/*********
 * This function takes in a vector of Variable pointers
 * and calculates the address of a probability table
 * having the following form:
 * (Var_1, ..., Var_n)
 *    0  , ...,  0
 *    0  , ...,  1
 * Returns -1 if any variable is not set
 *
 * Ex:
 * For a set of variables having the following domains:
 * v1 = { 0, 1, 2, 3 }
 * v2 = { 0, 1 }
 * v3 = { 0, 1, 2 }
 *
 * The address for (v1=1,v2=1,v3=2) will therefore be:
 * (1*6) + (1*3) + (2*1) = 11
*********/
int calc_address(vector<Variable*>& vars) {
  vector<int> block_sizes(vars.size(),1);
  for(int i=vars.size()-2; i>=0; i--) {
    block_sizes[i] = block_sizes[i+1] * vars[i+1]->domain().size();
  }
  int addr = 0;
  for(int i=0; i<vars.size(); i++) {
    if (!vars[i]->isSet())
      return -1;
    addr += block_sizes[i] * vars[i]->val();
  }
  return addr;
}

/*********
 * This function takes in a vector of Variable pointers
 * and unsets all their values (i.e. changes them to -1)
*********/
void unset_all(vector<Variable*>& vars) {
  for(int v=0; v<vars.size(); v++) {
    vars[v]->unset();
  }
}

/*********
 * This function takes in a vector of Variable pointers
 * and sets all their values to 0
*********/
void set_all_to_zero(vector<Variable*>& vars) {
  for(int v=0; v<vars.size(); v++) {
    vars[v]->setToZero();
  }
}

/*********
 * This function takes in a vector of Variable pointers
 * and sets their values according to evidence. Each
 * value of the evidence vector refers to an (index,
 * value) pair. If an evidence is invalid (wrong index
 * or wrong value), then that particular variable is
 * skipped.
 *
 * Ex:
 * vector<Variable*> vars({ &var1, &var2 });
 * vector<pair<int, int>> evid({pair<int,int>({0,1})});
 * set_evidence(vars, vector<pair<int,int>);
*********/
void set_evidence(vector<Variable*>& vars,
                    vector<pair<int,int> > evidence) {
    for(int e=0; e<evidence.size(); e++) {
        int index = evidence[e].first, value = evidence[e].second;
        if (index>vars.size() || value >= vars[index]->domain().size())
            continue;
        vars[index]->val() = value;
    }
}

vector<Variable*> is_set_vars(vector<Variable*>& vars) {
  vector<Variable*> set_var_vec;
  for(int v=0; v<vars.size(); v++) {
    if (vars[v]->val()>=0)
      set_var_vec.push_back(vars[v]);
  }
  return set_var_vec;
}

vector<Variable*> is_unset_vars(vector<Variable*>& vars) {
  vector<Variable*> unset_var_vec;
  for(int v=0; v<vars.size(); v++) {
    if (vars[v]->val()<0)
      unset_var_vec.push_back(vars[v]);
  }
  return unset_var_vec;
}