#ifndef CNET_H_
#define CNET_H_

#include <iostream>
#include <vector>
#include <set>
#include "Variable.h"
#include "Function.h"
#include "Dataset.h"
#include "CLT.h"
#include "Utilities.h"

using namespace std;

struct Node {
    Variable* var;
    Node* parent;
    vector<Node*> children;
    vector<double> probs;
    CLT* data;
  public:
    Node(Variable* = nullptr, Node* = nullptr, vector<Node*> = vector<Node*>(),
          vector<double> = vector<double>(), CLT* = nullptr);
    Node(Node&);
    void setParams(Variable* = nullptr, Node* = nullptr, vector<Node*> = vector<Node*>(),
          vector<double> = vector<double>(), CLT* = nullptr);
    bool isCLT();
    bool isValid();
    double quickLL();
    double probEvid(bool=false);
    double MPE(bool=false);
    void sample(bool=false);
    void substituteVars(unordered_map<Variable*,Variable*>& var_map);
    string str(int=0, int=3, string="", string="", bool=false);
    string toJSON(int level, int spacing, int& id);
    ~Node();
};

class CNet {
  protected:
    bool _import_vars;
    vector<Domain*> _doms;
    vector<Variable*> _vars;
    Node* _root;
    // Helper Methods
    Node* learnCNetHelper(Dataset&, vector<Variable*>, int, int, int, bool);
  public:
    CNet();
    CNet(CNet&);
    explicit CNet(Node*);
    explicit CNet(Dataset&, int=5, int=5, bool=true);
    CNet(Dataset&, vector<Variable*>&, int=5, int=5, bool=true);
    vector<Domain*>& doms();
    vector<Variable*>& vars();
    Node* root();
    void learnCNet(Dataset&, vector<Variable*>&, int=5, int=5, bool=true);
    double LL(vector<string>, vector<int> = vector<int>({}), bool=false, bool=false);
    double avgLL(Dataset&, bool=false);
    double quickProbEvid(bool=false);
    double probEvid(vector<pair<int,int> >, bool=false);
    double margEvid(vector<pair<int,int> >, vector<pair<int,int> >, bool=false);
    pair <double, vector<int> > quickMPE(bool=false);
    pair <double, vector<int> > MPE(bool=false);
    pair <double, vector<int> > MPE(vector<pair<int,int> >, bool=false);
    void quickSample(bool=false);
    pair <double, vector<int> > sample(bool=false);
    pair <double, vector<int> > sample(vector<pair<int,int> >, bool=false);
    vector<pair<double,vector<int> > > samples(int = 100, bool=false);
    vector<pair<double,vector<int> > > samples(vector<pair<int,int> >, int = 100,
                                                bool=false);
    void substituteVars(unordered_map<Variable*,Variable*>& var_map);
    string str(bool=false, int=3);
    string toJSON(int=4);
    ~CNet();
};

#endif
