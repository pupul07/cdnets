#include "Function.h"

using namespace std;

/*************************
Function Class
**************************/
Function::Function(vector<Variable*> scope) {
  _scope = scope;
  int size = 1;
  for(int v=0; v<scope.size(); v++) {
    size *= scope[v]->domain().size();
  }
  _table = vector<double>(size, 1.0);
}

int Function::size() {
  return _table.size();
}

vector<double>& Function::table() {
  return _table;
}

vector<Variable*>& Function::scope() {
  return _scope;
}

double Function::get(vector<int>& assign) {
  if (assign.size() != _scope.size())
    return -1.0;
  for(int v=0; v<_scope.size(); v++) {
    if (assign[v] >= _scope[v]->domain().size())
      return -1.0;
    _scope[v]->val() = assign[v];
  }
  return _table[calc_address(_scope)];
}

void Function::set(vector<int>& assign, double val) {
  if (assign.size() != _scope.size() || val < 0.0)
    return;
  for(int v=0; v<_scope.size(); v++) {
    if (assign[v] >= _scope[v]->domain().size())
      return;
    _scope[v]->val() = assign[v];
  }
  _table[calc_address(_scope)] = val;
}

void Function::setAll(vector<double>& new_table) {
  _table = new_table;
}

void Function::unsetAll() {
  unset_all(_scope);
}

void Function::setAllToZero() {
  set_all_to_zero(_scope);
}

void Function::normalize() {
  double sum = 0.0;
  for(int i=0; i<_table.size(); i++) {
    sum += _table[i];
  }
  if(sum==0.0)
    return;
    for(int i=0; i<_table.size(); i++) {
      _table[i] /= sum;
    }
}

Function* Function::removeEvidence(vector<Variable*> dummy_vars) {
  // Check which variables are non-evidence
  vector<Variable*> non_evidence = is_unset_vars(scope());
  // If no evidence, return original function
  if (non_evidence.size() == scope().size())
    return this;
  // If dummy_vars.size() mismatches
  if (dummy_vars.size() != (scope().size() - non_evidence.size()))
    return nullptr;
  // Create new Function
  vector<Variable*> new_scope = vector_union(non_evidence, dummy_vars);
  Function* func = new Function(new_scope);
  // If all variables are evidence, table will only have one entry
  if (non_evidence.size()==0)
    func->table()[0] = table()[calc_address(scope())];
  // Populate function table
  set_all_to_zero(new_scope);
  for(int n=0; n<var_vector_size(non_evidence); n++) {
    func->table()[calc_address(new_scope)] = table()[calc_address(scope())];
    increment_vars(non_evidence);
  }
  unset_all(new_scope);
  // Set evidence variables
  // set_all_to_zero(dummy_vars);
  // Return Function
  return func;
}

Function* Function::multiplyAndMarginalize(Function& f1, Function& f2,
                                            vector<Variable*> marginal_vars,
                                            bool to_normalize, bool maximize) {
  // First find the combined scope
  vector<Variable*> combined_scope = vector_union(f1.scope(), f2.scope());
  vector<int> combined_scope_vals = extract_var_vals(combined_scope);
  set_all_to_zero(combined_scope);
  // Next, extract the marginals
  vector<Variable*> marginal_scope = vector_intersection(marginal_vars, combined_scope);
  if (marginal_scope.size() == 0)
    marginal_scope = vector<Variable*>(combined_scope);
  // Now extract the non-marginals
  vector<Variable*> non_marginal_scope = vector_difference(combined_scope, marginal_scope);
  // Initialize Function
  Function* product = new Function(marginal_scope);
  // Multiply and marginalize Function
  int marginal_scope_size = var_vector_size(marginal_scope);
  int non_marginal_scope_size = var_vector_size(non_marginal_scope);
  for(int i=0; i<marginal_scope_size; i++) {
      product->table()[i] = f1.table()[calc_address(f1.scope())] * \
                            f2.table()[calc_address(f2.scope())];
      increment_vars(non_marginal_scope);
      for(int j=1; j<non_marginal_scope_size; j++) {
        double new_product = f1.table()[calc_address(f1.scope())] * \
                                f2.table()[calc_address(f2.scope())];
        if (maximize)
          product->table()[i] = max(product->table()[i], new_product);
        else
          product->table()[i] += new_product;
        increment_vars(non_marginal_scope);
      }
      increment_vars(marginal_scope);
  }
  // Return Function
  if (to_normalize) product->normalize();
  set_var_vals(combined_scope, combined_scope_vals);
  return product;
}

string Function::str(bool verbose) {
  string s = "Function ( ";
  // Display variable names
  for(int v=0; v<scope().size(); v++) {
    s += scope()[v]->name() + ", ";
  }
  s = s.substr(0, s.size()-2) + " )\n";
  if (!verbose)
    return s.substr(0, s.size()-1);
  // Generate table values if verbose
  setAllToZero();
  for(int i=0; i<_table.size(); i++) {
    for(int v=0; v<_scope.size(); v++) {
      s += to_string(_scope[v]->domainVal()) + " ";
    }
    s += ": " + to_string(_table[i], 2) + "\n";
    increment_vars(_scope);
  }
  unsetAll();
  return s;
}

/*************************
CPT Class
**************************/
CPT::CPT(vector<Variable*> marg_vars, vector<Variable*> cond_vars) {
  _marg_scope = marg_vars;
  if (_marg_scope.size()==0)
    return;
  _cond_scope = vector_difference(cond_vars, marg_vars);
  int size = 1;
  _scope = vector_union(_marg_scope, _cond_scope);
  for(int v=0; v<_scope.size(); v++) {
    size *= _scope[v]->domain().size();
  }
  _table = vector<double>(size, 1.0);
}

CPT::CPT(Function& func, vector<Variable*> marg_vars, bool to_normalize) {
  _marg_scope = vector_intersection(marg_vars, func.scope());
  if (_marg_scope.size()==0)
    return;
  _cond_scope = vector_difference(func.scope(), _marg_scope);
  Function dummy_func(_marg_scope);
  Function* cpt_func = Function::multiplyAndMarginalize(dummy_func, func, vector<Variable*>(), false);
  _scope = vector_union(_marg_scope, _cond_scope);
  _table = cpt_func->table();
  delete cpt_func;
  if (to_normalize) normalize();
}

CPT::CPT(CPT& cpt) {
    _scope = vector<Variable*>(cpt.scope().begin(), cpt.scope().end());
    _marg_scope = vector<Variable*>(cpt.marg_scope().begin(), cpt.marg_scope().end());
    _cond_scope = vector<Variable*>(cpt.cond_scope().begin(), cpt.cond_scope().end());
    _table = cpt.table();
}

vector<Variable*>& CPT::marg_scope() {
  return _marg_scope;
}

vector<Variable*>& CPT::cond_scope() {
  return _cond_scope;
}

void CPT::normalize() {
  setAllToZero();
  int marg_scope_size = var_vector_size(_marg_scope);
  int cond_scope_size = var_vector_size(_cond_scope);
  for(int j=0; j<cond_scope_size; j++) {
    double z = 0.0;
    for(int i=0; i<marg_scope_size; i++) {
      z += _table[calc_address(_scope)];
      increment_vars(_marg_scope);
    }
    if(z==0.0)
      continue;
    for(int i=0; i<marg_scope_size; i++) {
      _table[calc_address(_scope)] /= z;
      increment_vars(_marg_scope);
    }
    increment_vars(_cond_scope);
  }
  unsetAll();
}

CPT* CPT::multiplyAndMarginalize(CPT& c1, CPT& c2, vector<Variable*> marginal_vars,
                                  bool to_normalize, bool maximize) {
  vector<Variable*> c1_c2_all_marg_vars = vector_union(c1.marg_scope(), c2.marg_scope());
  vector<Variable*> final_marg_vars = vector_intersection(marginal_vars, c1_c2_all_marg_vars);
  if (final_marg_vars.size()==0)
    final_marg_vars = c1_c2_all_marg_vars;
  vector<Variable*> sum_out_vars = vector_difference(c1_c2_all_marg_vars, final_marg_vars);
  vector<Variable*> function_marg_vars = vector_difference(
                                          vector_union(
                                            final_marg_vars,
                                            vector_union(c1.cond_scope(), c2.cond_scope())
                                          ),
                                          sum_out_vars
                                        );
  Function* final_func = Function::multiplyAndMarginalize(c1, c2, function_marg_vars, false, maximize);
  CPT* final_cpt = new CPT(*final_func, final_marg_vars, false);
  if (to_normalize) final_cpt->normalize();
  delete final_func;
  return final_cpt;
}

CPT* CPT::removeEvidence(vector<Variable*> dummy_vars) {
    // If no evidence provided, return pointer to original function
    if (is_set_vars(scope()).size() == 0)
        return this;
    // Check new marginal scope
    vector<Variable*> marg_evidence = is_set_vars(marg_scope());
    vector<Variable*> marg_non_evidence = is_unset_vars(marg_scope());
    vector<Variable*> new_marg_scope(marg_non_evidence);
    new_marg_scope.insert(new_marg_scope.end(), dummy_vars.begin(), dummy_vars.begin() + marg_evidence.size());
    // Create function and convert to CPT
    Function* func = Function::removeEvidence(dummy_vars);
    // Check if function is invalid
    if (func==nullptr)
        return nullptr;
    // If function is valid, then return CPT
    CPT* cpt = new CPT(*func, new_marg_scope, false);
    delete func;
    return cpt;
}

string CPT::str(bool verbose) {
  string s = "CPT ( ";
  // Add marginal variable names
  for(int m=0; m<marg_scope().size(); m++) {
    s += marg_scope()[m]->name() + ", ";
  }
  s = s.substr(0, s.size()-2) + " | ";
  // Add conditional variable names
  for(int c=0; c<cond_scope().size(); c++) {
    s += cond_scope()[c]->name() + ", ";
  }
  s = s.substr(0, s.size()-2) + " )\n";
  if (!verbose)
    return s.substr(0, s.size()-1);
  // Generate table values, if verbose
  vector<int> old_vals = extract_var_vals(scope());
  setAllToZero();
  for(int i=0; i<_table.size(); i++) {
    for(int v=0; v<_marg_scope.size(); v++) {
      s += to_string(_marg_scope[v]->domainVal()) + " ";
    }
    s += "| ";
    for(int v=0; v<_cond_scope.size(); v++) {
      s += to_string(_cond_scope[v]->domainVal()) + " ";
    }
    s += ": " + to_string(_table[i], 2) + "\n";
    increment_vars(_scope);
  }
  set_var_vals(scope(), old_vals);
  return s;
}

string CPT::toJSON(bool no_key) {
    // Generate variable string
    string vars_json_str;
    for(auto & var : marg_scope()) {
        vars_json_str += to_string(var->id()) + ", ";
    }
    // Generate parent string
    string parents_json_str;
    for(auto & parent : cond_scope()) {
        parents_json_str += to_string(parent->id()) + ", ";
    }
    // Generate complete CPT string
    string json_str = "{ \"Variables\": [ " + vars_json_str.substr(0, vars_json_str.size()-2) + " ], " +
                      "\"Parents\": [ " + parents_json_str.substr(0, parents_json_str.size()-2) + " ], " +
                      "\"Probabilities\": [ " + to_string_vec(table()) + " ] }";
    if(no_key)
        return json_str;
    return "{ \"CPT\": " + json_str + " }";
}

/*************************
Miscellaneous Functions
**************************/
/*********
* This function takes in a vector of Variable pointers
* and increments its value by 1
*
* Ex:
* vector<Variable*> vars({ &var1, &var2 });
* increment_vars(vars);
*********/
void increment_vars(vector<Variable*>& vars) {
    if (vars.size() == 0)
        return;
    int curr_index = vars.size()-1;
    vars[curr_index]->val()++;
    if (vars[curr_index]->val() < vars[curr_index]->domain().size())
        return;
    while (curr_index >=0 && vars[curr_index]->val() >= vars[curr_index]->domain().size() - 1) {
        vars[curr_index]->val() = 0;
        curr_index--;
    }
    if (curr_index >=0 )
        vars[curr_index]->val()++;
}

/*********
* This function takes in a vector of Variable pointers
* and computes its total domain size
*
* Ex:
* vector<Variable*> vars({ &var1, &var2 });
* var_vector_size(vars);
*********/
int var_vector_size(vector<Variable*>& vars) {
  if (vars.size() == 0)
    return 0;
  int size = 1;
  for(int v=0; v<vars.size(); v++) {
    size *= vars[v]->domain().size();
  }
  return size;
}

/*********
* This function takes in a vector of Variable pointers
* and creates a mapping from these pointers to dummy
* Variables that can be used as evidence
*
* Ex:
* vector<Variable*> vars({ &var1, &var2 });
* unordered_map<Variable*,Variable*> dummy_map = create_dummy_map(vars);
*********/
unordered_map<Variable*,Variable*> create_dummy_map(vector<Variable*>& vars, bool set) {
  unordered_map<Variable*,Variable*> dummy_map;
  // If vars is empty, then return empty map
  if (vars.size()==0)
    return dummy_map;
  // Create dummy variables
  Domain* evid_domain = new Domain(vector<string>({"e"}), 0, "evidence");
  for(int v=0; v<vars.size(); v++) {
    Variable* key = vars[v];
    Variable* value = new Variable(*evid_domain, key->id(), "e"+key->name());
    if(set) value->setToZero();
    dummy_map.insert(pair<Variable*,Variable*>({key, value}));
  }
  // Return map
  return dummy_map;
}

/*********
* This function takes in a vector of Variable pointers
* and extracts its values into an integer vector
*
* Ex:
* vector<Variable*> vars({ &var1, &var2 });
* vector<int> var_vals = extract_var_vals(vars);
*********/
vector<int> extract_var_vals(vector<Variable*>& vars) {
  vector<int> var_vals(vars.size());
  for(int v=0; v<vars.size(); v++) {
    var_vals[v] = vars[v]->val();
  }
  return var_vals;
}

/*********
* This function takes in a vector of Variable pointers
* and a set of vector values and assigns them to the
* corresponding variables
*
* Ex:
* vector<Variable*> vars({ &var1, &var2 });
* vector<int> vals({0, 1});
* set_var_vals(vars, vals);
*********/
void set_var_vals(vector<Variable*>& vars, vector<int>& vals) {
  if (vars.size() != vals.size())
    return;
  for(int v=0; v<vars.size(); v++) {
    vars[v]->val() = vals[v];
  }
}

/*********
* This function takes in a vector of Variable pointers
* and generates a quick string representation
*
* Ex:
* vector<Variable*> vars({ &var1, &var2 });
* quick_scan_vars(vars);
*
* Output: v1: 0, v2: 1
*********/
const string quick_scan_vars(vector<Variable*>& vars) {
  string str = "";
  for(int v=0; v<vars.size(); v++) {
    str += vars[v]->name() + ": " +  to_string(vars[v]->val()) + " ";
  }
  return str;
}
