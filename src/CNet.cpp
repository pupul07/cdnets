#include "CNet.h"

using namespace std;

/*************************
Node
**************************/
Node::Node(Variable* var, Node* parent, vector<Node*> children, vector<double> probs, CLT* data) {
  setParams(var, parent, children, probs, data);
}

Node::Node(Node& node) {
    // First, copy everything
    var = node.var;
    parent = node.parent;
    probs = node.probs;
    children = node.children;
    data = node.data;
    // Call copy constructor of CLT if necessary
    if(node.isCLT()) {
        data = new CLT(*node.data);
        return;
    }
    // Otherwise, copy all children
    for(int c=0; c<node.children.size(); c++) {
        children[c] = new Node(*node.children[c]);
    }
}

Node::~Node() {
    if(isCLT())
        delete data;
    else {
        for (auto & child : children) {
            delete child;
        }
    }
}

void Node::setParams(Variable* var, Node* parent, vector<Node*> children, vector<double> probs, CLT* data) {
  this->var = var;
  this->parent = parent;
  this->children = children;
  this->probs = probs;
  this->data = data;
}

bool Node::isCLT() {
    return data != nullptr;
}

bool Node::isValid() {
    return !((var == nullptr && !isCLT()) || (var != nullptr && isCLT()));
}

double Node::quickLL() {
    if (!isValid())
        return 1.0;
    if(!isCLT())
        return log(probs[var->val()]) + children[var->val()]->quickLL();
    return data->quickLL();
}

double Node::probEvid(bool debug) {
  // Validity check
  if (!isValid())
    return -1.0;
  // Check if ChowLiu Tree
  if (isCLT())
    return data->quickProbEvid(debug);
  // If evidence, then return the value of that single branch
  if (var->isSet() && var->val()<var->domain().size()) {
    return probs[var->val()] * children[var->val()]->probEvid(debug);
  }
  // If not, then calculate and sum over all branches
  double total_prob = 0.0;
  for(int c=0; c<children.size(); c++) {
    total_prob += probs[c] * children[c]->probEvid(debug);
  }
  return total_prob;
}

// Assumes that evidence has already been set
double Node::MPE(bool debug) {
  // If node is not valid then stop and do nothing
  if (!isValid())
    return -1.0;
  // If CLT, then call quickMPE
  if (isCLT())
    return data->quickMPE(debug);
  // If current node is evidence, then call MPE on instantiated child
  if (var->isSet())
    return probs[var->val()] * children[var->val()]->MPE(debug);
  // Otherwise, choose the branch with the largest probability
  vector<double> mpe_probs(children.size(), -1.0);
  for(int c=0; c<children.size(); c++) {
    mpe_probs[c] = probs[c] * children[c]->MPE(debug);
  }
  var->val() = argmax(mpe_probs);
  return *max_element(mpe_probs.begin(), mpe_probs.end());
}

// Assumes that evidence has already been set
void Node::sample(bool debug) {
  // If node is not valid then stop and do nothing
  if (!isValid())
    return;
  // If CLT then send dummy evidence and invoke sampling function
  if (isCLT()) {
    data->quickSample(debug);
    return;
  }
  // Select which child to sample from
  if (!var->isSet())
    var->val() = sampleFromDist(probs);
  int c = var->val();
  // Sample from chosen child
  children[c]->sample(debug);
}

// Substitute variables according to variable map
void Node::substituteVars(unordered_map<Variable *, Variable *> &var_map) {
    if(!isValid())
        return;
    if(isCLT())
        data->substituteVars(var_map);
    // Check if current var needs to be substituted
    if (var_map.find(var) != var_map.end())
        var = var_map[var];
    // Call method on all children
    for(auto & child: children) {
        child->substituteVars(var_map);
    }
}

/*************************
Node: Human-Readable String
**************************/
string Node::str(int level, int spacing, string prob, string val, bool verbose) {
    // Calculate spaces
    string spaces = duplicate(" ", level*spacing);
    // Check if node is a CLT
    if (data!=nullptr)
        return spaces + "[" + val + ":" + prob + "] " + data->str(verbose);
    // Add node information
    string s = "";
    s += spaces + "[" + val + ":" + prob + "] " + var->name();
    // Add children information
    for(int c=0; c<children.size(); c++) {
        s += "\n" + children[c]->str(level+1, spacing, to_string(probs[c], 2), to_string(var->domain()[c]), verbose);
    }
    return s;
}

string Node::toJSON(int level, int spacing, int& id) {
    string json_str;
    // Calculate spaces
    string spaces = duplicate(" ", level*spacing);
    // Check if valid
    if(!isValid())
        return json_str;
    // Increment id variable
    string curr_id = to_string(id);
    id++;
    // Check if intermediate node
    if(!isCLT()) {
        // Generate JSON string for each child
        string nodes_json_str;
        for(auto & child : children) {
            nodes_json_str += child->toJSON(level+1, spacing, id) + ",\n";
        }
        // Concatenate to parent JSON string
        json_str += spaces + "{\n" +
                    spaces + " \"Id\": " + curr_id + ",\n" +
                    spaces + " \"Type\": \"tree\",\n" +
                    spaces + " \"Variable\": " + var->toJSON(true) + ",\n" +
                    spaces + " \"Probabilities\": [ " + to_string_vec(probs) + " ],\n" +
                    spaces + " \"Nodes\": [\n" + nodes_json_str.substr(0, nodes_json_str.size()-2) + "\n" +
                    spaces + " ]\n" +
                    spaces + "}";
        // Return JSON string
        return json_str;
    }
    // Convert CLT to JSON
    string vars_json_str;
    for(auto & var: data->vars()) {
        vars_json_str += spaces + "  " + var->toJSON(true) + ",\n";
    }
    string cpts_json_str;
    for(auto & cpt: data->cpts()) {
        cpts_json_str += spaces + "  " + cpt->toJSON(true) + ",\n";
    }
    json_str += spaces + "{\n" +
                spaces + " \"Id\": " + curr_id + ",\n" +
                spaces + " \"Type\": \"leaf\",\n" +
                spaces + " \"Variables\": [\n" + vars_json_str.substr(0, vars_json_str.size()-2) + "\n" +
                spaces + " ],\n" +
                spaces + " \"CPTs\": [\n" + cpts_json_str.substr(0, cpts_json_str.size()-2) + "\n" +
                spaces + " ]\n" +
                spaces + "}";
    // Return JSON string
    return json_str;
}

/*************************
Cutset Network
**************************/
CNet::CNet() {
    _import_vars = true;
    _root = nullptr;
}

CNet::CNet(Node* cnet) {
    _import_vars = true;
    _root = cnet;
}

CNet::CNet(Dataset& data, int max_depth, int clt_threshold, bool smooth) {
    _import_vars = false;
    _root = nullptr;
    // Create domains and variables
    doms() = vector<Domain*>(data.size());
    vector<Variable*> existing_vars(data.size());
    for(int c=0; c<data.size(); c++) {
      doms()[c] = data[c].toDomain(c, "d"+to_string(c));
      existing_vars[c] = new Variable(*doms()[c], c, "v"+to_string(c));
    }
    learnCNet(data, existing_vars, max_depth, clt_threshold, smooth);
  }

CNet::CNet(Dataset& data, vector<Variable*>& existing_vars, int max_depth, int clt_threshold, bool smooth) {
    _import_vars = true;
    _root = nullptr;
    learnCNet(data, existing_vars, max_depth, clt_threshold, smooth);
}

CNet::CNet(CNet& cnet) {
    // Copy domains and variables
    doms() = cnet.doms();
    vars() = cnet.vars();
    // Deep Copy root
    _import_vars = true;
    _root = new Node(*cnet.root());
}

CNet::~CNet() {
    if(!_import_vars) {
        deleteVec(doms());
        deleteVec(vars());
    }
    if (root())
        delete _root;
}

/*************************
CNet: Access
**************************/
vector<Domain*>& CNet::doms() {
    return _doms;
  }

vector<Variable*>& CNet::vars() {
    return _vars;
  }

Node* CNet::root() {
    return _root;
  }

/*************************
CNet: Learning
**************************/
void CNet::learnCNet(Dataset& data, vector<Variable*>& existing_vars,
                        int max_depth, int clt_threshold, bool smooth) {
  // Do nothing for empty dataset
  if (!data.size())
    return;
  // Assign existing variables to vars()
  vars() = existing_vars;
  // Extract doms() from vars()
  doms() = vector<Domain*>(vars().size());
  for(int v=0; v<vars().size(); v++) {
      doms()[v] = &vars()[v]->domain();
  }
  // Then, we need to learn the root using the helper
  _root = learnCNetHelper(data, vars(), 0, max_depth, clt_threshold, smooth);
}

/*************************
CNet: Inference
**************************/
double CNet::LL(vector<string> row, vector<int> evid_indices, bool mixture, bool debug) {
    if(root()==nullptr || (row.size()+mixture)!=vars().size())
        return 1.0;
    // Extract variable indices
    vector<int> vals = extract_var_val_indices(row, vars());
    // Set evidence
    for(int v=0; v<vals.size(); v++) {
        vars()[v]->val() = vals[v];
    }
    // Calculate LL
    double ll = log(root()->probEvid(debug));
    unset_all(vars());
    // If evidence is given, calculate conditional LL
    if (!evid_indices.empty()) {
        for(auto const & e : evid_indices) {
            if(e>=0 && e<vals.size())
                vars()[e]->val() = vals[e];
        }
        ll -= log(root()->probEvid(debug));
        unset_all(vars());
    }
    return ll;
}

double CNet::avgLL(Dataset &dset, bool debug) {
    if(root()==nullptr || dset.size()==0 || dset.nrows()==0)
        return 1.0;
    double total_ll = 0.0;
    for(int i=0; i<dset.nrows(); i++) {
        vector<string> row = dset.row(i);
        vector<int> vals = extract_var_val_indices(row, vars());
        // Set evidence
        for(int v=0; v<vals.size(); v++) {
            vars()[v]->val() = vals[v];
        }
        // Calculate LL for current point
        double curr_ll = root()->quickLL();
        if (curr_ll > 0)
            return 1.0;
        // Add to total LL score
        total_ll += curr_ll;
    }
    return total_ll / dset.nrows();
}

double CNet::quickProbEvid(bool debug) {
    // If no root, then return negative probability
    if (root()==nullptr)
        return -1.0;
    // Call probEvid() on root
    return root()->probEvid(debug);
}

double CNet::probEvid(vector<pair<int,int> > evidence, bool debug) {
  // If no evidence, then sum everything out
  if (evidence.size()==0)
    return 1.0;
  // Set evidence
  unset_all(vars());
  set_evidence(vars(), evidence);
  // Call probEvid() on root
  return quickProbEvid(debug);
}

double CNet::margEvid(vector<pair<int,int> > marg_evidence,
                      vector<pair<int,int> > evidence, bool debug) {
  // If no root, then return negative probability
  if (root()==nullptr)
    return -1.0;
  // Concatenate evidence
  vector<pair<int,int> > all_evidence = vector_union(marg_evidence, evidence);
  // Calculate Pr(m,e)
  double total_prob = probEvid(all_evidence, debug);
  if (debug) cout << "Pr(m,e): " << total_prob << endl;
  if (total_prob <= 0.0)
    return 0.0;
  // Calculate Pr(m|e)
  double evid_prob = probEvid(evidence, debug);
  if (debug) cout << "Pr(e): " << evid_prob << endl;
  return total_prob / evid_prob;
}

pair <double, vector<int> > CNet::quickMPE(bool debug) {
    vector<int> mpe_tuple(vars().size());
    // If no root, then return empty sample
    if (root()==nullptr)
        return pair <double, vector<int> >({-1.0, mpe_tuple});
    // Call MPE recursively on every node
    double mpe_prob = root()->MPE(debug);
    // Build MPE tuple
    for(int v=0; v<vars().size(); v++) {
        mpe_tuple[v] = vars()[v]->val();
    }
    // Return MPE probability and tuple
    return pair <double, vector<int> >({mpe_prob, mpe_tuple});
}

pair <double, vector<int> > CNet::MPE(bool debug) {
  vector <pair<int,int> > evidence;
  return MPE(evidence, debug);
}

pair <double, vector<int> > CNet::MPE(vector<pair<int,int> > evidence, bool debug) {
  vector<int> mpe_tuple(vars().size());
  // Assign evidence to variables
  unset_all(vars());
  for(int e=0; e<evidence.size(); e++) {
    int v = evidence[e].first;
    int val = evidence[e].second;
    if (v>=vars().size() || val >= vars()[v]->domain().size()) {
      unset_all(vars());
      return pair <double, vector<int> >({-1.0, mpe_tuple});
    }
    vars()[v]->val() = val;
  }
  // Return MPE probability and tuple
  return quickMPE(debug);
}

/*************************
CNet: Sampling
**************************/
// Quickly sample based on set variables
void CNet::quickSample(bool debug) {
    if (root() && root()->isValid())
        root()->sample(debug);
}

// Wrapper for forward sampling
pair <double, vector<int> > CNet::sample(bool debug) {
  vector <pair<int,int> > evidence;
  return sample(evidence, debug);
}

// Importance sampling
pair <double, vector<int> > CNet::sample(vector<pair<int,int> > evidence,
                                          bool debug) {
  vector<int> sample_vec(vars().size());
  // If no root, then return empty sample
  if (root()==nullptr)
    return pair <double, vector<int> >({-1.0, sample_vec});
  // Assign evidence to variables
  unset_all(vars());
  for(int e=0; e<evidence.size(); e++) {
    int v = evidence[e].first;
    int val = evidence[e].second;
    if (v>=vars().size() || val >= vars()[v]->domain().size()) {
      unset_all(vars());
      return pair <double, vector<int> >({-1.0, sample_vec});
    }
    vars()[v]->val() = val;
  }
  if (debug) cout << "[DEBUG] Assigned evidence" << endl;
  // Make a list of non-evidence variables because you will need them later
  vector <pair<int,int> > non_evidence(vars().size()-evidence.size());
  int ne = 0;
  for(int v=0; v<vars().size(); v++) {
    if (!vars()[v]->isSet())
      non_evidence[ne++] = pair<int,int>({v,-1});
  }
  if (debug) cout << "[DEBUG] Fixed non-evidence nodes" << endl;
  // Recursively call the sample method for each sampled node in the tree
  if (debug) cout << "[DEBUG] Started sampling from nodes" << endl;
  quickSample(debug);
  if (debug) cout << "[DEBUG] Finished sampling from nodes" << endl;
  // Collate results to build sample
  for(int v=0; v<vars().size(); v++) {
    sample_vec[v] = vars()[v]->val();
  }
  if (debug) cout << "[DEBUG] Collated results into vector" << endl;
  // Calculate weight of importance sample
  double weight = 0.0;
  if (evidence.size()==0 || evidence.size()==vars().size())
    weight = 1.0;
  else {
    // Set non-evidence variables
    for(int ne=0; ne<non_evidence.size(); ne++) {
      int v = non_evidence[ne].first;
      non_evidence[ne].second = vars()[v]->val();
    }
    weight = margEvid(evidence, non_evidence, debug);
  }
  if (debug) cout << "[DEBUG] Calculated sample weight" << endl;
  // Unset variable values and return result
  unset_all(vars());
  return pair <double, vector<int> >({weight, sample_vec});
}

// Multiple samples wrapper
vector<pair<double,vector<int> > > CNet::samples(int m, bool debug) {
  vector <pair<int,int> > evidence;
  return samples(evidence, m, debug);
}

vector<pair<double,vector<int> > > CNet::samples(vector<pair<int,int> > evidence,
                                            int m, bool debug) {
  vector<pair<double,vector<int> > > samples_vec(m);
  for(int i=0; i<m; i++) {
    samples_vec[i] = sample(evidence, debug);
  }
  return samples_vec;
}

/*************************
CNet: Substitute Variables
**************************/
void CNet::substituteVars(unordered_map<Variable*,Variable*>& var_map) {
    // Substitute domains and variables in CNet
    for(int v=0; v<vars().size(); v++) {
        if (var_map.find(vars()[v]) != var_map.end()) {
            vars()[v] = var_map[vars()[v]];
            doms()[v] = &vars()[v]->domain();
        }
    }
    // Substitute domains and variables in nodes
    if (root())
        root()->substituteVars(var_map);
}

/*************************
CNet: String Representation
**************************/
string CNet::str(bool verbose, int spacing) {
  if (root()==nullptr)
    return "NULL";
  return root()->str(0, spacing, "", "", verbose);
}

string CNet::toJSON(int spacing) {
    if (root()==nullptr)
        return "";
    // Generate node strings
    int id = 0;
    string nodes_json_str = root()->toJSON(1, spacing, id);
    // Return complete JSON string
    return "{\n \"CutsetNetwork\": {\n  \"Nodes\": [\n" + nodes_json_str + "\n  ]\n }\n}";
}

/*************************
CNet: Helper Methods
**************************/
Node* CNet::learnCNetHelper(Dataset& data,  vector<Variable*> curr_vars,
                            int curr_depth, int max_depth, int clt_threshold,
                            bool smooth) {
  // If conditions for CLT are met, learn CLT
  if (curr_depth>=max_depth || data.size()<=clt_threshold || data.nrows()<=(curr_vars.size()*3)) {
    CLT* clt = new CLT(data, curr_vars);
    return new Node(nullptr, nullptr, vector<Node*>(), vector<double>(), clt);
  }
  // If not, then first find the variable with the largest MI
  int var_index = 0;
  double max_mi = 0.0;
  vector< vector<double> > mi_matrix = data.MIMatrix();
  for(int v=0; v<mi_matrix.size(); v++) {
    double curr_mi = 0.0;
    for(int w=0; w<mi_matrix[v].size(); w++) {
      curr_mi += mi_matrix[v][w];
    }
    if (curr_mi > max_mi) {
      max_mi = curr_mi;
      var_index = v;
    }
  }
  // Second, remove this variable from curr_doms and curr_vars
  Variable* var = curr_vars[var_index];
  curr_vars.erase(curr_vars.begin()+var_index);
  // Third, learn probability values and children
  double z = 0.0;
  double smooth_factor = (data.nrows() * 0.001) / var->domain().size();
  vector<Node*> children(var->domain().size());
  vector<double> probs(var->domain().size());
  for(int c=0; c<var->domain().size(); c++) {
    // Learn child conditioned on current domain value
    string val = var->domain()[c];
    vector <pair<int,string> > filter = zip(vector<int>({var_index}), vector<string>({val}));
    Dataset* filtered_data = data.filterByValues(filter);
    Dataset* new_data = filtered_data->dropCols(set<int>({var_index}));
    children[c] = learnCNetHelper(*new_data, curr_vars, curr_depth+1, max_depth, clt_threshold, smooth);
    // Calculate unnormalized probability of current branch
    // probs[c] = (smooth) ? data[var_index].count(val) + smooth_factor : data[var_index].count(val);
    probs[c] = (smooth) ? vec_sum(filtered_data->weights()) + smooth_factor : vec_sum(filtered_data->weights());
    z += probs[c];
    // Garbage collection
    delete filtered_data;
    delete new_data;
  }
  // Fourth, normalize the probabilities
  if (z>0.0) {
    for(int c=0; c<probs.size(); c++) {
        probs[c] /= z;
    }
  }
  // Finally, create a new node and return
  Node* root = new Node(var, nullptr, children, probs);
  return root;
}
