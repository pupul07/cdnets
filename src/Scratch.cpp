#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include "Dataset.h"
#include "MixCDNet.h"
#include "AndCDNet.h"
#include "DCN.h"

using namespace std;

int main (int argc, char* argv[]) {
    // Import dataset
    string infile_path = "/Users/chiradeep/dev/cdnets/data/misc/mixand1.csv";
    Dataset data(infile_path);
    // Set seed
    setSeed(1991);
    // Create MixAndCDNet model
    vector<int> x_indices({0});
    MixAndCDNet* mix = new MixAndCDNet(data, x_indices, 1, 1,
                                        1, 10, 2, 1);
    // Delete MixAndCDNet model
    delete mix;
    return 0;
}

/*
// Import dataset
string infile_path = "data/test.csv";
Dataset data(infile_path);
// Check how many columns were read
cout << "Number of columns: " << data.size() << endl;
// Convert columns to variables
vector<Domain> doms(data.size());
vector<Variable> vars(data.size());
for(int c=0; c<data.size(); c++) {
  doms[c] = data[c].toDomain(c, "d"+to_string(c));ca
  vars[c] = Variable(doms[c], c, "v"+to_string(c));
  cout << vars[c].str() << endl;
}
// Convert to Function
vector<int> indices({0,1});
Function* func = discreteColsToFunction(vars, data, indices);
cout << func->str() << endl;
delete func;
// Convert to CPT
vector<int> marg_indices({1});
CPT* cpt = discreteColsToCPT(vars, data, indices, marg_indices);
cout << cpt->str() << endl;
delete cpt;
*/

/*
Code to perform variable elimination on a CLT
*/
/* // Import dataset
string infile_path = "data/test.csv";
Dataset data(infile_path);
// Learn CLT
CLT clt(data);
// Print structure
cout << clt.str(true) << endl;
// Perform Inference
vector < pair<int,int> > evidence = zip(vector<int>({1}), vector<int>({0}));
double prob = clt.probEvid(evidence);
cout << "Pr(v1=0): " << prob << endl; */

/*
Code to filter columns in datasets
*/
/* // Import dataset
string infile_path = "data/test.csv";
Dataset data(infile_path);
// Test Filtering
vector <pair<int,string> > filters = zip(vector<int>({0,5}), vector<string>({"0", "0"}));
Dataset new_data = data.filterByValues(filters);
// Print filtered dataset
cout << new_data.str() << endl; */

/*
Code to test cutset networks
*/
/* // Import dataset
string infile_path = "data/test.csv";
Dataset data(infile_path);
// Create Cutset Network
CNet cnet(data, 3, 3);
// Print structure
cout << cnet.str() << endl; */

/*
Code to perform inference in cutset networks
*/
/* // Import dataset
string infile_path = "data/test.csv";
Dataset data(infile_path);
// Learn CLT
CNet cnet(data,2);
// Print structure
cout << cnet.str() << endl;
// Perform Inference
vector < pair<int,int> > evidence = zip(vector<int>({0,1}), vector<int>({0,0}));
double prob = cnet.probEvid(evidence);
cout << "Pr(v0=0,v1=0): " << prob << endl; */

/*
Code to perform likelihood weighting in cutset networks
*/
/* // Import dataset
string infile_path = "data/test.csv";
Dataset data(infile_path);
// Create Cutset Network
CNet cnet(data, 3, 4);
// Print structure
cout << cnet.str() << endl;
// Sample
int m = 10;
vector< pair <double, vector<int> > > samples = cnet.samples(m);
// Print answer
for(int i=0; i<m; i++) {
  cout << samples[i].first << ": " << to_string_vec(samples[i].second) << endl;
} */

/*
Code to perform MPE in CLTs
*/
/* // Import dataset
string infile_path = "../data/data1.csv";
Dataset data(infile_path);
// Learn CLT
CLT clt(data);
// Print structure
cout << clt.str(true) << endl;
// Calculate MPE
vector<pair<int,int> > evidence;
pair<double,vector<int> > mpe = clt.MPE(evidence);
// Print MPE
cout << "MPE Probability: " << mpe.first << endl;
cout << "MPE Tuple: " << to_string_vec(mpe.second) << endl; */

/*
Code to perform inference in CDNets
*/
/* // Import dataset
string infile_path = "../data/data1.csv";
Dataset data(infile_path);
// Create CDNet
CDNet cdnet(data, vector<int>({1}), 2, 2, 1);
// Print CDNet
cout << cdnet.str() << endl;
cout << cdnet.nleaves() << endl;
// Prob evid
vector<pair<int,int> > evids = zip(vector<int>({1}), vector<int>({1}));
cout << cdnet.probEvid(evids) << endl;
// MPE
pair <double, vector<int> > mpe = cdnet.MPE(evids);
cout << mpe.first << ": " << to_string_vec(mpe.second) << endl;
// Samples
setSeed(100);
vector<pair <double, vector<int> > > samples = cdnet.samples(evids, 10);
for(int i=0; i<samples.size(); i++) {
cout << samples[i].first << ": " << to_string_vec(samples[i].second) << endl;
} */

/*
 * Code to multiply P(X) with P(Y|X)
 */
/* // Import dataset
string infile_path = "../data/temporal1.csv";
Dataset data(infile_path);
// Convert to domains and variables
vector<Domain*> doms(data.size());
vector<Variable*> vars(data.size());
for(int v=0; v<vars.size(); v++) {
doms[v] = data[v].toDomain(v, "d"+to_string(v));
vars[v] = new Variable(*doms[v], v, "v"+to_string(v));
}
// Create CNet on prior model
Dataset& data_new = *(data.dropCols(set<int>({0,1})));
vector<Variable*> prior_vars(vars.begin()+2, vars.begin()+4);
CNet prior(data_new, prior_vars, 0, 1);
cout << prior.str() << endl;
// Create CDNet on transition model
CDNet transition(data, vars, vector<int>({2,3}), 2, 2, 1);
cout << transition.str() << endl;
// Create mixture network (generative)
CNet& msg = *(transition.multiplyAndMarginalize(prior, vector<pair<int,int> >({})));
cout << msg.str() << endl; */

/*
 * Code to perform inference with DCNs
 */
/* // Create DCN
string train_path = "../data/temporal2.train";
string train_seqs_path = "../data/temporal2.train.seq";
Dataset train_data(train_path);
vector<int> train_seqs = readSeqFromFile(train_seqs_path, ',');
DCN dcn(train_data, train_seqs, 0, 1, 2,0,1);
// Print DCN parameters
cout << dcn.prior_dist().str() << endl;
cout << dcn.transition_dist().str() << endl;
// Perform inference
string test_path = "../data/temporal2.test";
string test_seqs_path = "../data/temporal2.test.seq";
Dataset test_data(test_path);
vector<int> test_seqs = readSeqFromFile(test_seqs_path, ',');
vector<int> evid_indices({});
cout << "Avg LL: " << dcn.forwardLL(test_data, test_seqs, evid_indices) << endl; */

/*
 * Code to test multiplyAndMarginalize in MixCDNets
 */
/* // Import dataset
string infile_path1 = "../data/misc/data1.csv";
string infile_path2 = "../data/misc/data2.csv";
Dataset data1(infile_path1);
Dataset data2(infile_path2);
// Create variables
vector<Domain*> doms(data2.size());
vector<Variable*> vars(data2.size());
for(int v=0; v<vars.size(); v++) {
doms[v] = data2[v].toDomain(v, "d"+to_string(v));
vars[v] = new Variable(*doms[v], v, "v"+to_string(v));
}
// Create CNet
vector<Variable*> cnet_vars(vars.begin(), vars.end()-1);
CNet* cnet = new CNet(data1, cnet_vars, 0, 1);
cout << cnet->str() << endl;
// Create MixCDNet
vector<int> x_indices({0,1});
MixCDNet* mixcdnet = new MixCDNet1(data2, vars,x_indices, 0, 1);
cout << mixcdnet->str() << endl;
// Multiply and Marginalize
CNet* msg = mixcdnet->quickMultiplyAndMarginalize(*cnet);
cout << msg->str() << endl; */

/* // Import Dataset
string train_path = "../data/temporal/temporal2.train";
string train_seqs_path = "../data/temporal/temporal2.train.seq";
Dataset train_data(train_path);
vector<int> train_seqs = readSeqFromFile(train_seqs_path, ',');
setSeed(1991);
DCN dcn(train_data, train_seqs, 0, 1, 2,0,1);
// Print DCN parameters
cout << dcn.prior_dist()->str() << endl;
cout << dcn.transition_dist()->str() << endl;
// Load test set
string test_path = "../data/temporal/temporal2.test";
string test_seqs_path = "../data/temporal/temporal2.test.seq";
Dataset test_data(test_path);
vector<int> test_seqs = readSeqFromFile(test_seqs_path, ',');
// Print log-likelihood of test set
cout << dcn.forwardLL(test_data, test_seqs) << endl;
return 0; */