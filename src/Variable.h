#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <iostream>
#include <vector>
#include "Utilities.h"

using namespace std;

const string null = "NULL";

struct Domain {
  int _id;
  string _name;
  vector<string> _vals;
  Domain(vector<string> = {}, int=0, string="");
  void set(vector<string> = {}, int=0, string="");
  int& id();
  string& name();
  vector<string>& vals();
  int size();
  int indexOf(string);
  const string &operator [](int);
  string str();
};

struct Variable {
  int _id;
  string _name;
  Domain* _domain;
  int _val;
  Variable() {};
  Variable(Domain&, int=0, string="");
  void set(Domain&, int=0, string="");
  int id();
  string name();
  int& val();
  string domainVal();
  Domain& domain();
  int indexOf(string);
  bool isSet();
  void unset();
  void setToZero();
  const string str();
  const string toJSON(bool=false);
};

int calc_address(vector<Variable*>&);
void unset_all(vector<Variable*>&);
void set_all_to_zero(vector<Variable*>&);
void set_evidence(vector<Variable*>&, vector<pair<int,int> >);
vector<Variable*> is_set_vars(vector<Variable*>&);
vector<Variable*> is_unset_vars(vector<Variable*>&);

#endif
