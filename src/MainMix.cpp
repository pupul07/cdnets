#include <iostream>
#include <string>
#include <vector>
#include "Dataset.h"
#include "DCN.h"

using namespace std;

string usage_string = "Usage:\n"
                      "Main <data-path> <data-name> <max-depth-prior> <clt-threshold-prior> "
                      "<max-cond-depth> <max-depth> <clt-threshold> [ <num-comps> <max-part-size> <max-iter <evidence> ]\n\n"
                      "Arguments:\n"
                      "<data-path>\t\t\tPath to datasets\n"
                      "<data-name>\t\t\tName of dataset\n"
                      "<max-depth-prior>\t\tMax Depth of prior cutset network\n"
                      "<clt-threshold-prior>\t\tThreshold number of variables for learning CLT in prior cutset network\n"
                      "<max-cond-depth>\t\tMax Depth of decision part of transition CDNet\n"
                      "<max-depth>\t\t\tMax Depth of leaf networks of CDNet\n"
                      "<clt-threshold>\t\t\tCLT threshold for leaf networks of DCNet\n"
                      "<num-comps>\t\t\tNumber of components for mixture model (default: 1)\n"
                      "<max-iter>\t\t\tMaximum number of iterations for EM (default: 10)\n"
                      "<evidence>\t\t\tEvidence in the form of index_1,..,index_n";

int main(int argc, char* argv[]) {
    // Check if all arguments are passed
    if (argc < 8) {
        cout << usage_string << endl;
        return 1;
    }
    // Parse arguments
    string data_dir = trim(argv[1]);
    string data_name = trim(argv[2]);
    string train_path = data_dir + "/" + data_name + ".train";
    string train_seqs_path = data_dir + "/" + data_name + ".train.seq";
    string test_path = data_dir + "/" + data_name + ".test";
    string test_seqs_path = data_dir + "/" + data_name + ".test.seq";
    string doms_path = data_dir + "/" + data_name + ".doms";
    int max_depth_prior = stoi(argv[3]);
    int clt_threshold_prior = stoi(argv[4]);
    int max_cond_depth = stoi(argv[5]);
    int max_depth = stoi(argv[6]);
    int clt_threshold = stoi(argv[7]);
    int num_comps = (argc>=9) ? stoi(argv[8]) : 1;
    int maxiter = (argc>=10) ? stoi(argv[9]) : 10;
    vector<int> evid_indices = (argc>=11) ? stringToVecInt(argv[10],",") : vector<int>({});
    // Print arguments
    cout << "================================" << endl;
    cout << "Dynamic Custset Networks" << endl;
    cout << "================================" << endl;
    cout << "Training Set: " << train_path << endl;
    cout << "Training Sequences: " << train_seqs_path << endl;
    cout << "Test Set: " << test_path << endl;
    cout << "Test Sequences: " << test_seqs_path << endl;
    cout << "Max Depth (Prior): " << max_depth_prior << endl;
    cout << "CLT Threshold (Prior): " << clt_threshold_prior << endl;
    cout << "Max Conditional Depth : " << max_cond_depth << endl;
    cout << "Max Depth (Transition): " << max_depth << endl;
    cout << "CLT Threshold (Transition): " << clt_threshold << endl;
    cout << "Number of Components: " << num_comps << endl;
    cout << "Max Iterations: " << maxiter << endl;
    cout << "Evidence Indices: " << to_string_vec(evid_indices) << endl;
    // Begin learning
    cout << "================================" << endl;
    cout << "Learning" << endl;
    cout << "================================" << endl;
    Dataset train_data(train_path);
    vector<int> train_seqs = readSeqFromFile(train_seqs_path, ',');
    vector<Domain*> doms = readDomsFromFile(doms_path, ",", true);
    vector<int> model_params({max_cond_depth, max_depth, clt_threshold, num_comps, maxiter});
    DCN<MixAndCDNet> dcn(train_data, train_seqs, max_depth_prior, clt_threshold_prior, model_params, doms);
    cout << dcn.prior_dist()->str() << endl;
    cout << dcn.transition_dist()->str() << endl;
    // Begin inference
    cout << "================================" << endl;
    cout << "Inference" << endl;
    cout << "================================" << endl;
    Dataset test_data(test_path);
    vector<int> test_seqs = readSeqFromFile(test_seqs_path, ',');
    // cout << "Avg LL: " << dcn.forwardLL(test_data, test_seqs, evid_indices) << endl;
    cout << "Avg LL: " << dcn.forwardLL(test_data, test_seqs) << endl;

    return 0;
}
