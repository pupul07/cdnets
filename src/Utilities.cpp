#include <iostream>
#include <string>
#include <vector>
#include "Utilities.h"

using namespace std;

default_random_engine generator;

const string to_string(string str) {
  return str;
}

const string to_string(double d, int precision) {
  stringstream stream;
  stream << fixed << setprecision(precision) << d;
  return stream.str();
}

const string duplicate(string str, int multiplier) {
  if (!str.size())
    return "";
  string s = "";
  for(int i=1; i<=multiplier; i++) {
    s += str;
  }
  return s;
}

vector<string> split(string s, string delim, bool to_trim) {
    int pos = 0;
    string token;
    vector<string> tokens;
    while ((pos = s.find(delim)) != string::npos) {
        token = to_trim ? trim(s.substr(0, pos)) : s.substr(0, pos);
        tokens.push_back(token);
        s.erase(0, pos + delim.length());
    }
    tokens.push_back(trim(s));
    tokens[tokens.size()-1] = to_trim ? trim(s) : s;
    return tokens;
}

vector< pair<int,int> > dfs_sort(set< pair<int,int> > graph) {
  vector< pair<int,int> > sorted_edges;
  // Check if size is empty
  if (graph.size()==0)
    return sorted_edges;
  // Find roots
  set<int> all_i, all_j;
  for(auto& edge: graph) {
    all_i.insert(edge.first);
    all_j.insert(edge.second);
  }
  set<int> roots = quick_difference(all_i, all_j);
  // Initialize stack with roots
  vector<int> stack(roots.begin(), roots.end());
  // Sort as long as stack has something in it
  while (stack.size()!=0) {
    int i = stack[0], j = -1;
    // Find j where (i,j) is some edge in the graph
    for(auto& edge: graph) {
      if(i==edge.first) {
        j = edge.second;
        break;
      }
    }
    // If no (i,j) exists, pop the current root
    if (j==-1) {
      stack.erase(stack.begin());
      continue;
    }
    // Else if (i,j) does exist, push it into sorted_edges
    sorted_edges.push_back( pair<int,int>({i,j}) );
    graph.erase(sorted_edges.back());
    // Insert j into the stack as a root
    stack.insert(stack.begin(), j);
  }
  return sorted_edges;
}

string trim(const string& str,
            const string& whitespace)
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

/*********
* This function takes in a vector of (index, evidence) pairs
* and pops out the (index, evidence) pair that matches index.
* It also adjusts the index numbers of other evidences accordingly.
*
* Ex:
* vector<int,int> evidence({ pair<int,int>({5,0}),
                              pair<int,int>({7,0}),
                              pair<int,int>({3,1})})
* evidencePop(evidence, 5)
* will return (5,0) and evidence = {(6,0),(3,1)}
*********/
pair<int,int> evidencePop(vector < pair<int,int> >& evidence, int index) {
  pair<int,int> index_evidence({-1,-1});
  int index_pos = -1;
  for(int e=0; e<evidence.size(); e++) {
    if (evidence[e].first==index) {
      index_pos = e;
      index_evidence = evidence[e];
    }
    else if (evidence[e].first > index)
      evidence[e].first--;
  }
  if(index_pos>=0)
    evidence.erase(evidence.begin()+index_pos);
  return index_evidence;
}

/*********
* 1D version of evidencePop()
*
* Ex:
* vector<int> evidence({5,7,3})
* evidencePop1d(evidence, 5)
* will change evidence to {(6,0),(3,1)}
*********/
void evidencePop1d(vector<int>& evidence, int index) {
    int index_pos = -1;
    for(int e=0; e<evidence.size(); e++) {
        if (evidence[e]==index)
            index_pos = e;
        else if (evidence[e] > index)
            evidence[e]--;
    }
    if(index_pos>=0)
        evidence.erase(evidence.begin()+index_pos);
}

/*********
* This function seeds the random number generator
*
* Ex:
* setSeed(100);
*********/
void setSeed(unsigned int seed) {
  generator.seed(seed);
}

/*********
* This function takes in a discrete distribution and uses
* the global random number generator to sample from it
*
* Ex:
* vector<double> probs = { 0.2, 0.3, 0.5 }
* sample(probs);
*********/
int sampleFromDist(vector<double>& probs) {
  if (probs.size()<=1)
    return probs.size()-1;
  discrete_distribution<int> prob_dist(probs.begin(), probs.end());
  return prob_dist(generator);
}

/*********
 * This function simply generates a set containing
 * a sequence of numbers [ start, end ]
 *
 * Ex:
 * setRange(5,100);
 *
 * This will generate { 5, 6, .., 100 }
*********/
set<int> setRange(int start, int end) {
    if (start>end)
        return set<int>();
    set<int> range;
    for(int i=start; i<=end; i++) {
        range.insert(i);
    }
    return range;
}

/*********
 * This function simply generates a string vector
 * of the sequence [ start, end ] for a string
 * domain
 *
 * Ex:
 * domRange(5,100);
 *
 * This will generate { "5", .., "100" }
*********/
vector<string> domRange(int start, int end) {
    if (start>end)
        return vector<string>();
    vector<string> range(end-start+1);
    for(int i=0; i<range.size(); i++) {
        range[i] = to_string(start+i);
    }
    return range;
}

/*********
 * This function reads in a delimited file that contains
 * a sequence of integers in the following form:
 * i1, i2, ..., in
 *
 * Ex:
 * readSeqFromFile("path/to/file", ',');
*********/
vector<int> readSeqFromFile(string filepath, char delim) {
    ifstream infile(filepath);
    string str;
    vector<int> vec;
    while (getline(infile, str, delim)) {
        vec.push_back(stoi(trim(str)));
    }
    return vec;
}

/*********
 * This function converts a delimited string of the
 * form i1, .. , in into an array of integers
 *
 * Ex:
 * stringToVecInt("1,2,3,4", ',');
*********/
vector<int> stringToVecInt(string str, string delim) {
    vector<int> vec;
    vector<string> str_vec = split(str, delim);
    for(auto & str: str_vec) {
        try{
            int i = stoi(str);
            vec.push_back(i);
        } catch (...) {
            continue;
        }
    }
    return vec;
}

/*********
 * This function takes in a weight array with doubles
 * and an index array with integers as input.
 * It then finds the summation of weights over the
 * given indices
 *
 * Ex:
 * vector<double> weights({0.2, 0.3, 0.4, 0.2, 0.1, 0.3, 0.2, 0.1, 0.3});
 * vector<int> indices({2, 5, 8});
 * sumWeights(weights, indices);
 *
 * This will give 1.0
*********/
double sumWeights(vector<double>& weights, vector<int>& indices) {
    double sum = 0.0;
    for(auto const & i : indices) {
        if (i>=0 && i < weights.size())
            sum += weights[i];
    }
    return sum;
}

/*********
 * This function takes in a weight array with doubles
 * and an index array with integers as input.
 * It then finds the summation of weights over the
 * given indices
 *
 * Ex:
 * vector<double> weights({0.2, 0.3, 0.4, 0.2, 0.1, 0.3, 0.2, 0.1, 0.3});
 * vector<int> indices({2, 5, 8});
 * sumWeights(weights, indices);
 *
 * This will give 1.0
*********/
vector<double> selectWeights(vector<double>& weights, vector<int>& indices) {
    vector<double> filtered_weights(indices.size());
    int count=0;
    for(auto const & i : indices) {
        if (i>=0 && i < weights.size())
            filtered_weights[count++] = weights[i];
    }
    if(count<indices.size())
        filtered_weights.erase(filtered_weights.begin()+count, filtered_weights.begin() + indices.size());
    return filtered_weights;
}

/*********
 * This function generates a vector of doubles of 'size' elements
 * with values randomly sampled from a uniform distribution
 * between (lower,upper) [ default: (0.0, 1.0) ]
 *
 * Ex:
 * randomWeightVec(5, 0.0, 10.0);
 *
 * Sample output:
 * { 5.3, 2.1, 0.3, 9.9, 8.7 }
*********/
vector<double> randomWeightVec(int size, double lower, double upper, bool to_normalize) {
    if(size<=0)
        return vector<double>();
    vector<double> weights(size);
    uniform_real_distribution<double> distribution(lower,upper);
    double Z = 0.0;
    for(auto & weight: weights) {
        weight = distribution(generator);
        Z += weight;
    }
    if(to_normalize) {
        for(auto & weight: weights) {
            weight /= Z;
        }
    }
    return weights;
}

/*********
 * This function generates a 2D vector of doubles of n * size
 * dimensions with values randomly sampled from a uniform distribution
 * between (lower,upper) [ default: (0.0, 1.0) ]
 *
 * Ex:
 * randomWeightVec(2, 5, 0.0, 10.0);
 *
 * Sample output:
 * { { 5.3, 2.1, 0.3, 9.9, 8.7 },
 *   { 1.3, 7.5, 2.3, 4.4, 6.6 } }
*********/
vector< vector<double> > randomWeightVecs(int n, int size, double lower, double upper, bool to_normalize) {
    if(n<=0 || size<=0)
        return vector< vector<double> >();
    vector < vector<double> > all_weights(n, vector<double>(size));
    for(auto & weights: all_weights) {
        weights = randomWeightVec(size, lower, upper, to_normalize);
    }
    return all_weights;
}

/*********
 * This function takes in two SORTED vectors:
 * org_indices: vector containing set of some indices
 * dropped_indices: vector containing set of dropped indices
 *
 * This function returns a set of non-deleted org_indices with
 * their index numbers adjusted to match the indices that
 * have been dropped from a dataset
 *
 * Ex:
 * org_indices: [ 0, 2, 4, 6 ]
 * dropped_indices: [ 2, 3, 5 ]
 *
 * The function will return
 * new_indices: [ 0, 3, 4 ]
 *
 * 2 is deleted and the indices of 4 and 6 change to 3 and
 * 4 respectively since indices 3 and 5 from the original
 * index list are removed
 *
*********/
vector<int> recalc_indices(vector<int>& org_indices, vector<int>& dropped_indices, bool to_sort) {
    // Sort if required
    if(to_sort) {
        sort(org_indices.begin(), org_indices.end());
        sort(dropped_indices.begin(), dropped_indices.end());
    }
    int i=0, j=0;
    int dec_val = 0;
    vector<int> new_indices = vector<int>(org_indices);
    while(i<new_indices.size()) {
        if(j>=dropped_indices.size() or new_indices[i]<dropped_indices[j]) {
            new_indices[i++] -= dec_val;
            continue;
        }
        if (new_indices[i]==dropped_indices[j])
            new_indices.erase(new_indices.begin()+i);
        dec_val += 1;
        j++;
    }
    return new_indices;
}