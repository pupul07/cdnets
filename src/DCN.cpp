#include "DCN.h"

using namespace std;

/*************************
Miscellaneous Functions
**************************/
Dataset* createTransitionData(Dataset& data, vector<int> seq) {
    // Prepare sequence for removal
    vector<int> to_remove_curr = cumsum(seq);
    vector<int> to_remove_prev = addConstant(to_remove_curr,-1);
    to_remove_curr.insert(to_remove_curr.begin(), 0);
    to_remove_curr.erase(to_remove_curr.end()-1);
    // Generate curr and prev columns
    vector<DiscreteColumn<string>*> curr_cols(data.size()), prev_cols(data.size());
    for(int c=0; c<data.size(); c++) {
        curr_cols[c] = data[c].removeByIndex(to_remove_curr);
        prev_cols[c] = data[c].removeByIndex(to_remove_prev);
    }
    // Concatenate curr and prev columns
    vector<DiscreteColumn<string>*> all_cols;
    all_cols = vector_union(curr_cols, prev_cols);
    // Return new dataset
    return new Dataset(all_cols);
}