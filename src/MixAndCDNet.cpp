#include "MixAndCDNet.h"

/****************************
MIXTURE OF AndCDNets
*****************************/
/****************************
MixAndCDNet: Constructors
*****************************/
MixAndCDNet::MixAndCDNet(Dataset& data, vector<int>& x_indices, int max_cond_depth, int max_depth,
                        int clt_threshold, int maxiter, int num_comps) {
    _import_vars = false;
    _nleaves = 0;
    learn(data, x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, num_comps);
}

MixAndCDNet::MixAndCDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices, int max_cond_depth,
                         int max_depth, int clt_threshold, int maxiter, int num_comps) {
    _import_vars = true;
    _nleaves = 0;
    learn(data, existing_vars,x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, num_comps);
}

MixAndCDNet::~MixAndCDNet() {
    if(!import_vars()) {
        deleteVec(doms());
        deleteVec(vars());
    }
    deleteVec(andcdnets());
}

/****************************
MixAndCDNet: Access
*****************************/
vector<Domain*>& MixAndCDNet::doms() {
    return _doms;
}

vector<Variable*>& MixAndCDNet::vars() {
    return _vars;
}

vector<Variable*>& MixAndCDNet::y_vars() {
    return _y_vars;
}

vector<Variable*>& MixAndCDNet::x_vars() {
    return _x_vars;
}

vector<AndCDNet*>& MixAndCDNet::andcdnets() {
    return _andcdnets;
}

vector<double>& MixAndCDNet::probs() {
    return _probs;
}

int MixAndCDNet::nleaves() {
    return _nleaves;
}

bool& MixAndCDNet::import_vars() {
    return _import_vars;
}

/****************************
MixAndCDNet: Learning
*****************************/
void MixAndCDNet::learn(Dataset& data, vector<int>& x_indices, int max_cond_depth, int max_depth,
                        int clt_threshold, int maxiter, int num_comps) {
    // Create variables
    doms() = vector<Domain*>(data.size());
    vars() = vector<Variable*>(data.size());
    for(int v=0; v<vars().size(); v++) {
        doms()[v] = data[v].toDomain(v, "d"+to_string(v));
        vars()[v] = new Variable(*doms()[v], v, "v"+to_string(v));
    }
    // Call learn with existing variables
    learn(data, vars(), x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, num_comps);
    import_vars() = false;
}

void MixAndCDNet::learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices, int max_cond_depth,
                      int max_depth, int clt_threshold, int maxiter, int num_comps) {
    // 0. Get vars()
    import_vars() = true;
    doms() = vector<Domain *>(existing_vars.size());
    vars() = existing_vars;
    num_comps = min(num_comps, (int)vars().size());
   // int num_parts = (int)((vars().size()/2)/max_part_size) + (((vars().size()/2)%max_part_size)!=0);
    // 1. Segregate into x and y
    vector<int> x_indices_final;
    for (int c = 0; c < data.size(); c++) {
        // Assign domain from existing_vars
        doms()[c] = &vars()[c]->domain();
        // Add to x_vars if evidence, otherwise add to y_vars
        if (find(x_indices.begin(), x_indices.end(), c) != x_indices.end()) {
            x_vars().push_back(vars()[c]);
            x_indices_final.push_back(c);
        } else
            y_vars().push_back(vars()[c]);
    }
    // Generate meta_indices
    vector<int> x_meta_indices(num_comps);
    iota(x_meta_indices.begin(), x_meta_indices.end(), 0);
    // 2. Perform Expectation Maximization to learn mixture model
    vector<double> old_probs;
    // _probs = vector<double>(x_meta_indices.size(), 1.0);
    _probs = randomWeightVec(x_meta_indices.size(), 1.0, 100.0, true);
    _andcdnets = vector<AndCDNet *>(x_meta_indices.size(), nullptr);
    // _probs = vector<double>(randomWeightVec(x_vars().size(), 0.0, 1.0,true));
    // vector<vector<double> > all_weights(x_vars().size(), vector<double>(data.nrows(), 1.0));
    vector<vector<double> > all_weights(randomWeightVecs(x_meta_indices.size(), data.nrows(), 1.0, 1000.0));
    /* vector< vector< vector<double> > > and_weights(x_meta_indices.size(),
                                                   vector<vector<double> >(num_parts,
                                                                           vector<double>(data.nrows()))); */
    double old_ll = -9999.0;
    for (int j = 0; j < maxiter; j++) {
        cout << "EM Iteration " << (j+1) << endl;
        // 2a. Learn AndCDNets
        old_probs = _probs;
        // _probs = vector<double>(x_meta_indices.size(), 0.0);
        //deleteVec(_andcdnets);
        //_andcdnets = vector<AndCDNet *>(x_meta_indices.size(), nullptr);
        _nleaves = 0;
        vector<double> weight_sums(data.nrows(), 0.0);
        // vector<vector<double > > and_weight_sums(num_parts, vector<double>(data.nrows()));
        for (int k = 0; k < x_meta_indices.size(); k++) {
            data.weights() = all_weights[k];
            if (!andcdnets()[k])
                /* andcdnets()[k] = new AndCDNet(data, vars(),x_indices_final, max_cond_depth,
                                            max_depth, clt_threshold, max_part_size); */
                andcdnets()[k] = new AndCDNet(data, vars(),x_indices_final, max_cond_depth,
                                              max_depth, clt_threshold, k+1);
            else
                andcdnets()[k]->updateParams(data, vars(), x_indices_final, max_depth,
                                             clt_threshold);
                /* andcdnets()[k]->updateParams(data, vars(), x_indices_final, max_depth,
                                                clt_threshold, and_weights[k]); */
            _nleaves += andcdnets()[k]->nleaves();
        }
        // cout << str(true) << endl;
        // 2b. Update weights
        for (int k = 0; k < x_meta_indices.size(); k++) {
            for(int i=0; i<data.nrows(); i++) {
                all_weights[k][i] = old_probs[k] * exp(andcdnets()[k]->LL(data.row(i)));
                weight_sums[i] += all_weights[k][i];
            }
        }
        // 2c. Learn mixture probabilities
        _probs = vector<double>(x_meta_indices.size(), 0.0);
        for (int k = 0; k < x_meta_indices.size(); k++) {
            for(int i=0; i<data.nrows(); i++) {
                all_weights[k][i] = (weight_sums[i]) ? all_weights[k][i] / weight_sums[i] : all_weights[k][i];
                _probs[k] += all_weights[k][i];
            }
            _probs[k] /= (!data.nrows()) ? 1.0 : data.nrows();
        }
        // 2d. Check log-likelihood
        double ll = avgLL(data);
        cout << "Log Likelihood: " << ll << endl;
        // 2e. Check for convergence
        if (abs(ll-old_ll)<0.001)
            break;
        old_ll = ll;
    }
}


/****************************
MixAndCDNet: Inference
*****************************/
double MixAndCDNet::avgLL(Dataset& data) {
    if (!data.nrows())
        return 1.0;
    double prob = 0.0;
    for(int c = 0; c < andcdnets().size(); c++) {
        prob += probs()[c] * exp(andcdnets()[c]->avgLL(data));
    }
    return log(prob);
}

double MixAndCDNet::probEvid(vector<pair<int, int> >& evids) {
    // Set evidence
    unset_all(vars());
    set_evidence(vars(), move(evids));
    if (!is_unset_vars(x_vars()).empty())
        return -1.0;
    // Call probEvid for each root node
    double prob = 0.0;
    for(int c=0; c<andcdnets().size(); c++) {
        prob += probs()[c] * andcdnets()[c]->quickProbEvid();
    }
    return prob;
}

/******************************
AndCDNet: String Representation
*******************************/
string MixAndCDNet::str(bool verbose, int spacing) {
    string str = "";
    for(int c=0; c<andcdnets().size(); c++) {
        str += "[" + to_string(c) + ":" + to_string(probs()[c], 2) + "]\n" + \
                andcdnets()[c]->str(verbose, spacing) + "\n";
    }
    return str;
}



