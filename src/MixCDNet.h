#ifndef MIXCDNET_H
#define MIXCDNET_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include "Dataset.h"
#include "CDNet.h"

using namespace std;

/*************************
MIXTURE OF CDNets
**************************/
class MixCDNet {
    protected:
        vector<Domain*> _doms;
        vector<Variable*> _vars;
        vector<Variable*> _y_vars;
        vector<Variable*> _x_vars;
        vector<CDNet*> _cdnets;
        vector<double> _probs;
        int _nleaves;
        bool _import_vars;
    public:
        // Constructors
        MixCDNet() { _import_vars = false; _nleaves = 0; };
        // Access
        vector<Domain*>& doms();
        vector<Variable*>& vars();
        vector<Variable*>& y_vars();
        vector<Variable*>& x_vars();
        vector<CDNet*>& cdnets();
        vector<double>& probs();
        int nleaves();
        bool& import_vars();
        // Learning
        void learn(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                    int max_depth, int clt_threshold, int maxiter=100, int limit=0);
        virtual void learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                       int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) {};
        // Inference
        double avgLL(Dataset& data);
        double probEvid(vector<pair<int, int> >& evids);
        CNet* quickMultiplyAndMarginalize(CNet& x_dist);
        CNet* multiplyAndMarginalize(CNet& x_dist, vector<pair<int, int> > evids);
        // String Representation
        string str(bool verbose=false, int spacing=3);
        ~MixCDNet();
};

/*************************
MIXTURE OF CDNets: TYPE 1
**************************/
class MixCDNet1 : public MixCDNet {
    public:
        MixCDNet1(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                    int maxiter, int max_depth, int clt_threshold, int limit);
        MixCDNet1(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                    int max_cond_depth, int max_depth, int clt_threshold, int maxiter=100, int limit=0);
        void learn(Dataset &data, vector<Variable *> &existing_vars, vector<int> &x_indices,
                   int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) override;
};

/*************************
MIXTURE OF CDNets: TYPE 2
**************************/
class MixCDNet2 : public MixCDNet {
public:
    MixCDNet2(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                int max_depth, int clt_threshold, int maxiter, int limit);
    MixCDNet2(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
              int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit);
    void learn(Dataset &data, vector<Variable *> &existing_vars, vector<int> &x_indices,
               int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) override;
};

/*************************
FUNCTIONS
**************************/
void decNodeProb(DecisionNode* curr_node, Dataset* data, vector<int>& x_indices,
                    vector<pair<int,string> >& curr_filter, vector<double>& weights,
                    vector<double>& weight_sums, double& mix_prob);

#endif
