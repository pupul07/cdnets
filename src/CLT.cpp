#include "CLT.h"

using namespace std;

/***************************
Constructors and Destructors
****************************/
CLT::CLT(Dataset& data, bool smooth) {
  _import_vars = false;
  vector<Variable*> existing_vars(data.size());
  for(int c=0; c<data.size(); c++) {
    Domain* dom_c = data[c].toDomain(c, "d"+to_string(c));
    existing_vars[c] = new Variable(*dom_c, c, "v"+to_string(c));
  }
  learn(data, existing_vars, smooth);
}

CLT::CLT(Dataset &data, vector<Variable*>& existing_vars, bool smooth) {
  _import_vars = true;
  learn(data, existing_vars, smooth);
}

CLT::CLT(CLT& clt) {
    _import_vars = true;
    doms() = clt.doms();
    vars() = clt.vars();
    cpts() = clt.cpts();
    for(auto & cpt : cpts()) {
        cpt = new CPT(*cpt);
    }
}

CLT::~CLT() {
    // Deallocate variables if they haven't been imported
    if (!_import_vars)
        deleteVec(vars());
    // Deallocate CPTs
    deleteVec(cpts());
}

/*************************
Access
**************************/
int CLT::size() {
  return cpts().size();
}

vector<Domain>& CLT::doms() {
  return _doms;
}

vector<Variable*>& CLT::vars() {
  return _vars;
}

vector<CPT*>& CLT::cpts() {
  return _cpts;
}

/*************************
Learning
**************************/
// Function to learn the structure and parameters of a Chow-Liu tree
void CLT::learn(Dataset &data, vector<Variable*>& existing_vars, bool smooth) {
    // If number of columns and number of variables don't match, exit
    if (data.size() != existing_vars.size())
        return;
    // First, convert all the columns into variables
    vars() = existing_vars;
    // Next, check if the dataset has only one variable
    if (data.size()==1) {
        CPT* cpt = discreteColsToCPT(vars(), data, vector<int>({0}), vector<int>({0}), smooth);
        cpts().push_back(cpt);
        return;
    }
    // Otherwise, compute the spanning tree
    vector < vector<double> > mi_mat = data.MIMatrix();
    set< pair<int,int> > max_span_tree = maxSpanTree(mi_mat);
    // Then, perform topological sorting
    vector< pair<int,int> >sorted_edges = dfs_sort(max_span_tree);
    // Finally, learn parameters in topological order
    set<int> visited;
    for(auto& edge: sorted_edges) {
        int i = edge.first, j = edge.second;
        // Learn CPT for Pr(i)
        if (visited.find(i)==visited.end()) {
            visited.insert(i);
            CPT* cpt = discreteColsToCPT(vars(), data, vector<int>({i}), vector<int>({i}), smooth);
            cpts().push_back(cpt);
        }
        // Learn CPT for Pr(j) or Pr(j|i)
        if (visited.find(j)==visited.end()) {
            visited.insert(j);
            CPT* cpt = nullptr;
            if (mi_mat[i][j]<0.05)
                cpt = discreteColsToCPT(vars(), data, vector<int>({j}), vector<int>({j}), smooth);
            else
                cpt = discreteColsToCPT(vars(), data, vector<int>({i,j}), vector<int>({j}), smooth);
            cpts().push_back(cpt);
        }
    }
}

/*************************
Inference
**************************/
// Assume all variables are assigned
double CLT::quickLL() {
    if (is_unset_vars(vars()).size() != 0)
        return 1.0;
    return log(quickProbEvid());
}

// Calculate Log-Likelihood of a single instantiation
double CLT::LL(vector<int>& vals) {
    if (vars().size()!=vals.size())
        return 1.0;
    // Create evidence tuple
    vector<int> var_indices(vars().size());
    iota(var_indices.begin(), var_indices.end(), 0);
    vector<pair<int,int> > evid = zip(var_indices, vals);
    // Return log of probability of evidence
    return log(probEvid(evid));
}

// Calculate average Log-Likelihood of a dataset
double CLT::avgLL(Dataset &data) {
    if (data.nrows()==0 || data.size() != vars().size())
        return 1.0;
    // Initialize variables
    double total_ll = 0.0;
    vector<int> var_indices(vars().size());
    iota(var_indices.begin(), var_indices.end(), 0);
    // Calculate LL for each data point
    for(int d=0; d<data.nrows(); d++) {
        vector<string> row = data.row(d);
        vector<int> indices = extract_var_val_indices(row, vars());
        // If invalid indices, then return invalid LL score
        if (indices.size()==0)
            return 1.0;
        // Invoke probability of evidence function
        vector<pair<int,int> > evid = zip(var_indices, indices);
        double curr_ll = log(probEvid(evid));
        total_ll += curr_ll;
    }
    return total_ll/data.nrows();
}

// Quickly compute Pr(e) assuming variables have been assigned
double CLT::quickProbEvid(bool debug) {
    // 0. Return 1.0 if evidence is empty
    if (is_set_vars(vars()).empty())
        return 1.0;
    // 1. Create dummy variables for evidence
    vector<Variable*> new_vars(vars().begin(), vars().end());
    vector<Variable*> new_parents(new_vars.size(), nullptr);
    vector<Variable*> new_vars_only;
    Domain evid_domain(vector<string>({"e"}), 0, "evidence");
    Variable* curr_parent = nullptr;
    for(int c=0; c<cpts().size(); c++) {
        Variable* var = cpts()[c]->marg_scope()[0];
        Variable* parent = (cpts()[c]->cond_scope().size()>0) ? cpts()[c]->cond_scope()[0] : nullptr;
        if (var->isSet()) {
            new_vars[c] = new Variable(evid_domain, var->id(), var->name());
            new_vars_only.push_back(new_vars[c]);
        }
        if (!parent)
            continue;
        if (!curr_parent || curr_parent->id()!=parent->id())
            curr_parent = new_vars[c-1];
        new_parents[c] = curr_parent;
    }
    // 2. Generate new CPTs by removing evidence
    if (debug) cout << "[DEBUG] Creating new CPTs" << endl;
    vector<CPT*> new_cpts(cpts().begin(), cpts().end());
    vector<CPT*> new_cpts_only;
    for(int c=0; c<cpts().size(); c++) {
        vector<Variable*> dummy_vars;
        Variable* var = cpts()[c]->marg_scope()[0];
        Variable* parent = (cpts()[c]->cond_scope().size()>0) ? cpts()[c]->cond_scope()[0] : nullptr;
        if (var->isSet())
            dummy_vars.push_back(new_vars[c]);
        if (parent && parent->isSet())
            dummy_vars.push_back(new_parents[c]);
        new_cpts[c] = cpts()[c]->removeEvidence(dummy_vars);
        if (cpts()[c]!=new_cpts[c]) new_cpts_only.push_back(new_cpts[c]);
        if (debug) cout << "new_cpts[" << c << "]: " << new_cpts[c]->str(true) << endl;
    }
    // 3. Perform variable elimination in reverse-topological order
    Variable* last_var = new_cpts.back()->marg_scope()[0];
    vector<Variable*> curr_marg_scope;
    CPT* msg = new CPT(vector<Variable*>({last_var}), vector<Variable*>());
    CPT* prev_msg = msg;
    bool isLastVarSet = cpts().back()->marg_scope()[0]->isSet();
    bool isFirstVarSet = cpts()[0]->marg_scope()[0]->isSet();
    if (debug) cout << "[DEBUG] Starting Variable Elimination" << endl;
    // Start elimination
    for(int c=vars().size()-1; c>=0; c--) {
        Variable* var_to_elim = new_cpts[c]->marg_scope()[0];
        // If evidence variable, then append to marginal scope and do not eliminate
        if (cpts()[c]->marg_scope()[0]->isSet() || curr_marg_scope.size()==0)
            curr_marg_scope.push_back(var_to_elim);
        else if (c>0 && !isLastVarSet)
            curr_marg_scope[0] = var_to_elim;
        // We have reached the root node, so no more eliminations left
        if(c==0 && msg->table().size()>1 && curr_marg_scope.size()>1)
            curr_marg_scope.erase(curr_marg_scope.begin());
        msg = CPT::multiplyAndMarginalize(*prev_msg, *new_cpts[c], curr_marg_scope, false);
        if (debug) cout << "msg: " << msg->str(true) << endl;
        delete prev_msg;
        prev_msg = msg;
    }
    if (debug) cout << "[DEBUG] Finished Variable Elimination" << endl;
    // Store probability of evidence
    double prob = msg->table()[0];
    // Deallocate dynamic memory
    deleteVec(new_vars_only);
    deleteVec(new_cpts_only);
    delete prev_msg;
    // Return probability of evidence
    return prob;
}

// Compute Pr(e) where evidence is provided as a vector
double CLT::probEvid(vector< pair<int,int> > evidence, bool debug) {
  // If evidence is empty, return 1.0
  if (evidence.empty())
    return 1.0;
  // Set evidence
  unset_all(vars());
  for(int e=0; e<evidence.size(); e++) {
    int v = evidence[e].first;
    int val = evidence[e].second;
    if (v>=vars().size() || val >= vars()[v]->domain().size()) {
      unset_all(vars());
      return -1.0;
    }
    vars()[v]->val() = val;
  }
  // Compute Pr(e)
  double prob = quickProbEvid(debug);
  unset_all(vars());
  return prob;
}

// Computer Pr(m|e) by computing Pr(m,e) / Pr(e) {
double CLT::margEvid(vector< pair<int,int> > marg_evidence,
                      vector< pair<int,int> > evidence, bool debug) {
  // Check if indices are valid
  vector< pair<int,int> > all_evidence = vector_union(marg_evidence, evidence);
  // Calculate Pr(m,e)
  if (debug) cout << "[DEBUG] Computing Pr(m,e)" << endl;
  double total_prob = probEvid(all_evidence, debug);
  if (total_prob<=0.0)
    return total_prob;
  // Calculate Pr(m|e)
  if (debug) cout << "[DEBUG] Computing Pr(m|e)" << endl;
  return total_prob / probEvid(evidence, debug);
  return 0.0;
}

// Compute MPE using max-product algorithm
double CLT::quickMPE(bool debug) {
  // 1. Create dummy variables for evidence
  unordered_map<Variable*,Variable*> evid_map = create_dummy_map(vars());
  // 2. Generate new CPTs by removing evidence
  if (debug) cout << "[DEBUG] Creating new CPTs" << endl;
  vector<CPT*> new_cpts(cpts().begin(), cpts().end());
  vector<CPT*> new_cpts_only;
  for(int c=0; c<cpts().size(); c++) {
    Variable* var = cpts()[c]->marg_scope()[0];
    Variable* parent = (cpts()[c]->cond_scope().size()>0) ? cpts()[c]->cond_scope()[0] : nullptr;
    vector<Variable*> dummy_vars;
    if (var->isSet())
      dummy_vars.push_back(evid_map[var]);
    if (parent && parent->isSet())
      dummy_vars.push_back(evid_map[parent]);
    new_cpts[c] = cpts()[c]->removeEvidence(dummy_vars);
    if (cpts()[c]!=new_cpts[c]) new_cpts_only.push_back(new_cpts[c]);
    if (debug) cout << "new_cpts[" << c << "]: " << new_cpts[c]->str(true) << endl;
  }
  // 3. Compute, store and propagate max messages from the leaves to the root
  vector<Variable*> curr_marg_scope({new_cpts.back()->marg_scope()[0]});
  vector<CPT*> max_msgs(new_cpts.begin(), new_cpts.end());
  Variable* last_var = new_cpts.back()->marg_scope()[0];
  CPT* curr_msg = new CPT(vector<Variable*>({last_var}), vector<Variable*>());
  if (debug) cout << "[DEBUG] 1st propagation" << endl;
  for(int c=vars().size()-1; c>=0; c--) {
    Variable* var_to_elim = new_cpts[c]->marg_scope()[0];
    // Eliminate current marginal variable
    curr_marg_scope[0] = var_to_elim;
    // Generate current message
    curr_msg = CPT::multiplyAndMarginalize(*curr_msg, *new_cpts[c], curr_marg_scope, false, true);
    // Store message in array
    max_msgs[c] = curr_msg;
    if (debug) cout << "curr_msg: " << curr_msg->str(true) << endl;
  }
  // 4. Propagate max messages from the root to the leaves and calculate MPE
  if (debug) cout << "[DEBUG] 2nd propagation" << endl;
  double mpe_prob = *max_element(max_msgs[0]->table().begin(), max_msgs[0]->table().end());
  CPT* prev_msg = curr_msg;
  for(int c=0; c<max_msgs.size(); c++) {
    // 4a. Remove parent evidence from max_msgs[c]
    vector<Variable*> dummy_vars(max_msgs[c]->cond_scope().size());
    for(int p=0; p<max_msgs[c]->cond_scope().size(); p++) {
      Variable* parent = max_msgs[c]->cond_scope()[p];
      Variable* parent_evid = (evid_map.find(parent)!=evid_map.end()) ? evid_map[parent] : parent;
      parent_evid->val() = (!parent_evid->isSet()) ? 0 : parent_evid->val();
      dummy_vars[p] = parent_evid;
    }
    CPT* prev_max_msg = max_msgs[c];
    max_msgs[c] = max_msgs[c]->removeEvidence(dummy_vars);
    prev_max_msg = (prev_max_msg==max_msgs[c]) ? nullptr : prev_max_msg;
    // if (debug) cout << "max_msgs[c]: " << max_msgs[c]->str(true) << endl;
    // 4b. Calculate current message
    if (c==0)
      curr_msg = new CPT(*max_msgs[c]);
    else
        curr_msg = CPT::multiplyAndMarginalize(*max_msgs[c], *prev_msg, vector<Variable *>(), false, true);
    // 4c. Find the max value given evidence
    Variable* var= new_cpts[c]->marg_scope()[0];
    if (!var->isSet())
      var->val() = argmax(curr_msg->table());
    // 4d. Remove evidence from curr_msg
    Variable* var_evid = (evid_map.find(var)!=evid_map.end()) ? evid_map[var] : var;
    dummy_vars = vector<Variable*>({var_evid});
    CPT* temp_msg = curr_msg;
    curr_msg = curr_msg->removeEvidence(dummy_vars);
    if (debug) cout << "curr_msg: " << curr_msg->str(true) << endl;
    // 4e. Garbage collection
    if (prev_msg != max_msgs[c]) delete max_msgs[c];
    if (temp_msg != curr_msg) delete temp_msg;
    delete prev_msg;
    delete prev_max_msg;
    prev_msg = curr_msg;
  }
  // 5. Return mpe_prob
  delete prev_msg;
  deleteUMapVal(evid_map);
  deleteVec(new_cpts_only);
  // deleteVec(max_msgs);
  return mpe_prob;
}

// Return MPE tuple after assigning evidence and computing MPE
pair<double,vector<int> > CLT::MPE(vector< pair<int,int> > evidence, bool debug) {
  vector<int> mpe_tuple(vars().size(), -1);
  // Set evidence
  unset_all(vars());
  for(int e=0; e<evidence.size(); e++) {
    int v = evidence[e].first;
    int val = evidence[e].second;
    if (v>=vars().size() || val >= vars()[v]->domain().size()) {
      unset_all(vars());
      return pair<double,vector<int> >(-1.0, mpe_tuple);
    }
    vars()[v]->val() = val;
  }
  // Call quickMPE()
  double mpe_prob = quickMPE(debug);
  // Collate results
  for(int v=0; v<vars().size(); v++) {
    mpe_tuple[v] = vars()[v]->val();
  }
  // Return MPE tuple
  unset_all(vars());
  return pair<double,vector<int> >(mpe_prob, mpe_tuple);
}

/*************************
Sampling
**************************/
void CLT::quickSample(bool debug) {
  // Create dummy domain and variable
  Domain dom_dummy(vector<string>({"e"}), -1, "evidence");
  Variable var_dummy(dom_dummy, -1, "vevid");
  // Traverse in topological order and reduce evidence
  for(int c=0; c<cpts().size(); c++) {
    Variable* cpt_var = cpts()[c]->marg_scope()[0];
    // If evidence is already set, then do nothing
    if (cpt_var->isSet())
      continue;
    // Otherwise, remove evidence and sample
    vector<Variable*> dummy_vars({&var_dummy});
    CPT* dist_cpt = cpts()[c]->removeEvidence(dummy_vars);
    vector<double> distribution = dist_cpt->table();
    cpt_var->val() = sampleFromDist(distribution);
    delete dist_cpt;
  }
}

pair <double, vector<int> > CLT::sample(vector< pair<int,int> >& evidence,
                                        bool debug) {
  vector<int> sample_vec(vars().size(), -1);
  // Set the evidence
  unset_all(vars());
  for(int e=0; e<evidence.size(); e++) {
    int v = evidence[e].first;
    int val = evidence[e].second;
    if (v>=vars().size() || val >= vars()[v]->domain().size()) {
      unset_all(vars());
      return pair <double, vector<int> >({-1.0, sample_vec});
    }
    vars()[v]->val() = val;
  }
  // Make a list of all non-evidence variables too
  vector<pair<int,int> > non_evidence(vars().size()-evidence.size());
  int ne = 0;
  for(int v=0; v<vars().size(); v++) {
    if (!vars()[v]->isSet())
      non_evidence[ne++] = pair<int,int>({v,-1});
  }
  // Invoke quickSample()
  quickSample(debug);
  // Fill in values of sample into sample_vec
  for(int v=0; v<vars().size(); v++) {
    sample_vec[v] = vars()[v]->val();
  }
  // Calculate weight
  double weight;
  if (evidence.size()==vars().size())
    weight = 1.0;
  else {
    for(int ne=0; ne<non_evidence.size(); ne++) {
      int v = non_evidence[ne].first;
      non_evidence[ne].second = vars()[v]->val();
    }
    weight = margEvid(evidence, non_evidence);
  }
  // Return sample
  unset_all(vars());
  return pair <double, vector<int> > ({weight, sample_vec});
}

/*************************
Misc Functions
**************************/
void CLT::substituteVars(unordered_map<Variable*,Variable*>& var_map) {
    // Substitute variables in vars()
    for(auto & var : vars()) {
        if (var_map.find(var) != var_map.end())
            var = var_map[var];
    }
    // Substitute variables in cpts()
    for(auto & cpt : cpts()) {
        // Marginal Scope
        for(auto & var : cpt->marg_scope()) {
            if (var_map.find(var) != var_map.end())
                var = var_map[var];
        }
        // Conditional Scope
        for(auto & var : cpt->cond_scope()) {
            if (var_map.find(var) != var_map.end())
                var = var_map[var];
        }
        // Full Scope
        for(auto & var : cpt->scope()) {
            if (var_map.find(var) != var_map.end())
                var = var_map[var];
        }
    }
}

/*************************
String Representation
**************************/
// String representation of ChowLiu Tree
string CLT::str(bool verbose) {
  string s = "ChowLiuTree { ";
  string delim = verbose ? "\n" : ", ";
  for(int c=0; c<cpts().size(); c++) {
    s += cpts()[c]->str(verbose) + delim;
  }
  return s.substr(0, s.size()-2)+" }";
}
