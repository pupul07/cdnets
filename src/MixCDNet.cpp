#include "MixCDNet.h"

/*************************
MIXTURE OF CDNets
**************************/
/*************************
MixCDNet: Constructors
**************************/
MixCDNet::~MixCDNet() {
    if(!import_vars()) {
        deleteVec(doms());
        deleteVec(vars());
    }
    deleteVec(cdnets());
}

/*************************
MixCDNet: Access
**************************/
vector<Domain*>& MixCDNet::doms() {
    return _doms;
}

vector<Variable*>& MixCDNet::vars() {
    return _vars;
}

vector<Variable*>& MixCDNet::y_vars() {
    return _y_vars;
}

vector<Variable*>& MixCDNet::x_vars() {
    return _x_vars;
}

vector<CDNet*>& MixCDNet::cdnets() {
    return _cdnets;
}

vector<double>& MixCDNet::probs() {
    return _probs;
}

int MixCDNet::nleaves() {
    return _nleaves;
}

bool& MixCDNet::import_vars() {
    return _import_vars;
}

/*************************
MixCDNet: Learning
**************************/
void MixCDNet::learn(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                        int maxiter, int max_depth, int clt_threshold, int limit) {
    // Create variables
    import_vars() = false;
    doms() = vector<Domain*>(data.size());
    vars() = vector<Variable*>(data.size());
    for(int v=0; v<vars().size(); v++) {
        doms()[v] = data[v].toDomain(v, "d"+to_string(v));
        vars()[v] = new Variable(*doms()[v], v, "v"+to_string(v));
    }
    // Call learn with existing variables
    learn(data, vars(), x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, limit);
}

/*************************
MixCDNet: Inference
**************************/
double MixCDNet::avgLL(Dataset& data) {
    if (!data.nrows())
        return 1.0;
    double total_ll = 0.0;
    for(int i=0; i<data.nrows(); i++) {
        double prob_i = 0.0;
        for (int c = 0; c < cdnets().size(); c++) {
            prob_i += probs()[c]*exp(cdnets()[c]->LL(data.row(i)));
        }
        total_ll += log(prob_i);
    }
    return total_ll / data.nrows();
}

double MixCDNet::probEvid(vector<pair<int, int> >& evids) {
    // Set evidence
    unset_all(vars());
    set_evidence(vars(), move(evids));
    if (!is_unset_vars(x_vars()).empty())
        return -1.0;
    // Call probEvid for each root node
    double prob = 0.0;
    for(int c=0; c<cdnets().size(); c++) {
        if (!cdnets()[c]->root())
            return -1.0;
        prob += probs()[c] * cdnets()[c]->root()->quickProbEvid();
    }
    return prob;
}

CNet* MixCDNet::quickMultiplyAndMarginalize(CNet& x_dist) {
    // First, check if all x_dist variables are contained inside x_vars
    if (!vector_difference(x_vars(), x_dist.vars()).empty())
        return nullptr;
    // Initialize mixture CNet variables
    vector<string> mixture_comps = domRange(0, nleaves()-1);
    Domain* latent_dom = new Domain(mixture_comps, -1, "dsum");
    Variable* latent_var = new Variable(*latent_dom, -1, "vsum");
    Node* new_root = new Node(latent_var);
    new_root->probs = vector<double>(nleaves());
    new_root->children = vector<Node*>(nleaves());
    // Call multiply and marginalize for each cdnet
    int count = 0;
    for(int c=0; c<cdnets().size(); c++) {
        CDNet* cdnet = cdnets()[c];
        CNet* mixture = cdnet->quickMultiplyAndMarginalize(x_dist);
        for(int l=0; l<cdnet->nleaves(); l++) {
            new_root->probs[count+l] = mixture->root()->probs[l] * _probs[c];
            new_root->children[count+l] = new Node(*mixture->root()->children[l]);
        }
        count += cdnet->nleaves();
        delete mixture;
    }
    // Create cutset network and return
    CNet* cnet = new CNet(new_root);
    cnet->vars() = vector<Variable*>(y_vars().size()+1);
    cnet->doms() = vector<Domain*>(y_vars().size()+1);
    for(int v=0; v<cnet->vars().size()-1; v++) {
        cnet->vars()[v] = y_vars()[v];
        cnet->doms()[v] = &cnet->vars()[v]->domain();
    }
    cnet->doms()[y_vars().size()] = latent_dom;
    cnet->vars()[y_vars().size()] = latent_var;
    return cnet;
}

CNet* MixCDNet::multiplyAndMarginalize(CNet& x_dist, vector<pair<int, int> > evids) {
    // Set evidence
    set_evidence(x_vars(), evids);
    // Call quickMultiplyAndMarginalize()
    return quickMultiplyAndMarginalize(x_dist);
}

/******************************
MixCDNet: String Representation
*******************************/
string MixCDNet::str(bool verbose, int spacing) {
    string str = "";
    for(int c=0; c<cdnets().size(); c++) {
        if(!cdnets()[c]->root())
            continue;
        str += cdnets()[c]->root()->str(0, spacing, to_string(probs()[c], 2) ,verbose) + "\n";
    }
    return str;
}

/*************************
MIXTURE OF CDNets: TYPE 1
**************************/
/*************************
MixCDNet1: Constructors
**************************/
MixCDNet1::MixCDNet1(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                        int max_depth, int clt_threshold, int maxiter, int limit) {
    MixCDNet::learn(data, x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, limit);
}

MixCDNet1::MixCDNet1(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                     int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) {
    learn(data, existing_vars, x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, limit);
}

/*************************
MixCDNet1: Learning
**************************/
void MixCDNet1::learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                      int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) {
    // 0. Get vars()
    import_vars() = true;
    doms() = vector<Domain *>(existing_vars.size());
    vars() = existing_vars;
    // 1. Segregate into x and y
    vector<int> x_indices_final;
    for (int c = 0; c < data.size(); c++) {
        // Assign domain from existing_vars
        doms()[c] = &vars()[c]->domain();
        // Add to x_vars if evidence, otherwise add to y_vars
        if (find(x_indices.begin(), x_indices.end(), c) != x_indices.end()) {
            x_vars().push_back(vars()[c]);
            x_indices_final.push_back(c);
        } else
            y_vars().push_back(vars()[c]);
    }
    // Generate meta_indices
    vector<int> x_meta_indices(x_vars().size());
    iota(x_meta_indices.begin(), x_meta_indices.end(), 0);
    x_meta_indices = randomSubset(x_meta_indices, limit);
    // 2. Perform Expectation Maximization to learn mixture model
    vector<double> old_probs;
    _probs = vector<double>(x_meta_indices.size(), 1.0);
    // _probs = vector<double>(randomWeightVec(x_vars().size(), 0.0, 1.0,true));
    // vector<vector<double> > all_weights(x_vars().size(), vector<double>(data.nrows(), 1.0));
    vector<vector<double> > all_weights(randomWeightVecs(x_meta_indices.size(), data.nrows(), 1.0, 1000.0));
    double old_ll = -9999.0;
    for (int i = 0; i < maxiter; i++) {
        cout << "EM Iteration " << (i+1) << endl;
        // 2a. Learn CDNet for each x variable
        old_probs = _probs;
        _probs = vector<double>(x_meta_indices.size(), 0.0);
        deleteVec(_cdnets);
        _cdnets = vector<CDNet *>(x_meta_indices.size(), nullptr);
        _nleaves = 0;
        vector<double> weight_sums(data.nrows(), 0.0);
        for (int k = 0; k < x_meta_indices.size(); k++) {
            int x = x_meta_indices[k];
            Variable *x_var = x_vars()[x];
            int size = x_var->domain().size();
            vector<DecisionNode *> children(size);
            int cdnet_nleaves = 0;
            // Learn Cutset Network for each child
            data.weights() = all_weights[k];
            for (int c = 0; c < children.size(); c++) {
                // Create list of new indices
                vector<int> x_indices_new = x_indices_final;
                evidencePop1d(x_indices_new, x_indices_final[x]);
                vector<Variable *> new_vars = vector_difference(vars(), vector<Variable *>({x_var}));
                // First, learn CDNet
                vector<pair<int, string> > filter = zip(vector<int>({x_indices_final[x]}),
                                                        vector<string>({x_var->domain()[c]}));
                Dataset *filtered_data = data.filterByValues(filter);
                Dataset *new_data = filtered_data->dropCols(set<int>({x_indices_final[x]}));
                CDNet *cdnet = new CDNet(*new_data, new_vars, x_indices_new, max_cond_depth-1,
                                         max_depth, clt_threshold);
                children[c] = new DecisionNode(*cdnet->root());
                cdnet_nleaves += cdnet->nleaves();
                // Finally, do some garbage collection
                delete cdnet;
                delete filtered_data;
                delete new_data;
            }
            // Add decision node to mixture
            DecisionNode *new_root = new DecisionNode(x_var, children, nullptr);
            _cdnets[k] = new CDNet(doms(), vars(), y_vars(), x_vars(), new_root, cdnet_nleaves);
            DecisionNode *curr_node = _cdnets[k]->root();
            // Generate weights for calculating mixture probabilities
            vector<pair<int, string> > curr_filter;
            decNodeProb(curr_node, &data, x_indices_final, curr_filter, all_weights[k], weight_sums, old_probs[k]);
            // Update nleaves
            _nleaves += cdnet_nleaves;
        }
        // 2b. Learn mixture probabilities
        for (int k = 0; k < x_meta_indices.size(); k++) {
            for(int i=0; i<data.nrows(); i++) {
                all_weights[k][i] = (weight_sums[k]) ? all_weights[k][i] / weight_sums[i] : all_weights[k][i];
                _probs[k] += all_weights[k][i];
            }
            _probs[k] /= (!data.nrows()) ? 1.0 : data.nrows();
        }
        // 2c. Check log-likelihood
        double ll = avgLL(data);
        cout << "Log Likelihood: " << ll << endl;
        // 2d. Check for convergence
        if (abs(ll-old_ll)<0.001)
            break;
        old_ll = ll;
    }
}

/*************************
MIXTURE OF CDNets: TYPE 2
**************************/
/*************************
MixCDNet2: Constructors
**************************/
MixCDNet2::MixCDNet2(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                        int max_depth, int clt_threshold, int maxiter, int limit) {
    MixCDNet::learn(data, x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, limit);
}

MixCDNet2::MixCDNet2(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                     int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) {
    learn(data, existing_vars, x_indices, max_cond_depth, max_depth, clt_threshold, maxiter, limit);
}

/*************************
MixCDNet2: Learning
**************************/
void MixCDNet2::learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                      int max_cond_depth, int max_depth, int clt_threshold, int maxiter, int limit) {
    // 0. Get vars()
    import_vars() = true;
    doms() = vector<Domain*>(existing_vars.size());
    vars() = existing_vars;
    // 1. Segregate into x and y
    vector<int> x_indices_final;
    for(int c=0; c<data.size(); c++) {
        // Assign domain from existing_vars
        doms()[c] = &vars()[c]->domain();
        // Add to x_vars if evidence, otherwise add to y_vars
        if(find(x_indices.begin(), x_indices.end(), c) != x_indices.end()) {
            x_vars().push_back(vars()[c]);
            x_indices_final.push_back(c);
        }
        else
            y_vars().push_back(vars()[c]);
    }
    // 2. Calculate MI matrix
    vector< vector<double> > mi = data.MIMatrix();
    unordered_set<int> leftover_x(x_indices_final.begin(), x_indices_final.end());
    // 3. Learn CDNet for each (x1,x2) pair of variables
    _probs = vector<double>((x_vars().size()+1)/2, 0.0);
    _cdnets = vector<CDNet*>((x_vars().size()+1)/2, nullptr);
    _nleaves = 0;
    double Z = 0.0;
    int m = 0;
    while (!leftover_x.empty()) {
        // Pop first index x1
        int x1 = *leftover_x.begin();
        leftover_x.erase(x1);
        // Search for most correlated index max_x2
        double max_mi = 0.0;
        int max_x2 = *leftover_x.begin();
        for (auto const& x2 : leftover_x) {
            if(mi[x1][x2]>max_mi) {
                max_x2 = x2;
                max_mi = mi[x1][x2];
            }
        }
        leftover_x.erase(max_x2);
        // Learn CDNet over these two indices
        Variable* x1_var = vars()[x1];
        Variable* x2_var = vars()[max_x2];
        int size1 = x1_var->domain().size();
        int size2 = x2_var->domain().size();
        vector<DecisionNode*> children1(size1);
        vector<DecisionNode*> children2(size2);
        // Learn Decision Node over each x2 for given x1
        for(int c1=0; c1<children1.size(); c1++) {
            // Learn cutset network for every combination of x1 and x2
            for(int c2=0; c2<children2.size(); c2++) {
                // First, learn CNet
                vector <pair<int,string> > filter = zip(vector<int>({x1, max_x2}),
                                                        vector<string>({x1_var->domain()[c1],
                                                                        x2_var->domain()[c2]}));
                Dataset* filtered_data = data.filterByValues(filter);
                Dataset* new_data = filtered_data->dropCols(set<int>(x_indices.begin(), x_indices.end()));
                CNet* cnet = new CNet(*new_data, y_vars(), max_depth, clt_threshold);;
                children2[c2] = new DecisionNode(nullptr, vector<DecisionNode*>(), cnet);
                // Then, learn mixture probability
                double prob = 0.0;
                for(int i=0; i<new_data->nrows(); i++) {
                    prob += exp(cnet->LL(new_data->row(i)));
                }
                _probs[m] += (prob * new_data->nrows()) / data.nrows();
                // Finally, do some garbage collection
                delete filtered_data;
                delete new_data;
            }
            // Create new decision node for x2
            children1[c1] = new DecisionNode(x2_var, children2, nullptr);
        }
        // Add decision node to CDNet
        DecisionNode* new_root = new DecisionNode(x1_var, children1, nullptr);
        _cdnets[m] = new CDNet(doms(), vars(), y_vars(), x_vars(), new_root, size1*size2);
        // Increment number of leaves
        _nleaves += size1*size2;
        // Add to partition function
        Z += _probs[m++];
    }
    // 4. Normalize mixture probabilities
    if(Z>0.0) {
        for(double & prob : probs()) {
            prob /= Z;
        }
    }
}

void decNodeProb(DecisionNode* curr_node, Dataset* data, vector<int>& x_indices,
                    vector<pair<int,string> >& curr_filter, vector<double>& weights,
                    vector<double>& weight_sums, double& mix_prob) {
    // For cutset network, filter the data and return probability score
    if (curr_node->isCNet()) {
        Dataset* new_data = data->dropCols(set<int>(x_indices.begin(), x_indices.end()));
        vector<int> indices(data->filterByValuesOnly(curr_filter));
        for(auto const & i: indices) {
            double local_prob = mix_prob * exp(curr_node->data->LL(new_data->row(i)));
            weights[i] = local_prob;
            weight_sums[i] += local_prob;
        }
        delete new_data;
    }
    for(int c=0; c<curr_node->children.size(); c++) {
        vector<pair<int,string> > new_filter = zip(vector<int>({curr_node->x_var->id()}),
                                                    vector<string>({curr_node->x_var->domain()[c]}));
        curr_filter.insert(curr_filter.end(), new_filter.begin(), new_filter.end());
        decNodeProb(curr_node->children[c], data, x_indices, curr_filter, weights, weight_sums, mix_prob);
        curr_filter.erase(curr_filter.end()-1);
    }
}
