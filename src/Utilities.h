#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <random>
#include <algorithm>
#include "Utilities.h"

using namespace std;

/***********************
Global Objects
***********************/
extern default_random_engine generator;

/***********************
Non-Template Utilities
***********************/
const string to_string(string);
const string to_string(double, int);
const string duplicate(string, int);
vector<string> split(string s, string delim=",", bool to_trim=true);
vector< pair<int,int> > dfs_sort(set< pair<int,int> > graph);
string trim(const string& str,
            const string& whitespace = " \t");
pair<int,int> evidencePop(vector < pair<int,int> >& evidence, int index);
void evidencePop1d(vector<int>& evidence, int index);
void setSeed(unsigned int seed = time(NULL)*1000);
int sampleFromDist(vector<double>& probs);
set<int> setRange(int start=1, int end=10);
vector<string> domRange(int start=1, int end=10);
vector<int> readSeqFromFile(string filepath, char delim=',');
vector<int> stringToVecInt(string str, string delim=",");
double sumWeights(vector<double>& weights, vector<int>& indices);
vector<double> selectWeights(vector<double>& weights, vector<int>& indices);
vector<double> randomWeightVec(int size, double lower=0.0, double upper=1.0, bool to_normalize=false);
vector< vector<double> > randomWeightVecs(int n, int size, double lower=0.0, double upper=1.0, bool to_normalize=false);
vector<int> recalc_indices(vector<int>& org_indices, vector<int>& dropped_indices, bool to_sort=true);

/***********************
Template Utilities
***********************/
// String conversion functions
template <typename T, typename S>
string to_string(pair<T,S>);
template <typename T>
string to_string(T*);
template <typename T>
string to_string_set(set<T>);
template <typename T>
string to_string_vec(vector<T>);
template <typename T>
string to_string_2dvec(vector<T>);
template <typename T, typename S>
string to_string_map(map<T,S>);
template <typename T, typename S>
string to_string_map_size(map<T,S>);
// Set Operation Wrappers on Sets
template <typename T>
set<T> quick_union(set<T>&, set<T>&);
template <typename T>
set<T> quick_intersection(set<T>&, set<T>&);
template <typename T>
set<T> quick_difference(set<T>&, set<T>&);
// Set Operation Wrappers on Vectors
template <typename T>
vector<T> vector_union(vector<T>, vector<T>);
template <typename T>
vector<T> vector_intersection(vector<T>, vector<T>);
template <typename T>
vector<T> vector_difference(vector<T>, vector<T>);
// Useful vector operations
template <typename T>
bool are_equal_vec(vector<T> v1, vector<T> v2, T precision);
template <typename T>
vector<int> index_of_vec(vector<T> vec, vector<T> subset_vec);
template <typename T>
vector<T> slice(vector<T> vec, vector<int> indices);
template <typename T, typename S>
vector < pair<T,S> > zip(vector<T> a, vector<S> b);
template <typename T, typename S>
unordered_map <T,S> zipMap(vector<T> a, vector<S> b);
template <typename T>
T vec_sum(vector<T>& vec);
template <typename T>
vector<T> select_from_vector(vector<T>& vec, vector<int>& indices);
// Misc operations
template <typename T>
int argmax(vector<T>& vec);
template <typename T>
bool are_equal(T val1, T val2, T precision);
template <typename T>
void removeIndices(vector<T>& vec, vector<int> to_remove, bool to_sort=false);
template <typename T>
vector<T> cumsum(vector<T>& vals);
template <typename T>
vector<T> addConstant(vector<T>& vals, T constant);
template <typename T>
void deleteVec(vector<T*>& vec);
template <typename T, typename S>
void deleteUMapKey(unordered_map<T*,S*>& umap);
template <typename T, typename S>
void deleteUMapVal(unordered_map<T*,S*>& umap);
template <typename T>
vector<T> randomSubset(vector<T>& vec, int limit);
template <typename T>
unordered_set<T> init_unordered_set_range(int upper, int lower=0);
template <typename T>
vector<int> sort_indices_desc(vector<T> vec);
template <typename T>
vector<T> sum_2d_vector_rows(vector< vector<T> > twod_vec);

/*
 * The section below contains the code for all
 * template utilities defined above
 */

template <typename T, typename S>
string to_string(pair<T,S> p) {
  return "(" + to_string(p.first) + ", " + to_string(p.second) + ")";
}

template <typename T>
string to_string(T* pointer) {
  ostringstream address;
  address << (void*)pointer;
  return address.str();
}

template <typename T>
string to_string_set(set<T> s) {
  string str = "";
  for(auto element: s) {
    str += to_string(element) + ", ";
  }
  return str.substr(0, str.size()-2);
}

template <typename T>
string to_string_vec(vector<T> vec) {
  string str = "";
  for(int i=0; i<vec.size(); i++) {
    str += to_string(vec[i]) + ", ";
  }
  return str.substr(0, str.size()-2);
}

template <typename T>
string to_string_2dvec(vector< vector<T> > vec) {
  string str = "";
  for(int i=0; i<vec.size(); i++) {
    for(int j=0; j<vec[i].size(); j++) {
      str += to_string(vec[i][j]) + ", ";
    }
    str = str.substr(0, str.size()-2) + "\n";
  }
  return str;
}

template <typename T, typename S>
string to_string_map(map<T,S> m) {
  string str = "";
  for(typename map<T,S>::iterator itr=m.begin(); itr!=m.end(); itr++) {
    str += to_string(itr->first) + ": " + to_string(itr->second) + ", ";
  }
  return str.substr(0, str.size()-2);
}

template <typename T, typename S>
string to_string_map_size(map<T,S> m) {
  string str = "";
  for(typename map<T,S>::iterator itr=m.begin(); itr!=m.end(); itr++) {
    str += to_string(itr->first) + ": " + to_string((itr->second).size()) + ", ";
  }
  return str.substr(0, str.size()-2);
}

template <typename T>
vector<T> vector_union(vector<T> a, vector<T> b) {
  // Return the other vector if one of them is empty
  if (a.size()==0)
    return vector<T>(b);
  if(b.size()==0)
    return vector<T>(a);
  // Otherwise, check for matches and leave them out
  vector<T> c(a);
  for(int j=0; j<b.size(); j++) {
    int i;
    for(i=0; i<a.size(); i++) {
      if (b[j] == a[i])
        break;
    }
    if (i==a.size() || b[j]!=a[i])
      c.push_back(b[j]);
  }
  return c;
}

template <typename T>
vector<T> vector_intersection(vector<T> a, vector<T> b) {
  vector<T> c({});
  // Return an empty vector if either of the two vectors is empty
  if (a.size()==0 || b.size()==0)
    return c;
  // Otherwise, check for intersection
  for(int i=0; i<a.size(); i++) {
    for(int j=0; j<b.size(); j++) {
      if (a[i] == b[j]) {
        c.push_back(a[i]);
        break;
      }
    }
  }
  return c;
}

template <typename T>
vector<T> vector_difference(vector<T> a, vector<T> b) {
  vector<T> c({});
  // Return an empty vector if a is empty
  if (a.size()==0)
    return c;
  // Return a copy of a if b is empty
  if (b.size()==0)
    return vector<T>(a);
  // Otherwise, perform vector difference
  for(int i=0; i<a.size(); i++) {
    int j;
    for(j=0; j<b.size(); j++) {
      if (a[i] == b[j]) {
        break;
      }
    }
    if (j>=b.size() || a[i] != b[j])
      c.push_back(a[i]);
  }
  return c;
}

template <typename T>
set<T> quick_union(set<T>& a, set<T>& b) {
  set<T> c;
  set_union(a.begin(),a.end(),b.begin(),b.end(),
                  inserter(c,c.begin()));
  return c;
}

template <typename T>
set<T> quick_intersection(set<T>& a, set<T>& b) {
  set<T> c;
  set_intersection(a.begin(),a.end(),b.begin(),b.end(),
                  inserter(c,c.begin()));
  return c;
}

template <typename T>
set<T> quick_difference(set<T>& a, set<T>& b) {
  set<T> c;
  set_difference(a.begin(),a.end(),b.begin(),b.end(),
                  inserter(c,c.begin()));
  return c;
}

template <typename T>
vector<int> index_of_vec(vector<T> vec, vector<T> subset_vec) {
  vector<int> indices;
  set<T> subset_vec_set(subset_vec.begin(), subset_vec.end());
  for(int i=0; i<vec.size(); i++) {
    if (subset_vec_set.find(vec[i]) != subset_vec_set.end())
      indices.push_back(i);
  }
  return indices;
}

template <typename T>
vector<T> slice(vector<T> vec, vector<int> indices) {
    vector<T> new_vec(indices.size());
    int j = 0;
    for(auto & i : indices) {
        if (i < vec.size())
            new_vec[j++] = vec[i];
    }
    if (j<indices.size())
        new_vec.erase(new_vec.begin()+j, new_vec.end());
    return new_vec;
}

template <typename T, typename S>
vector < pair<T,S> > zip(vector<T> a, vector<S> b) {
  int max_size = (a.size()>b.size()) ? a.size() : b.size();
  // Check if vectors can be broadcasted
  if (max_size==0 || max_size%a.size()!=0 || max_size%b.size()!=0)
    return vector < pair<T,S> >();
  // Zip the two vectors
  vector < pair<T,S> > zipped_vector(max_size);
  for(int k=0; k<max_size; k++) {
    int i = k%a.size(), j = k%b.size();
    pair<T,S> element({a[i], b[j]});
    zipped_vector[k] = element;
  }
  return zipped_vector;
}

template <typename T, typename S>
unordered_map <T,S> zipMap(vector<T> a, vector<S> b) {
    int max_size = (a.size()>b.size()) ? a.size() : b.size();
    // Check if vectors can be broadcasted
    if (max_size==0 || max_size%a.size()!=0 || max_size%b.size()!=0)
        return unordered_map <T,S>();
    // Zip the two vectors
    unordered_map <T,S> zipped_map(max_size);
    for(int k=0; k<max_size; k++) {
        int i = k%a.size(), j = k%b.size();
        zipped_map[a[i]] = b[j];
    }
    return zipped_map;
}

template <typename T>
T vec_sum(vector<T>& vec) {
    if (is_integral<T>::value)
        return accumulate(vec.begin(), vec.end(), 0);
    else
        return accumulate(vec.begin(), vec.end(), 0.0);
}

template <typename T>
int argmax(vector<T>& vec) {
  if (vec.size()==0)
    return -1;
  return distance(vec.begin(), max_element(vec.begin(), vec.end()));
}

template <typename T>
bool are_equal(T v1, T v2, T precision) {
  return abs(v1-v2) <= precision;
}

template <typename T>
bool are_equal_vec(vector<T> v1, vector<T> v2, T precision) {
  if (v1.size() != v2.size())
    return false;
  for (int i=0; i<v1.size(); i++) {
    if (abs(v1[i]-v2[i])>precision)
      return false;
  }
  return true;
}

template <typename T>
void removeIndices(vector<T>& vec, vector<int> to_remove, bool to_sort) {
    if (to_sort)
        sort(to_remove.begin(), to_remove.end());
    for(int i=to_remove.size()-1; i>=0; i--) {
        if (to_remove[i]<vec.size())
            vec.erase(vec.begin()+to_remove[i]);
    }
}

template <typename T>
vector<T> cumsum(vector<T>& vals) {
    vector<T> cum_vals(vals.size());
    partial_sum(vals.begin(), vals.end(), cum_vals.begin());
    return cum_vals;
}

template <typename T>
vector<T> addConstant(vector<T>& vals, T constant) {
    vector<T> new_vals(vals.begin(), vals.end());
    for(int i=0; i<vals.size(); i++) {
        new_vals[i] += constant;
    }
    return new_vals;
}

/**
 * Like delete, but for a vector of pointers
 **/
template <typename T>
void deleteVec(vector<T*>& vec) {
    for(int i=0; i<vec.size(); i++) {
        delete vec[i];
    }
}

/**
 * Like delete, but for all keys of an
 * unordered map
 **/
template <typename T, typename S>
void deleteUMapKey(unordered_map<T*,S*>& umap) {
    for(auto const & element: umap) {
        delete element.first;
    }
}

/**
 * Like delete, but for all values of an
 * unordered map
 **/
template <typename T, typename S>
void deleteUMapVal(unordered_map<T*,S*>& umap) {
    for(auto const & element: umap) {
        delete element.second;
    }
}

/**
 * Samples a random subset from vec upto
 * limit elements. If limit is equal to
 * the entire vector length, then no sampling
 * takes place
 **/
template <typename T>
vector<T> randomSubset(vector<T>& vec, int limit) {
    if(limit<1 || limit>=vec.size())
        return vec;
    vector<T> new_vec(vec.begin(), vec.end());
    shuffle(new_vec.begin(), new_vec.end(), generator);
    return vector<int>(new_vec.begin(),new_vec.begin()+limit);
}

/**
 * Initializes an unordered set over a range
 * [ lower, upper ]
 **/
template <typename T>
unordered_set<T> init_unordered_set_range(int upper, int lower) {
    if(lower>=upper)
        return unordered_set<T>();
    unordered_set<T> us(upper-lower);
    for(int i=lower; i<=upper; i++) {
        us.insert(i);
    }
    return us;
}

/**
 * Takes in a vector and returns the indices
 * of the sorted array
 *
 * Ex: [ 5, 3, 1, 2, 4 ]
 * will return [ 0, 4, 1, 3, 2 ]
 **/
template <typename T>
vector<int> sort_indices_desc(vector<T> vec) {
    vector<int> indices(vec.size());
    iota(indices.begin(),indices.end(),0.0);
    sort( indices.begin(),indices.end(), [&](T i,T j){return vec[i]>vec[j];} );
    return indices;
}

/**
 * Takes a 2D vector and finds the row-wise
 * sum totals
 *
 * Ex: [ [ 1, 2 ], [ 3, 4 ] ]
 * will return [ 3, 7 ]
 **/
template <typename T>
vector<T> sum_2d_vector_rows(vector< vector<T> > twod_vec) {
    vector<T> twod_sum(twod_vec.size());
    for(int i=0; i<twod_vec.size(); i++) {
        twod_sum[i] = accumulate(twod_vec[i].begin(), twod_vec[i].end(), 0.0);
    }
    return twod_sum;
}

/**
 * Selects all elements in a vector that correspond to
 * a set of indices
 *
 * Ex:
 * vec: [ 5, 200, 33, -20, 89, -101, 44, 63 ]
 * indices: [ 1, 3, 2, 1 ]
 * will return [ 200, -20, 33, 200 ]
 **/
template <typename T>
vector<T> select_from_vector(vector<T>& vec, vector<int>& indices) {
    vector<T> new_vec;
    for(auto const & i: indices) {
        if(i>=0 and i<=vec.size())
            new_vec.push_back(vec[i]);
    }
    return new_vec;
}

#endif
