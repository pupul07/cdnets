#ifndef CLT_H_
#define CLT_H_

#include <iostream>
#include <vector>
#include <cmath>
#include <unordered_map>
#include "Variable.h"
#include "Function.h"
#include "Dataset.h"
#include "Utilities.h"

using namespace std;

class CLT {
  protected:
    bool _import_vars;
    vector<Domain> _doms;
    vector<Variable*> _vars;
    vector<CPT*> _cpts;
  public:
    CLT(Dataset&, bool=true);
    CLT(Dataset&, vector<Variable*>&, bool=true);
    CLT(CLT&);
    int size();
    void learn(Dataset&, vector<Variable*>&, bool=true);
    vector<Domain>& doms();
    vector<Variable*>& vars();
    vector<CPT*>& cpts();
    double quickLL();
    double LL(vector<int>&);
    double avgLL(Dataset&);
    double quickProbEvid(bool=false);
    double probEvid(vector< pair<int,int> >,bool=false);
    double margEvid(vector< pair<int,int> >,vector< pair<int,int> >,bool=false);
    double quickMPE(bool=false);
    pair<double,vector<int> > MPE(vector< pair<int,int> >,bool=false);
    void quickSample(bool=false);
    pair <double, vector<int> > sample(vector< pair<int,int> >&, bool=false);
    void substituteVars(unordered_map<Variable*,Variable*>& var_map);
    string str(bool=false);
    ~CLT();
};

#endif
