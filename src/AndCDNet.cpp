#include "AndCDNet.h"

/***********************
AND CDNet: Constructors
************************/
AndCDNet::~AndCDNet() {
    if(!import_vars()) {
        deleteVec(doms());
        deleteVec(vars());
    }
    deleteVec(cdnets());
}

AndCDNet::AndCDNet(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                     int max_depth, int clt_threshold, int max_part_size) {
    _nleaves = 0;
    _import_vars = false;
    learn(data, x_indices, max_cond_depth, max_depth, clt_threshold, max_part_size);
}

AndCDNet::AndCDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                   int max_cond_depth, int max_depth, int clt_threshold, int max_part_size) {
    _nleaves = 0;
    _import_vars = true;
    learn(data, existing_vars, x_indices, max_cond_depth, max_depth, clt_threshold, max_part_size);
}

/*****************
AND CDNet: Access
******************/
vector<Domain*>& AndCDNet::doms() {
    return _doms;
}

vector<Variable*>& AndCDNet::vars() {
    return _vars;
}

vector<Variable*>& AndCDNet::y_vars() {
    return _y_vars;
}

vector<Variable*>& AndCDNet::x_vars() {
    return _x_vars;
}

vector<CDNet*>& AndCDNet::cdnets() {
    return _cdnets;
}

vector< vector<int> >& AndCDNet::y_partitions() {
    return _y_partitions;
}

int AndCDNet::nleaves() {
    return _nleaves;
}

bool& AndCDNet::import_vars() {
    return _import_vars;
}

/*******************
AND CDNet: Learning
********************/
void AndCDNet::learn(Dataset& data, vector<int>& x_indices, int max_cond_depth,
            int max_depth, int clt_threshold, int max_part_size) {
    // Create variables
    import_vars() = false;
    doms() = vector<Domain*>(data.size());
    vars() = vector<Variable*>(data.size());
    for(int v=0; v<vars().size(); v++) {
        doms()[v] = data[v].toDomain(v, "d"+to_string(v));
        vars()[v] = new Variable(*doms()[v], v, "v"+to_string(v));
    }
    // Call learn with existing variables
    learn(data, vars(), x_indices, max_cond_depth, max_depth, clt_threshold, max_part_size);
}

void AndCDNet::learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
           int max_cond_depth, int max_depth, int clt_threshold, int max_part_size) {
    // 0. Get vars()
    import_vars() = true;
    doms() = vector<Domain *>(existing_vars.size());
    vars() = existing_vars;
    // 1. Segregate into x and y
    vector<int> x_indices_final;
    vector<int> y_indices_final;
    for (int c = 0; c < data.size(); c++) {
        // Assign domain from existing_vars
        doms()[c] = &vars()[c]->domain();
        // Add to x_vars if evidence, otherwise add to y_vars
        if (find(x_indices.begin(), x_indices.end(), c) != x_indices.end()) {
            x_vars().push_back(vars()[c]);
            x_indices_final.push_back(c);
        } else {
            y_vars().push_back(vars()[c]);
            y_indices_final.push_back(c);
        }
    }
    // 2. Partition the variables
    Dataset* y_data = data.dropCols(set<int>(x_indices_final.begin(), x_indices_final.end()));
    y_partitions() = partition_vars(*y_data, max_part_size);
    delete y_data;
    // 3. Learn CDNet for each disjoint partition
    vector<int> all_idx(data.size());
    iota(all_idx.begin(), all_idx.end(), 0);
    cdnets() = vector<CDNet*>(y_partitions().size());
    for (int p=0; p<y_partitions().size(); p++) {
        // 3a. First partition the data
        vector<int> y_indices_partition = select_from_vector(y_indices_final, y_partitions()[p]);
        vector<int> y_indices_to_drop = vector_difference(y_indices_final, y_indices_partition);
        Dataset* local_data = data.dropCols(set<int>(y_indices_to_drop.begin(), y_indices_to_drop.end()));
        vector<int> x_indices_new = recalc_indices(x_indices_final, y_indices_to_drop);
        // 3b. Then learn the CDNet
        vector<int> idx_not_to_drop = vector_difference(all_idx, y_indices_to_drop);
        vector<Variable*> new_vars = select_from_vector(existing_vars, idx_not_to_drop);
        cdnets()[p] = new CDNet(*local_data, new_vars, x_indices_new, max_cond_depth,
                                max_depth, clt_threshold);
        _nleaves += cdnets()[p]->nleaves();
        // 3c. Do garbage collection
        delete local_data;
    }
}

void AndCDNet::updateParams(Dataset &data, vector<Variable*>& existing_vars, vector<int>& x_indices,
                            int max_depth, int clt_threshold, vector< vector<double> > weights) {
    if (data.size() != vars().size() || x_vars().size() != x_indices.size() || \
        (!weights.empty() && weights.size() != y_partitions().size()))
        return;
    // 1. Segregate into x and y
    vector<int> x_indices_final;
    vector<int> y_indices_final;
    for (int c = 0; c < data.size(); c++) {
        if (find(x_indices.begin(), x_indices.end(), c) != x_indices.end())
            x_indices_final.push_back(c);
        else
            y_indices_final.push_back(c);
    }
    // 2. Update params according to partitions
    vector<int> all_idx(data.size());
    iota(all_idx.begin(), all_idx.end(), 0);
    for (int p=0; p<y_partitions().size(); p++) {
        // 2a. First partition the data
        vector<int> y_indices_partition = select_from_vector(y_indices_final, y_partitions()[p]);
        vector<int> y_indices_to_drop = vector_difference(y_indices_final, y_indices_partition);
        Dataset* local_data = data.dropCols(set<int>(y_indices_to_drop.begin(), y_indices_to_drop.end()));
        vector<int> x_indices_new = recalc_indices(x_indices_final, y_indices_to_drop);
        if (!weights.empty()) local_data->weights() = weights[p];
        // 2b. Then update the CDNet parameters
        vector<int> idx_not_to_drop = vector_difference(all_idx, y_indices_to_drop);
        vector<Variable*> new_vars = select_from_vector(existing_vars, idx_not_to_drop);
        cdnets()[p]->updateParams(*local_data, new_vars, x_indices_new, max_depth, clt_threshold);
        // 2c. Do garbage collection
        delete local_data;
    }
}

/*******************
AND CDNet: Inference
********************/
vector<double> AndCDNet::comp_LL(vector<string> row) {
    // Validate
    if(row.size()!=vars().size())
        return vector<double>();
    // Extract variable indices
    vector<int> vals = extract_var_val_indices(row, vars());
    // Set evidence
    for(int v=0; v<vals.size(); v++) {
        vars()[v]->val() = vals[v];
    }
    // Use quickProbEvid for each CDNet
    vector<double> probs(cdnets().size(), 0.0);
    for(int c=0; c<cdnets().size(); c++) {
        if(!cdnets()[c]->root())
            return vector<double>();
        double curr_prob = cdnets()[c]->root()->quickProbEvid();
        if (curr_prob<0.0 || curr_prob>1.0)
            return vector<double>();
        probs[c] = log(curr_prob);
    }
    return probs;
}

double AndCDNet::LL(vector<string> row) {
    vector<double> probs = comp_LL(row);
    if(probs.size()==0)
        return 1.0;
    return accumulate(probs.begin(), probs.end(), 0.0);
}

double AndCDNet::avgLL(Dataset& data) {
    if (!data.nrows())
        return 1.0;
    double total_ll = 0.0;
    for(int i=0; i<data.nrows(); i++) {
        double curr_ll = LL(data.row(i));
        if (curr_ll==1.0)
            return 1.0;
        total_ll += curr_ll;
    }
    return total_ll / data.nrows();
}

double AndCDNet::quickProbEvid() {
    double prob = 1.0;
    for(auto & cdnet : cdnets()) {
        if (!cdnet->root())
            return -1.0;
        prob *= cdnet->root()->quickProbEvid();
    }
    return prob;
}

double AndCDNet::probEvid(vector<pair<int, int> >& evids) {
    // Set evidence
    unset_all(vars());
    set_evidence(vars(), move(evids));
    if (!is_unset_vars(x_vars()).empty())
        return -1.0;
    // Call quickProbEvid()
    return quickProbEvid();
}

/******************************
AndCDNet: String Representation
*******************************/
string AndCDNet::str(bool verbose, int spacing) {
    string str = "*\n";
    for(int c=0; c<cdnets().size(); c++) {
        if(!cdnets()[c]->root())
            continue;
        str += cdnets()[c]->root()->str(1, spacing, to_string(c) ,verbose) + "\n";
    }
    return str;
}