#ifndef ANDCDNET_H
#define ANDCDNET_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include "Dataset.h"
#include "CDNet.h"

using namespace std;

/**********
AND CDNets
***********/
class AndCDNet {
protected:
    vector<Domain*> _doms;
    vector<Variable*> _vars;
    vector<Variable*> _y_vars;
    vector<Variable*> _x_vars;
    vector<CDNet*> _cdnets;
    vector< vector<int> > _y_partitions;
    int _nleaves;
    bool _import_vars;
public:
    // Constructors
    AndCDNet() { _import_vars = false; _nleaves = 0; };
    AndCDNet(Dataset& data, vector<int>& x_indices, int max_cond_depth,
                int max_depth, int clt_threshold, int max_part_size=1);
    AndCDNet(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
             int max_cond_depth, int max_depth, int clt_threshold, int max_part_size=1);
    // Access
    vector<Domain*>& doms();
    vector<Variable*>& vars();
    vector<Variable*>& y_vars();
    vector<Variable*>& x_vars();
    vector<CDNet*>& cdnets();
    vector< vector<int> >& y_partitions();
    int nleaves();
    bool& import_vars();
    // Learning
    void learn(Dataset& data, vector<int>& x_indices, int max_cond_depth,
               int max_depth, int clt_threshold, int max_part_size=1);
    void learn(Dataset& data, vector<Variable*>& existing_vars, vector<int>& x_indices,
               int max_cond_depth, int max_depth, int clt_threshold, int max_part_size=1);
    void updateParams(Dataset &data, vector<Variable*>& existing_vars, vector<int>& x_indices, int max_depth,
                        int clt_threshold, vector< vector<double> > weights=vector< vector<double> >({}));
    // Inference
    double LL(vector<string> row);
    vector<double> comp_LL(vector<string> row);
    double avgLL(Dataset& data);
    double probEvid(vector<pair<int, int> >& evids);
    double quickProbEvid();
    // String Representation
    string str(bool verbose=false, int spacing=3);
    ~AndCDNet();
};

#endif
