#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <iostream>
#include <vector>
#include <unordered_map>
#include "Variable.h"
#include "Utilities.h"

using namespace std;

class Function {
  protected:
    vector<Variable*> _scope;
    vector<double> _table;
  public:
    Function(){};
    Function(vector<Variable*>);
    int size();
    vector<Variable*>& scope();
    vector<double>& table();
    double get(vector<int>&);
    void set(vector<int>&, double);
    void setAll(vector<double>&);
    void unsetAll();
    void setAllToZero();
    void normalize();
    Function* removeEvidence(vector<Variable*>);
    static Function* multiplyAndMarginalize(Function&, Function&,
                                            vector<Variable*> = vector<Variable*>(),
                                            bool=true, bool=false);
    string str(bool=false);
};

class CPT: public Function {
  protected:
    vector<Variable*> _marg_scope;
    vector<Variable*> _cond_scope;
  public:
    CPT(vector<Variable*>, vector<Variable*>);
    CPT(Function&, vector<Variable*>, bool=true);
    CPT(CPT&);
    vector<Variable*>& marg_scope();
    vector<Variable*>& cond_scope();
    void normalize();
    CPT* removeEvidence(vector<Variable*>);
    static CPT* multiplyAndMarginalize(CPT&, CPT&,
                                        vector<Variable*> = vector<Variable*>(),
                                        bool=true, bool=false);
    string str(bool=false);
    string toJSON(bool=false);
};

// Extra Functions
void increment_vars(vector<Variable*>&);
int var_vector_size(vector<Variable*>&);
unordered_map<Variable*,Variable*> create_dummy_map(vector<Variable*>&,bool=false);
vector<int> extract_var_vals(vector<Variable*>&);
void set_var_vals(vector<Variable*>&, vector<int>&);
const string quick_scan_vars(vector<Variable*>&);

#endif
